var audio_player;
var video_players;
var current_index = 0;
var custom_playlist = null;
var custom_index = null;
var audio_src = '';
var play_audio = function(btn){
    var $this = $(btn);
    var audio_file = $this.data('song');
    audio_src = $this.data('song');
    var audio_title = $this.data('title');    
    var play_audio = true;

    switch ($this.data('play')) {    
        case 'banner-slide':
            if($this.hasClass('playing')){
                $this.removeClass('playing');
                play_audio = false;
            } else{
                $('.carousel-3d-container a.playing').removeClass('playing');
                $this.addClass('playing');
                play_audio = true;
            }
            custom_playlist = null;
            custom_index = null;
        break;
        case 'home-projects':
            if($this.hasClass('playing')){
                $this.removeClass('playing');
                play_audio = false;
            } else{
                $this.closest('li').siblings().find('a').removeClass('playing');
                $this.addClass('playing');
                play_audio = true;
            }
            custom_playlist = null;
            custom_index = null;
        break;
        case 'projects':
            if($this.hasClass('playing')){
                $this.removeClass('playing');
                play_audio = false;
            } else{
                $this.closest('.portfolio-item').siblings().find('.item-image').removeClass('playing');
                $this.addClass('playing');
                play_audio = true;
            }
            custom_playlist = null;
            custom_index = null;
        break;
        case 'blog-audio':
            if($this.hasClass('playing')){
                $this.removeClass('playing');
                play_audio = false;
            } else{
                $this.closest('.article-playlist').find('a').removeClass('playing');
                $this.addClass('playing');
                play_audio = true;
            }

            custom_index = $this.data('index');
        break;
        default:
            play_audio = true;
            custom_playlist = null;
            custom_index = null;
        break;
    }

    if(play_audio){
        audio_player_callback('pause');
        if(custom_playlist != null && custom_index != null){
            playlist = custom_playlist;
            current_index = custom_index;
        } else{
            current_index = 0;
            playlist = [
                {
                    title: audio_title,
                    type: 'audio',
                    sources: [
                        {
                            src: audio_file,
                            type: 'audio/mp3'
                        }
                    ]
                }
            ];
        }
        audio_player.source(playlist[current_index]);
        audio_player_callback('play');
    } else{
        audio_player_callback('pause');
    }
}

function audio_player_callback(action){
    switch (action) {
        case 'restart':
            audio_player.restart();
        break;    
        case 'stop':
            if(audio_player.isReady() && !audio_player.isPaused()){
                audio_player.stop();
            }
        break;    
        case 'play':
            if(audio_player.isReady()){
                audio_player.play();
            }
        break;    
        default:
        case 'pause':
            if(!audio_player.isPaused()){
                audio_player.pause();
            }
        break;
    }
}

$(function(){
    'use strict';
    var controls = [
        "<div class='plyr__title-xs'></div>",
        "<div class='plyr__controls'>",
            "<span class='plyr__icon_play ik ik-play-circle-o' data-plyr='play'>",
                "<span class='plyr__sr-only'>Play</span>",
            "</span>",
            "<span class='plyr__icon_pause ik ik-pause-circle-o' data-plyr='pause'>",
                "<span class='plyr__sr-only'>Pause</span>",
            "</span>",
            "<span class='plyr__time'>",
                "<span class='plyr__sr-only'>Current time</span>",
                "<span class='plyr__time--current'>00:00</span>",
            "</span>",
            "<span class='plyr__progress'>",
                "<div class='plyr__title'></div>",
                "<label for='seek{id}' class='plyr__sr-only'>Seek</label>",
                "<input id='seek{id}' class='plyr__progress--seek' type='range' min='0' max='100' step='0.1' value='0' data-plyr='seek'>",
                "<progress class='plyr__progress--played' max='100' value='0' role='presentation'></progress>",
                "<progress class='plyr__progress--buffer' max='100' value='0'>",
                    "<span>0</span>% buffered",
                "</progress>",
                "<span class='plyr__tooltip'>00:00</span>",
            "</span>",
            "<span class='plyr__time'>",
                "<span class='plyr__sr-only'>Duration</span>",
                "<span class='plyr__time--duration'>00:00</span>",
            "</span>",
            "<span class='ml-30 mr-5 w-15_i ik ik-volume-up text-left plyr__volume_icon' data-plyr='mute'>",
                "<span class='plyr__sr-only'>Toggle Mute</span>",
            "</span>",
            "<span class='plyr__volume'>",
                "<label for='volume{id}' class='plyr__sr-only'>Volume</label>",
                "<input id='volume{id}' class='plyr__volume--input' type='range' min='0' max='10' value='5' data-plyr='volume'>",
                "<progress class='plyr__volume--display' max='10' value='0' role='presentation'></progress>",
            "</span>",
        "</div>"
    ].join("");

    var players = plyr.setup($('#header_player-element'), {
        html: controls,
        debug: false
    });
    
    audio_player = players[0];
    if(default_play == 1){
        audio_player.source(playlist[current_index]);
        audio_player_callback('play');
    } else if(playlist[current_index] != undefined){
        audio_player.source(playlist[current_index]);
        audio_player_callback('stop');
    } else{
        audio_player.source({});
    }
    audio_player.on('volumechange', function(event) {
        var $icon = $(event.target).find('.plyr__volume_icon');
        var volume = event.detail.plyr.getVolume() * 10;
        
        var volume_on_icon = (volume > 5)?'ik-volume-up':(volume == 5)?'ik-volume-half':'ik-volume-down';
        if(event.detail.plyr.isMuted() || volume == 0){
            $icon.removeClass("ik-volume-up ik-volume-half ik-volume-down ik-volume-mute").addClass('ik-volume-mute');
        } else{
            $icon.removeClass('ik-volume-up ik-volume-half ik-volume-down ik-volume-mute').addClass(volume_on_icon);
        }
    }).on('ended', function(event) {
        if(audio_player.getCurrentTime() >= audio_player.getDuration()){
            if(current_index < (Object.keys(playlist).length - 1)){
                current_index = current_index + 1;
            } else{
                current_index = 0;
            }
            
            if(playlist[current_index] != undefined){
                audio_player_callback('pause');
                audio_player.source(playlist[current_index]);
                audio_player_callback('play');
                $('[data-index]').removeClass('playing');
                $('[data-index="'+current_index+'"]').addClass('playing');
            } else{
                audio_player_callback('stop');
            }
        }
    }).on('play', function(event) {
        if(playlist[current_index] != undefined){
            $(event.target).find('.plyr__title').text(playlist[current_index].title);
            $(event.target).find('.plyr__title-xs').text(playlist[current_index].title);
        } else{
            $(event.target).find('.plyr__title').text('');
            $(event.target).find('.plyr__title-xs').text('');
        }
    });
});