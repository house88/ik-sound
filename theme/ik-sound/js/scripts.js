var play_video = function(btn){
    var $this = $(btn);
    audio_player_callback('pause');

    $this.closest('.video-item').find('.play-video-btn').click();
}

$(function(){
    'use strict';
    
    $('body').on('click vclick', ".call-function", function(e){
        e.preventDefault();
        e.stopPropagation();
        var $thisBtn = $(this);
        if($thisBtn.hasClass('disabled')){
            return false;
        }
        
        var callBack = $thisBtn.data('callback');
        window[callBack]($thisBtn);
        return false;
    });
	
	$('body').on('click vclick', ".call-video-popup", function(e){
		e.preventDefault();
		var $this = $(this);
		if($this.hasClass('disabled')){
			return false;
		}

		var popup_url = $this.data('href');
		var $popup_parent = $($this.data('popup'));
		$.ajax({
			type: 'POST',
			url: popup_url,
			data: {},
			dataType: 'JSON',
			beforeSend: function(){},
			success: function(resp){
				if(resp.mess_type == 'success'){
                    audio_player_callback('pause');
					$popup_parent.html(resp.popup_content).modal('show').on('hidden.bs.modal', function(){
						$(this).html('');
					});
				}
			}
		});
		return false;
	});

    $('#video_popup_form').on('hidden.bs.modal', function(){
        video_players[0].destroy();
        $(this).html('');
    });
    
    set_nav_item_active();
});

window.addEventListener('popstate', function(event) {
    if(event && event.state && event.state.url){
        header_menu(event.state.url);
        navigate_url(event.state.url);
    } else{
        header_menu(site_url);
        navigate_url(site_url);
    }
});

function set_nav_item_active(){
    var url = window.location;
    $('ul.navbar-ik-links li').removeClass('active');
    var element = $('ul.navbar-ik-links a').filter(function() {
        return this.href == url.href || (url.href.indexOf(this.href) == 0 && this.href != site_url && this.href != site_lang_url);
    }).parent('li').addClass('active');

    $('ul.header_menu_dropdown li').removeClass('active');
    var element = $('ul.header_menu_dropdown a').filter(function() {
        return this.href == url.href || (url.href.indexOf(this.href) == 0 && this.href != site_url && this.href != site_lang_url);
    }).parent('li').addClass('active');
}

var countAnimationExecuted = false;
function countAnimation(){
    if (!countAnimationExecuted) {
        countAnimationExecuted = true;
        var _countList = $('[data-count]');
        $.each(_countList, function (value, index) {
            var endValue = $(this).data('number');

            var queueCountAnimation = new CountUp($(this).data('target'), 0, endValue, 0, 2);
            queueCountAnimation.start();
        });
    }
}

var popup_share = function(obj){
	var $this = $(obj);
	var popupUrl = '';
	var popupTitle = $this.data('title');
	var popupOptions = 'width=550,height=400,0,status=0';
	var socialUrl = $this.data('url');
    var socialTitle = cleanInput($this.data('title'));
    switch ($this.data('social')) {
        case 'facebook':
            popupUrl += 'http://www.facebook.com/share.php?u=' + socialUrl + '&title=' + socialTitle;
        break;
        case 'twitter':
            popupUrl += 'http://twitter.com/intent/tweet?status=' + socialTitle + '+' + socialUrl;
        break;
		case 'vk':
			popupUrl += 'https://vk.com/share.php?url=' + socialUrl;
		break;
		case 'linkedin':
			popupUrl += 'http://www.linkedin.com/shareArticle?mini=true&url=' + socialUrl + '&title=' + socialTitle;
		break;
	}

	winPopup(popupUrl, popupTitle, popupOptions);
}

function winPopup(mylink,title,options) {
	var mywin = window.open(mylink,title,options);
	mywin.focus();

	return mywin;
}

function cleanInput(str){
	if(str != undefined && str != '' && str!= null){
		str = str.replace(/&/g, "&amp;");
		str = str.replace(/>/g, "&gt;");
		str = str.replace(/</g, "&lt;");
		str = str.replace(/"/g, "&quot;");
		str = str.replace(/'/g, "&#039;");
		return str;
	} else{
		return false;
	}
}



function intval(num){
	if (typeof num == 'number' || typeof num == 'string'){
		num = num.toString();
		var dotLocation = num.indexOf('.');
		if (dotLocation > 0){
			num = num.substr(0, dotLocation);
		}

		if (isNaN(Number(num))){
			num = parseInt(num);
		}

		if (isNaN(num)){
			return 0;
		}

		return Number(num);
	} else if (typeof num == 'object' && num.length != null && num.length > 0){
		return 1;
	} else if (typeof num == 'boolean' && num === true){
		return 1;
	}

	return 0;
}

function floatval(mixed_var) {
	return (parseFloat(mixed_var) || 0);
}

var switch_lang = function(btn){
    var $this = $(btn);
    var link = $this.attr('href');

    site_lang = $this.data('lang');
    site_lang_url = site_url + site_lang;
    
    header_menu(link);
    navigate_url(link);

    var state = {};
    state['url'] = link;
    history.pushState(state, 'IK-Sound'+$this.text(), link);

    if($this.closest('.navbar-ik-links').length != undefined){
        var $navbar_links = $this.closest('.navbar-ik-links');
        $navbar_links.find('li').removeClass('active');
        $this.closest('li').addClass('active');
    }
}

var render_page = function(btn){
    var $this = $(btn);
    var link = $this.attr('href');

    header_menu(link);
    navigate_url(link);

    var state = {};
    state['url'] = link;
    history.pushState(state, 'IK-Sound'+$this.text(), link);

    if($this.closest('.navbar-ik-links').length != undefined){
        var $navbar_links = $this.closest('.navbar-ik-links');
        $navbar_links.find('li').removeClass('active');
        $this.closest('li').addClass('active');
    }
}

function navigate_url(link) {
    $.ajax({
        type: 'POST',
        url: link,
        data: {full_render:true},
        dataType: 'JSON',
        beforeSend: function(){},
        success: function(resp){
            if(resp.mess_type == 'success'){
                $('.js-page-wr').html(resp.content);
                Waypoint.refreshAll();
                countAnimationExecuted = false;
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            jqXHR.abort();
        }
    });
}

function header_menu(link) {
    $.ajax({
        type: 'POST',
        url: site_url+site_lang+'/page/get_header',
        data: {link:link},
        dataType: 'JSON',
        beforeSend: function(){},
        success: function(resp){
            if(resp.mess_type == 'success'){
                $('.header-wr').replaceWith(resp.header_menu);
                set_nav_item_active();
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            jqXHR.abort();
        }
    });
}