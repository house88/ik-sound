var header_banner = new Vue({
    el: '#home-carousel-slider',
    data: {
        header_slides: slider_elements
    },
    methods: {
        canPlay : function (audio) {
            return audio.play_btn != true;
        }
    },
    components: {
        'carousel-3d': Carousel3d.Carousel3d,
        'slide': Carousel3d.Slide
    }
});