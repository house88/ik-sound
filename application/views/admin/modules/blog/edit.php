<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
			<div class="box-tools pull-right">
				<a href="<?php echo base_url('admin/blog/add');?>" class="btn btn-box-tool">
					<i class="fa fa-plus"></i> Добавить блог
				</a>
			</div>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form id="edit_form">			
			<div class="box-body">
				<div class="form-group">
					<label>Фото <small>(Мин. ширина: 800px, высота: 100px, не превышать 5Мб)</small></label>
					<div class="clearfix"></div>
					<span class="btn btn-default btn-file pull-left mb-15">
						<i class="fa fa-picture-o"></i>
						Добавить фото <input id="select_photo" type="file" name="userfile">
					</span>
					<div class="clearfix"></div>
					<div id="blog_photo" class="files pull-left">
						<?php if($blog['blog_photo'] != ''){?>
							<div class="user-image-thumbnail-wr">
								<div class="user-image-thumb">
									<img class="img-thumbnail" src="<?php echo base_url('files/blog/'.$blog['blog_photo']);?>">
								</div>
								<a href="#" class="btn btn-danger btn-xs confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="Удалить" data-photo="<?php echo $blog['blog_photo'];?>">
									<span class="glyphicon glyphicon-remove-circle"></span>
								</a>
								<input type="hidden" name="blog_photo" value="<?php echo $blog['blog_photo'];?>">
							</div>
						<?php }?>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="form-group">
					<label>Аудио <small>(формат .mp3)</small></label>
					<div class="clearfix"></div>
					<span class="btn btn-default btn-file pull-left mb-15">
						<i class="fa fa-audio-o"></i>
						Добавить аудио <input id="select_audio" type="file" name="audiofile" multiple>
					</span>
					<div class="clearfix"></div>
					<div id="blog_audio" class="files pull-left">
						<?php $blog_audio_files = json_decode($blog['blog_audio']);?>
						<?php if(!empty($blog_audio_files)){?>
							<?php foreach($blog_audio_files as $blog_audio_file){?>
								<div class="input-group mb-5">
									<div class="input-group-btn">
										<a href="#" class="btn btn-danger btn-sm confirm-dialog" data-callback="remove_audio" data-message="Вы уверены что хотите удалить эту аудио запись?" title="Удалить" data-title="Удалить" data-file="<?php echo $blog_audio_file->name;?>">
											<span class="glyphicon glyphicon-remove-circle"></span>
										</a>
										<a href="#" class="btn btn-default btn-sm call-function" data-callback="order_element_row" title="Поднять" data-action="up">
											<span class="fa fa-caret-square-o-up"></span>
										</a>
										<a href="#" class="btn btn-default btn-sm call-function" data-callback="order_element_row" title="Опустить" data-action="down">
											<span class="fa fa-caret-square-o-down"></span>
										</a>
									</div>
									<input name="audio_files[]" type="hidden" class="form-control input-sm" value="<?php echo $blog_audio_file->name;?>">
									<input name="audio_client_names[]" type="text" class="form-control input-sm" value="<?php echo $blog_audio_file->title;?>">
								</div>
							<?php }?>
						<?php }?>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#form_ru" data-toggle="tab" aria-expanded="true">Русский</a>
						</li>
						<li class="">
							<a href="#form_en" data-toggle="tab" aria-expanded="false">English</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="form_ru">							
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_ru" value="<?php echo $blog['blog_title_ru'];?>">
								<p class="help-block">Название не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Краткое описание</label>
								<textarea class="form-control" name="stext_ru"><?php echo $blog['blog_small_description_ru'];?></textarea>
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Текст</label>
								<textarea class="description" name="description_ru"><?php echo $blog['blog_description_ru'];?></textarea>
							</div>
							<div class="form-group">
								<label>Meta keywords</label>
								<input class="form-control" placeholder="Meta keywords" name="mk_ru" value="<?php echo $blog['mk_ru'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta description</label>
								<input class="form-control" placeholder="Meta description" name="md_ru" value="<?php echo $blog['md_ru'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
						</div>
						<div class="tab-pane" id="form_en">							
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_en" value="<?php echo $blog['blog_title_en'];?>">
								<p class="help-block">Название не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Краткое описание</label>
								<textarea class="form-control" name="stext_en"><?php echo $blog['blog_small_description_en'];?></textarea>
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Текст</label>
								<textarea class="description" name="description_en"><?php echo $blog['blog_description_en'];?></textarea>
							</div>
							<div class="form-group">
								<label>Meta keywords</label>
								<input class="form-control" placeholder="Meta keywords" name="mk_en" value="<?php echo $blog['mk_en'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta description</label>
								<input class="form-control" placeholder="Meta description" name="md_en" value="<?php echo $blog['md_en'];?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
						</div>
					</div>
					<!-- /.tab-content -->
				</div>
			</div>
			<!-- /.box-body -->

			<div class="box-footer">
				<div class="form-group pull-left w-205">
					<div class="input-group date" id="datetimepicker">
						<input type="text" class="form-control" name="blog_date" value="<?php echo formatDate($blog['blog_date'], 'd.m.Y H:i:s');?>"/>
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
				</div>
				<input type="hidden" name="blog" value="<?php echo $blog['id_blog'];?>">
				<button type="submit" class="btn btn-primary btn-flat pull-right">Сохранить</button>
			</div>
		</form>
	</div>
</div>

<script>
	$(function(){
		'use strict';
		$('#datetimepicker').datetimepicker({
			locale: 'ru',
			format: 'DD.MM.YYYY HH:mm:ss',
			tooltips:{
				today:"Сегодня",
				clear:"Очистить",
				close:"Закрыть",
				selectMonth:"Выберите месяц",
				prevMonth:"Пред. месяц",
				nextMonth:"След. месяц",
				selectYear:"Выберите год",
				prevYear:"Пред. год",
				nextYear:"След. год",
				selectDecade:"Выберите десятилетие",
				prevDecade:"Пред. десятилетие",
				nextDecade:"След. десятилетие",
				prevCentury:"Пред. век",
				nextCentury:"След. век",
				pickHour:"Выберите час",
				incrementHour:"Добавить час",
				decrementHour:"Убавить час",
				pickMinute:"Выберите минуты",
				incrementMinute:"Добавить минуту",
				decrementMinute:"Убавить минуту",
				pickSecond:"Выберите секунду",
				incrementSecond:"Добавить секунду",
				decrementSecond:"Убавить секунду",
				togglePeriod:"Переключить период",
				selectTime:"Выберите время"
			}
		});

		$('#select_photo').fileupload({
			url: base_url+'admin/blog/ajax_operations/upload_photo',
			dataType: 'json',
			done: function (e, data) {
				if(data.result.mess_type == 'error'){
					systemMessages( data.result.message, data.result.mess_class );
				} else{
					$.each(data.result.files, function (index, file) {
						var template = '<div class="user-image-thumbnail-wr">\
											<div class="user-image-thumb">\
												<img class="img-thumbnail" src="'+base_url+'files/blog/'+file.filename+'"/>\
											</div>\
											<a href="#" class="btn btn-danger btn-xs confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="Удалить" data-photo="'+file.filename+'"><span class="glyphicon glyphicon-remove-circle"></span></a>\
											<input type="hidden" name="blog_photo" value="'+file.filename+'">\
										</div>';

						if($('#blog_photo .user-image-thumbnail-wr').length > 0){
							var unused_photo = $('#blog_photo .user-image-thumbnail-wr').find('input[name="blog_photo"]').val();
							$('#blog_photo').closest('form').append('<input type="hidden" name="remove_photos[]" value="'+unused_photo+'"/>');
						}
						$('#blog_photo').html(template);
					});
				}
			}
		}).prop('disabled', !$.support.fileInput)
			.parent().addClass($.support.fileInput ? undefined : 'disabled');
		
		$('#select_audio').fileupload({
		url: base_url+'admin/blog/ajax_operations/upload_audio',
		dataType: 'json',
		done: function (e, data) {
			if(data.result.mess_type == 'error'){
				systemMessages( data.result.message, data.result.mess_class );
			} else{
				var file = data.result.file;
				var template = '<div class="input-group mb-5">\
									<div class="input-group-btn">\
										<a href="#" class="btn btn-danger btn-sm confirm-dialog" data-callback="remove_audio" data-message="Вы уверены что хотите удалить эту аудио запись?" title="Удалить" data-title="Удалить" data-file="'+file.name+'">\
											<span class="glyphicon glyphicon-remove-circle"></span>\
										</a>\
										<a href="#" class="btn btn-default btn-sm call-function" data-callback="order_element_row" title="Поднять" data-action="up">\
											<span class="fa fa-caret-square-o-up"></span>\
										</a>\
										<a href="#" class="btn btn-default btn-sm call-function" data-callback="order_element_row" title="Опустить" data-action="down">\
											<span class="fa fa-caret-square-o-down"></span>\
										</a>\
									</div>\
									<input name="audio_files[]" type="hidden" class="form-control input-sm" value="'+file.name+'">\
									<input name="audio_client_names[]" type="text" class="form-control input-sm" value="'+file.client_name+'">\
								</div>';
				$('#blog_audio').append(template);
			}
		}
	}).prop('disabled', !$.support.fileInput)
		.parent().addClass($.support.fileInput ? undefined : 'disabled');
	});
	
	var edit_form = $('#edit_form');
	edit_form.submit(function () {
		tinyMCE.triggerSave();
		var fdata = edit_form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/blog/ajax_operations/edit',
			data: fdata,
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
			}
		});
		return false;
	});

	var remove_photo = function(btn){
		var $this = $(btn);
		var photo = $this.data('photo');
		$this.closest('form').append('<input type="hidden" name="remove_photos[]" value="'+photo+'"/>');
		$this.closest('.user-image-thumbnail-wr').remove();
	}

	var order_element_row = function(btn){
		var $this = $(btn);
		var $element_row = $this.closest('.input-group');
		switch($this.data('action')){
			case 'up':
				var $prev_element_row = $element_row.prev();
				$element_row.insertBefore($prev_element_row);
			break;
			case 'down':
				var $next_element_row = $element_row.next();
				$element_row.insertAfter($next_element_row);
			break;
		}
	}

	var remove_audio = function(btn){
		var $this = $(btn);
		var file = $this.data('file');
		$this.closest('form').append('<input type="hidden" name="remove_files[]" value="'+file+'"/>');
		$this.closest('.input-group').remove();
	}
</script>
