<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
		</div>
		<!-- /.box-header -->
			<!-- form start -->
		<form id="settings_form">
			<div class="box-body">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#settings_ru" data-toggle="tab" aria-expanded="true">Русский</a>
						</li>
						<li class="">
							<a href="#settings_en" data-toggle="tab" aria-expanded="false">English</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="settings_ru">
							<?php foreach($settings as $setting){?>
								<div class="form-group">
									<label><?php echo $setting['setting_title'];?></label>
									<input class="form-control" type="text" name="settings[<?php echo $setting['setting_alias'];?>][ru]" value="<?php echo $setting['setting_value_ru'];?>">
								</div>
							<?php }?>
						</div>
						<!-- /.tab-pane -->
						<div class="tab-pane" id="settings_en">
							<?php foreach($settings as $setting){?>
								<div class="form-group">
									<label><?php echo $setting['setting_title'];?></label>
									<input class="form-control" type="text" name="settings[<?php echo $setting['setting_alias'];?>][en]" value="<?php echo $setting['setting_value_en'];?>">
								</div>
							<?php }?>
						</div>
					</div>
					<!-- /.tab-content -->
				</div>
			</div>
				<!-- /.box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-primary">Сохранить</button>
			</div>
		</form>
	</div>
</div>

<script>
	var settings_form = $('#settings_form');
	settings_form.submit(function () {
		var fdata = settings_form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/ajax_operations/save_settings',
			data: fdata,
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
			}
		});
		return false;
	});
</script>