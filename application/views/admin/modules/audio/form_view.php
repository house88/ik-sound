<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php if(!empty($audio)){echo 'Редактирование';} else{echo 'Добавление';}?> аудио</h4>
        </div>
        <form id="add_audio_form">
            <div class="modal-body">
                <div class="form-group clearfix">
                    <span class="btn btn-default btn-sm btn-file pull-left">
                        <i class="fa fa-file-audio-o"></i>
                        Загрузить файл
                        <input id="select_file" type="file" name="audiofile">
                    </span>
                </div>
                <div class="form-group clearfix mb-10" id="audio_file">
                    <?php if(!empty($audio)){?>
                        <div class="input-group mb-5">
                            <input name="audio_client_name" class="form-control input-sm" value="<?php echo $audio['audio_client_name'];?>">
                            <input name="audio_file" type="hidden" value="<?php echo $audio['audio_file'];?>">
                            <div class="input-group-btn">
                                <a href="#" class="btn btn-danger btn-sm confirm-dialog" data-dtype="danger" data-callback="remove_audio" data-message="Вы уверены что хотите удалить эту аудио запись?" title="Удалить" data-title="Удалить" data-file="<?php echo $audio['audio_file'];?>">
                                    <span class="fa fa-trash"></span>
                                </a>
                            </div>
                        </div>
                    <?php }?>
                </div>
                <div class="nav-tabs-custom no-shadow mb-0">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#form_ru" data-toggle="tab" aria-expanded="true">Русский</a>
						</li>
						<li class="">
							<a href="#form_en" data-toggle="tab" aria-expanded="false">English</a>
						</li>
						<li class="">
							<a href="#form_properties" data-toggle="tab" aria-expanded="false">Свойства</a>
						</li>
						<li class="">
							<a href="#form_image" data-toggle="tab" aria-expanded="false">Постер</a>
						</li>
					</ul>
					<div class="tab-content pl-0_i pr-0_i">
						<div class="tab-pane active" id="form_ru">
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_ru" value="<?php if(!empty($audio)){echo $audio['audio_title_ru'];}?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Описание</label>
                                <textarea name="description_ru" rows="4" class="form-control" placeholder="Описание"><?php if(!empty($audio)){echo $audio['audio_description_ru'];}?></textarea>
								<p class="help-block">Не должно содержать более 500 символов.</p>
							</div>
						</div>
						<!-- /.tab-pane -->
						<div class="tab-pane" id="form_en">
                            <div class="form-group">
                                <label>Название</label>
                                <input class="form-control" placeholder="Название" name="title_en" value="<?php if(!empty($audio)){echo $audio['audio_title_en'];}?>">
                                <p class="help-block">Не должно содержать более 250 символов.</p>
                            </div>
                            <div class="form-group">
                                <label>Описание</label>
                                <textarea name="description_en" rows="4" class="form-control" placeholder="Описание"><?php if(!empty($audio)){echo $audio['audio_description_en'];}?></textarea>
                                <p class="help-block">Не должно содержать более 500 символов.</p>
                            </div>
                        </div>
                        <?php if(!empty($properties)){?>
                            <div class="tab-pane" id="form_properties">
                                <?php foreach($properties as $property){?>
                                    <div class="form-group">
                                        <label><?php echo $property['property_title_ru'];?></label>
                                        <select name="properties[<?php echo $property['id_property'];?>][]" class="form-control selectpicker" data-live-search="true" multiple>
                                            <?php $property_values = json_decode($property['property_values'], true);?>
                                            <?php foreach($property_values as $id_value => $property_value){?>
                                                <option value="<?php echo $id_value;?>" <?php echo set_select('property['.$property['id_property'].'][]', $id_value, !empty($audio_properties_values[$property['id_property']]) && in_array($id_value, $audio_properties_values[$property['id_property']]) );?> data-tokens="<?php echo $property_value['title_ru'];?> <?php echo $property_value['title_en'];?>"><?php echo $property_value['title_ru'];?></option>
                                            <?php }?>
                                        </select>
                                    </div>                                
                                <?php }?>
                            </div>                        
                        <?php }?>
						<div class="tab-pane" id="form_image">
                            <div class="form-group clearfix">
                                <p class="help-block text-left"><strong><small>(Мин. ширина: 600px, высота: 400px, не превышать 5Мб)</small></strong></p>
                                <span class="btn btn-default btn-sm btn-file pull-left">
                                    <i class="fa fa-picture-o"></i>
                                    Загрузить постер
                                    <input id="select_photo" type="file" name="userfile">
                                </span>
                            </div>
                            <div class="form-group clearfix mb-0" id="audio_image">
                                <?php if(!empty($audio)){?>
                                    <div class="user-image-thumbnail-wr mb-0 mr-0">
                                        <div class="user-image-thumb" style="height:auto;">
                                            <img src="<?php echo base_url('files/audio/poster/'.$audio['audio_image']);?>"/>
                                        </div>
                                        <a href="#" class="btn btn-danger btn-xs confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="Удалить" data-photo="<?php echo $audio['audio_image'];?>">
                                            <span class="glyphicon glyphicon-remove-circle"></span>
                                        </a>
                                        <input type="hidden" name="audio_image" value="<?php echo $audio['audio_image'];?>">
                                    </div>
                                <?php }?>
                            </div>
						</div>
					</div>
					<!-- /.tab-content -->
				</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <?php if(!empty($audio)){?>
                    <div class="form-group pull-left w-185">
                        <div class="input-group date" id="datetimepicker">
                            <input type="text" class="form-control" name="audio_date" value="<?php echo formatDate($audio['audio_date'], 'd.m.Y H:i:s');?>"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <input type="hidden" name="id_audio" value="<?php echo $audio['id_audio'];?>">
                    <button type="button" class="btn btn-success call-function" data-callback="manage_audio" data-action="edit">Сохранить</button>
                <?php } else{?>
                    <button type="button" class="btn btn-success call-function" data-callback="manage_audio" data-action="add">Сохранить</button>
                <?php }?>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
        'use strict';

        <?php if(!empty($audio)){?>
            $('#datetimepicker').datetimepicker({
                locale: 'ru',
                format: 'DD.MM.YYYY HH:mm:ss',
                tooltips:{
                    today:"Сегодня",
                    clear:"Очистить",
                    close:"Закрыть",
                    selectMonth:"Выберите месяц",
                    prevMonth:"Пред. месяц",
                    nextMonth:"След. месяц",
                    selectYear:"Выберите год",
                    prevYear:"Пред. год",
                    nextYear:"След. год",
                    selectDecade:"Выберите десятилетие",
                    prevDecade:"Пред. десятилетие",
                    nextDecade:"След. десятилетие",
                    prevCentury:"Пред. век",
                    nextCentury:"След. век",
                    pickHour:"Выберите час",
                    incrementHour:"Добавить час",
                    decrementHour:"Убавить час",
                    pickMinute:"Выберите минуты",
                    incrementMinute:"Добавить минуту",
                    decrementMinute:"Убавить минуту",
                    pickSecond:"Выберите секунду",
                    incrementSecond:"Добавить секунду",
                    decrementSecond:"Убавить секунду",
                    togglePeriod:"Переключить период",
                    selectTime:"Выберите время"
                }
            });
        <?php }?>

        $('.selectpicker').selectpicker({
            style: 'btn-default',
            size: 4
        });
        
		$('#select_photo').fileupload({
			url: base_url+'admin/audio/ajax_operations/upload_photo',
			dataType: 'json',
			done: function (e, data) {
				if(data.result.mess_type == 'error'){
					systemMessages( data.result.message, data.result.mess_class );
				} else{
					$.each(data.result.files, function (index, file) {
						var template = '<div class="user-image-thumbnail-wr mb-0 mr-0">\
											<div class="user-image-thumb" style="height:auto;">\
												<img src="'+base_url+'files/audio/poster/'+file.filename+'"/>\
											</div>\
											<a href="#" class="btn btn-danger btn-xs confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="Удалить" data-photo="'+file.filename+'">\
												<span class="glyphicon glyphicon-remove-circle"></span>\
											</a>\
											<input type="hidden" name="audio_image" value="'+file.filename+'">\
										</div>';

						if($('#audio_image .user-image-thumbnail-wr').length > 0){
							var unused_photo = $('#audio_image .user-image-thumbnail-wr').find('input[name="audio_image"]').val();
							$('#audio_image').closest('form').append('<input type="hidden" name="remove_photos[]" value="'+unused_photo+'"/>');
						}
						$('#audio_image').html(template);
					});
				}
			}
		}).prop('disabled', !$.support.fileInput)
			.parent().addClass($.support.fileInput ? undefined : 'disabled');

		$('#select_file').fileupload({
			url: base_url+'admin/audio/ajax_operations/upload_file',
			dataType: 'json',
			done: function (e, data) {
				if(data.result.mess_type == 'error'){
					systemMessages( data.result.message, data.result.mess_class );
				} else{
					var file = data.result.file;
                    var template = '<div class="input-group mb-5">\
                                        <input name="audio_client_name" class="form-control input-sm" value="'+file.client_name+'">\
                                        <input name="audio_file" type="hidden" value="'+file.name+'">\
                                        <div class="input-group-btn">\
                                            <a href="#" class="btn btn-danger btn-sm confirm-dialog" data-dtype="danger" data-callback="remove_audio" data-message="Вы уверены что хотите удалить эту аудио запись?" title="Удалить" data-title="Удалить" data-file="'+file.name+'">\
                                                <span class="fa fa-trash"></span>\
                                            </a>\
                                        </div>\
                                    </div>';
                    if($('#audio_file .input-group').length > 0){
                        var unused_file = $('#audio_file .input-group').find('input[name="audio_file"]').val();
                        $('#audio_file').closest('form').append('<input type="hidden" name="remove_files[]" value="'+unused_file+'"/>');
                    }
					$('#audio_file').html(template);
				}
			}
		}).prop('disabled', !$.support.fileInput)
			.parent().addClass($.support.fileInput ? undefined : 'disabled');

		$('input.nice-input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%'
		});
    });

	var remove_photo = function(btn){
		var $this = $(btn);
		var photo = $this.data('photo');
		$this.closest('form').append('<input type="hidden" name="remove_photos[]" value="'+photo+'"/>');
		$this.closest('.user-image-thumbnail-wr').remove();
	}
    
    var remove_audio = function(btn){
        var $this = $(btn);
        var file = $this.data('file');
        $this.closest('form').append('<input type="hidden" name="remove_files[]" value="'+file+'"/>');
        $this.closest('.input-group').remove();
    }

    var manage_audio = function(btn){
        var $this = $(btn);
        var action = $this.data('action');
        var $form = $this.closest('form');
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url();?>admin/audio/ajax_operations/'+action,
            data: $form.serialize(),
            dataType: 'JSON',
            beforeSend: function(){
                clearSystemMessages();
                $this.addClass('disabled');
            },
            success: function(resp){
                systemMessages( resp.message, resp.mess_type );
                $this.removeClass('disabled');

                if(resp.mess_type == 'success'){
                    $('#general_popup_form').modal('hide');
                    $form.trigger( 'reset' );
					manage_audio_callback();
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
        });

        return false;
    }
</script>