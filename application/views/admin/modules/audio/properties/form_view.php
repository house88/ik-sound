<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php if(!empty($category)){echo 'Редактирование';} else{echo 'Добавление';}?> свойства</h4>
        </div>
        <form id="add_category_form">			
            <div class="modal-body">                
                <div class="nav-tabs-custom no-shadow mb-0">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#form_ru" data-toggle="tab" aria-expanded="true">Русский</a>
                        </li>
                        <li class="">
                            <a href="#form_en" data-toggle="tab" aria-expanded="false">English</a>
                        </li>
                    </ul>
                    <div class="tab-content pl-0_i pr-0_i">
                        <div class="tab-pane active" id="form_ru">
                            <div class="form-group mb-0">
                                <label>Название</label>
                                <input class="form-control" placeholder="Название" name="title_ru" value="<?php if(!empty($property)){echo $property['property_title_ru'];}?>">
                                <p class="help-block mb-0">Название не должно содержать более 50 символов.</p>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="form_en">
                            <div class="form-group mb-0">
                                <label>Название</label>
                                <input class="form-control" placeholder="Название" name="title_en" value="<?php if(!empty($property)){echo $property['property_title_en'];}?>">
                                <p class="help-block mb-0">Название не должно содержать более 50 символов.</p>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-content -->
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label>Значения</label>
                    </div>
                    <div class="col-xs-12" id="property_values">
                        <?php if(!empty($property)){?>
                            <?php 
                                $property_values = json_decode($property['property_values'], true);
                                $max_index = max(array_keys($property_values));
                            ?>
                            <?php foreach($property_values as $id_value => $property_value){?>
                                <div class="input-group mb-10">
                                    <span class="input-group-addon">Рус</span>
                                    <input type="hidden" name="values[]" value="<?php echo $id_value;?>">
                                    <input type="text" class="form-control input-sm" name="values_ru[<?php echo $id_value;?>]" value="<?php echo $property_value['title_ru'];?>">
                                    <span class="input-group-addon">En</span>
                                    <input type="text" class="form-control input-sm" name="values_en[<?php echo $id_value;?>]" value="<?php echo $property_value['title_en'];?>">
                                    <div class="input-group-btn">
                                        <span class="btn btn-default btn-sm call-function" data-callback="change_weight_value" data-order="up"><i class="fa fa-caret-square-o-up"></i></span>
                                        <span class="btn btn-default btn-sm call-function" data-callback="change_weight_value" data-order="down"><i class="fa fa-caret-square-o-down"></i></span>
                                        <span class="btn btn-danger btn-sm confirm-dialog" data-callback="remove_value" data-message="Вы точно хотите удалить это значение?" data-dtype="danger" data-title="Удалить"><i class="fa fa-trash"></i></span>
                                    </div>
                                </div>                            
                            <?php }?>
                        <?php } else{?>
                            <div class="input-group mb-10">
                                <span class="input-group-addon">Рус</span>
                                <input type="hidden" name="values[]" value="1">
                                <input type="text" class="form-control input-sm" name="values_ru[1]">
                                <span class="input-group-addon">En</span>
                                <input type="text" class="form-control input-sm" name="values_en[1]">
                                <div class="input-group-btn">
                                    <span class="btn btn-default btn-sm call-function" data-callback="change_weight_value" data-order="up"><i class="fa fa-caret-square-o-up"></i></span>
                                    <span class="btn btn-default btn-sm call-function" data-callback="change_weight_value" data-order="down"><i class="fa fa-caret-square-o-down"></i></span>
                                    <span class="btn btn-danger btn-sm confirm-dialog" data-callback="remove_value" data-message="Вы точно хотите удалить это значение?" data-dtype="danger" data-title="Удалить"><i class="fa fa-trash"></i></span>
                                </div>
                            </div>
                        <?php }?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default call-function pull-left" data-callback="add_value"><i class="fa fa-plus"></i> Добавить значение</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <?php if(!empty($property)){?>
                    <input type="hidden" name="id_property" value="<?php echo $property['id_property'];?>">
                    <button type="button" class="btn btn-success call-function" data-callback="manage_property" data-action="edit_property">Сохранить</button>
                <?php } else{?>
                    <button type="button" class="btn btn-success call-function" data-callback="manage_property" data-action="add_property">Сохранить</button>
                <?php }?>
            </div>
        </form>
    </div>
</div>
<script>
    <?php if(!empty($property)){?>
        var value_index = intval('<?php echo $max_index;?>');
    <?php } else{?>
        var value_index = 1;
    <?php }?>
    var add_value = function(btn){
        var $this = $(btn);
        value_index = value_index + 1;
        var template = '<div class="input-group mb-10">\
                            <span class="input-group-addon">Рус</span>\
                            <input type="hidden" name="values[]" value="'+value_index+'">\
                            <input type="text" class="form-control input-sm" name="values_ru['+value_index+']">\
                            <span class="input-group-addon">En</span>\
                            <input type="text" class="form-control input-sm" name="values_en['+value_index+']">\
                            <div class="input-group-btn">\
                                <span class="btn btn-default btn-sm call-function" data-callback="change_weight_value" data-order="up"><i class="fa fa-caret-square-o-up"></i></span>\
                                <span class="btn btn-default btn-sm call-function" data-callback="change_weight_value" data-order="down"><i class="fa fa-caret-square-o-down"></i></span>\
                                <span class="btn btn-danger btn-sm confirm-dialog" data-callback="remove_value" data-message="Вы точно хотите удалить это значение?" data-dtype="danger" data-title="Удалить"><i class="fa fa-trash"></i></span>\
                            </div>\
                        </div>';
        $('#property_values').append(template);
    }

    var remove_value = function(btn){
        var $this = $(btn);
        var $parent_row = $this.closest('.input-group');
        if($parent_row.data('value') != undefined){
            $this.closest('form').append('<input type="hidden" name="remove_values[]" value="'+$parent_row.data('value')+'">');
        }

        $parent_row.remove();
    }
    
    var change_weight_value = function(btn){
		var $this = $(btn);
        var $element_row = $this.closest('.input-group');
		switch($this.data('order')){
			case 'up':
				var $prev_element_row = $element_row.prev();
				$element_row.insertBefore($prev_element_row);
			break;
			case 'down':
				var $next_element_row = $element_row.next();
				$element_row.insertAfter($next_element_row);
			break;
		}
    }
    
    var manage_property = function(btn){
        var $this = $(btn);
        var action = $this.data('action');
        var $form = $this.closest('form');
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url();?>admin/audio/ajax_operations/'+action,
            data: $form.serialize(),
            dataType: 'JSON',
            beforeSend: function(){
                clearSystemMessages();
                $this.addClass('disabled');
            },
            success: function(resp){
                systemMessages( resp.message, resp.mess_type );
                $this.removeClass('disabled');

                if(resp.mess_type == 'success'){
                    $('#general_popup_form').modal('hide');
                    $form.trigger( 'reset' );
					manage_property_callback();
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
        });

        return false;
    }
</script>