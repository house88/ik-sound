<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
		</div>
		<!-- /.box-header -->
			<!-- form start -->
		<form id="translations_form">
			<div class="box-body">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#translations_ru" data-toggle="tab" aria-expanded="true">Русский</a>
						</li>
						<li class="">
							<a href="#translations_en" data-toggle="tab" aria-expanded="false">English</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="translations_ru">
							<?php foreach($translations as $translation){?>
								<div class="form-group">
									<input class="form-control" type="text" name="translations[<?php echo $translation['translation_key'];?>][ru]" value="<?php echo $translation['translation_text_ru'];?>">
								</div>
							<?php }?>
						</div>
						<!-- /.tab-pane -->
						<div class="tab-pane" id="translations_en">
							<?php foreach($translations as $translation){?>
								<div class="form-group">
									<input class="form-control" type="text" name="translations[<?php echo $translation['translation_key'];?>][en]" value="<?php echo $translation['translation_text_en'];?>">
								</div>
							<?php }?>
						</div>
					</div>
					<!-- /.tab-content -->
				</div>
			</div>
				<!-- /.box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-primary">Сохранить</button>
			</div>
		</form>
	</div>
</div>

<script>
	var translations_form = $('#translations_form');
	translations_form.submit(function () {
		var fdata = translations_form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/ajax_operations/save_translations',
			data: fdata,
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
			}
		});
		return false;
	});
</script>