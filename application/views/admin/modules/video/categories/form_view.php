<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php if(!empty($category)){echo 'Редактирование';} else{echo 'Добавление';}?> категории</h4>
        </div>
        <form id="add_category_form">			
            <div class="modal-body">
                <div class="nav-tabs-custom mb-0">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#form_ru" data-toggle="tab" aria-expanded="true">Русский</a>
						</li>
						<li class="">
							<a href="#form_en" data-toggle="tab" aria-expanded="false">English</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="form_ru">
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_ru" value="<?php if(!empty($category)){echo $category['category_title_ru'];}?>">
								<p class="help-block">Название не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta keywords</label>
								<input class="form-control" placeholder="Meta keywords" name="mk_ru" value="<?php if(!empty($category)){echo $category['mk_ru'];}?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta description</label>
								<input class="form-control" placeholder="Meta description" name="md_ru" value="<?php if(!empty($category)){echo $category['md_ru'];}?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
						</div>
						<!-- /.tab-pane -->
						<div class="tab-pane" id="form_en">
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_en" value="<?php if(!empty($category)){echo $category['category_title_en'];}?>">
								<p class="help-block">Название не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta keywords</label>
								<input class="form-control" placeholder="Meta keywords" name="mk_en" value="<?php if(!empty($category)){echo $category['mk_en'];}?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Meta description</label>
								<input class="form-control" placeholder="Meta description" name="md_en" value="<?php if(!empty($category)){echo $category['md_en'];}?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
						</div>
					</div>
					<!-- /.tab-content -->
				</div>
            </div>
            <div class="modal-footer">                
                <label class="pull-left lh-34 mb-0">
                    <input type="checkbox" name="category_active" <?php echo (!empty($category))?set_checkbox('category_active', 1, $category['category_active'] == 1):'checked';?> class="flat-red nice-input">
                    Видна на сайте
                </label>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <?php if(!empty($category)){?>
                    <input type="hidden" name="id_category" value="<?php echo $category['id_category'];?>">
                    <button type="button" class="btn btn-success call-function" data-callback="manage_category" data-action="edit_category">Сохранить</button>
                <?php } else{?>
                    <button type="button" class="btn btn-success call-function" data-callback="manage_category" data-action="add_category">Сохранить</button>
                <?php }?>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
		$('input.nice-input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%'
		});
    });
    var manage_category = function(btn){
        var $this = $(btn);
        var action = $this.data('action');
        var $form = $this.closest('form');
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url();?>admin/video/ajax_operations/'+action,
            data: $form.serialize(),
            dataType: 'JSON',
            beforeSend: function(){
                clearSystemMessages();
                $this.addClass('disabled');
            },
            success: function(resp){
                systemMessages( resp.message, resp.mess_type );
                $this.removeClass('disabled');

                if(resp.mess_type == 'success'){
                    $('#general_popup_form').modal('hide');
                    $form.trigger( 'reset' );
					manage_category_callback();
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
        });

        return false;
    }
</script>