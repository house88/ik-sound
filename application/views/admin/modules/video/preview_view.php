<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php echo $video['video_title_ru'];?></h4>
        </div>
        <div class="modal-body">
            <div data-video-id="<?php echo $video['video_code'];?>" data-type="<?php echo $video['video_type'];?>" data-title="<?php echo $video['video_title_ru'];?>"></div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        </div>
    </div>
</div>
<script>
    video_players = plyr.setup();    
    video_player = video_players[0];
    video_player.on('ready', function(event) {
        event.detail.plyr.play();
    });
</script>