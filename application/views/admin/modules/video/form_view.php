<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php if(!empty($category)){echo 'Редактирование';} else{echo 'Добавление';}?> видео</h4>
        </div>
        <form id="add_video_form">
            <div class="modal-body">
                <div class="form-group">
                    <label>Категория</label>
                    <select name="category" class="form-control">
                        <option value="">Выберите категорию</option>
                        <?php foreach($categories as $category){?>
                            <option value="<?php echo $category['id_category'];?>" <?php if(!empty($video)){ echo set_select('category', $category['id_category'], $video['video_category'] == $category['id_category']);}?>><?php echo $category['category_title_ru'];?></option>                                        
                        <?php }?>
                    </select>
                </div>
                <?php if(!empty($video)){?>
                    <div class="form-group">
                        <div class="input-group date" id="datetimepicker">
                            <input type="text" class="form-control" name="video_date" value="<?php echo formatDate($video['video_date'], 'd.m.Y H:i:s');?>"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                <?php }?>
                <div class="form-group">
                    <label>Ссылка, youtube или vimeo</label>
                    <input class="form-control" placeholder="Ссылка" name="link" value="<?php if(!empty($video)){echo $video['video_link'];}?>">
                </div>
                <div class="nav-tabs-custom mb-0">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#form_ru" data-toggle="tab" aria-expanded="true">Русский</a>
						</li>
						<li class="">
							<a href="#form_en" data-toggle="tab" aria-expanded="false">English</a>
						</li>
					</ul>
					<div class="tab-content pl-0_i pr-0_i">
						<div class="tab-pane active" id="form_ru">
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_ru" value="<?php if(!empty($video)){echo $video['video_title_ru'];}?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Описание</label>
                                <textarea name="description_ru" rows="4" class="form-control" placeholder="Описание"><?php if(!empty($video)){echo $video['video_description_ru'];}?></textarea>
								<p class="help-block">Не должно содержать более 500 символов.</p>
							</div>
						</div>
						<!-- /.tab-pane -->
						<div class="tab-pane" id="form_en">
                            <div class="form-group">
                                <label>Название</label>
                                <input class="form-control" placeholder="Название" name="title_en" value="<?php if(!empty($video)){echo $video['video_title_en'];}?>">
                                <p class="help-block">Не должно содержать более 250 символов.</p>
                            </div>
                            <div class="form-group">
                                <label>Описание</label>
                                <textarea name="description_en" rows="4" class="form-control" placeholder="Описание"><?php if(!empty($video)){echo $video['video_description_en'];}?></textarea>
                                <p class="help-block">Не должно содержать более 500 символов.</p>
                            </div>
						</div>
					</div>
					<!-- /.tab-content -->
				</div>
                <div class="form-group clearfix mb-0" id="video_image">
                    <?php if(!empty($video)){?>
                        <div class="user-image-thumbnail-wr mb-0 mr-0">
                            <div class="user-image-thumb" style="height:auto;">
                                <img src="<?php echo base_url('files/video/'.$video['video_image']);?>"/>
                            </div>
                            <a href="#" class="btn btn-danger btn-xs confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="Удалить" data-photo="<?php echo $video['video_image'];?>">
                                <span class="glyphicon glyphicon-remove-circle"></span>
                            </a>
                            <input type="hidden" name="video_image" value="<?php echo $video['video_image'];?>">
                        </div>
                    <?php }?>
                </div>
            </div>
            <div class="modal-footer">
                <p class="help-block text-left"><strong><small>(Мин. ширина: 600px, высота: 400px, не превышать 5Мб)</small></strong></p>
                <span class="btn btn-default btn-file pull-left">
                    <i class="fa fa-picture-o"></i>
                    Загрузить постер
                    <input id="select_photo" type="file" name="userfile">
                </span>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <?php if(!empty($video)){?>
                    <input type="hidden" name="id_video" value="<?php echo $video['id_video'];?>">
                    <button type="button" class="btn btn-success call-function" data-callback="manage_video" data-action="edit">Сохранить</button>
                <?php } else{?>
                    <button type="button" class="btn btn-success call-function" data-callback="manage_video" data-action="add">Сохранить</button>
                <?php }?>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
        'use strict';

        <?php if(!empty($video)){?>
            $('#datetimepicker').datetimepicker({
                locale: 'ru',
                format: 'DD.MM.YYYY HH:mm:ss',
                tooltips:{
                    today:"Сегодня",
                    clear:"Очистить",
                    close:"Закрыть",
                    selectMonth:"Выберите месяц",
                    prevMonth:"Пред. месяц",
                    nextMonth:"След. месяц",
                    selectYear:"Выберите год",
                    prevYear:"Пред. год",
                    nextYear:"След. год",
                    selectDecade:"Выберите десятилетие",
                    prevDecade:"Пред. десятилетие",
                    nextDecade:"След. десятилетие",
                    prevCentury:"Пред. век",
                    nextCentury:"След. век",
                    pickHour:"Выберите час",
                    incrementHour:"Добавить час",
                    decrementHour:"Убавить час",
                    pickMinute:"Выберите минуты",
                    incrementMinute:"Добавить минуту",
                    decrementMinute:"Убавить минуту",
                    pickSecond:"Выберите секунду",
                    incrementSecond:"Добавить секунду",
                    decrementSecond:"Убавить секунду",
                    togglePeriod:"Переключить период",
                    selectTime:"Выберите время"
                }
            });
        <?php }?>

		$('#select_photo').fileupload({
			url: base_url+'admin/video/ajax_operations/upload_photo',
			dataType: 'json',
			done: function (e, data) {
				if(data.result.mess_type == 'error'){
					systemMessages( data.result.message, data.result.mess_class );
				} else{
					$.each(data.result.files, function (index, file) {
						var template = '<div class="user-image-thumbnail-wr mb-0 mr-0">\
											<div class="user-image-thumb" style="height:auto;">\
												<img src="'+base_url+'files/video/'+file.filename+'"/>\
											</div>\
											<a href="#" class="btn btn-danger btn-xs confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="Удалить" data-photo="'+file.filename+'">\
												<span class="glyphicon glyphicon-remove-circle"></span>\
											</a>\
											<input type="hidden" name="video_image" value="'+file.filename+'">\
										</div>';

						if($('#video_image .user-image-thumbnail-wr').length > 0){
							var unused_photo = $('#video_image .user-image-thumbnail-wr').find('input[name="video_image"]').val();
							$('#video_image').closest('form').append('<input type="hidden" name="remove_photos[]" value="'+unused_photo+'"/>');
						}
						$('#video_image').html(template);
					});
				}
			}
		}).prop('disabled', !$.support.fileInput)
			.parent().addClass($.support.fileInput ? undefined : 'disabled');

		$('input.nice-input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%'
		});
    });

	var remove_photo = function(btn){
		var $this = $(btn);
		var photo = $this.data('photo');
		$this.closest('form').append('<input type="hidden" name="remove_photos[]" value="'+photo+'"/>');
		$this.closest('.user-image-thumbnail-wr').remove();
	}

    var manage_video = function(btn){
        var $this = $(btn);
        var action = $this.data('action');
        var $form = $this.closest('form');
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url();?>admin/video/ajax_operations/'+action,
            data: $form.serialize(),
            dataType: 'JSON',
            beforeSend: function(){
                clearSystemMessages();
                $this.addClass('disabled');
            },
            success: function(resp){
                systemMessages( resp.message, resp.mess_type );
                $this.removeClass('disabled');

                if(resp.mess_type == 'success'){
                    $('#general_popup_form').modal('hide');
                    $form.trigger( 'reset' );
					manage_video_callback();
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
        });

        return false;
    }
</script>