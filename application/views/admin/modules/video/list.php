<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
			<div class="box-tools pull-right">
				<a href="#" data-href="<?php echo base_url('admin/video/popup_forms/add');?>" class="btn btn-box-tool call-popup" data-popup="#general_popup_form">
					<i class="fa fa-plus"></i> Добавить видео
				</a>
			</div>
		</div>
		<!-- /.box-header -->		
		<div class="box-body">
			<table class="table table-bordered table-striped" id="dtTable">
				<thead>
					<tr>
						<th class="dt_photo">Постер</th>
						<th class="dt_name">Название</th>
						<th class="dt_cname">Категория</th>
						<th class="dt_type">Тип</th>
						<th class="dt_date">Дата</th>
						<th class="dt_actions">Операций</th>
					</tr>
				</thead>
			</table>
		</div>
		<!-- /.box-body -->
	</div>
</div>
<div class="modal fade" id="video_popup_form" data-keyboard="false" data-backdrop="static"></div>
<script>
	var dtFilter; //obj for filters
	var dtTable; //obj of datatable
	var video_players;
	$(function(){
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "/theme/admin/plugins/datatable/js/Russian.json"
			},
			"paging": true,
			"lengthChange": false,
			"searching": false,
			"ordering": true,
			"info": true,
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/video/ajax_operations/list_dt",
			"sServerMethod": "POST",
			"iDisplayLength": 100,
			"aoColumnDefs": [
				{ "sClass": "w-50 text-center vam_i", "aTargets": ["dt_photo"], "mData": "dt_photo", "bSortable": false},
				{ "sClass": "text-left vam_i", "aTargets": ["dt_name"], "mData": "dt_name"},
				{ "sClass": "text-left vam_i", "aTargets": ["dt_cname"], "mData": "dt_cname"},
				{ "sClass": "w-100 text-center vam_i", "aTargets": ["dt_type"], "mData": "dt_type"},
				{ "sClass": "w-130 text-center vam_i", "aTargets": ["dt_date"], "mData": "dt_date"},
				{ "sClass": "w-120 text-center vam_i", "aTargets": ["dt_actions"], "mData": "dt_actions" , "bSortable": false }

			],
			"aaSorting" : [],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type == 'error')
							systemMessages(data.message, 'message-' + data.mess_type);
						if(data.mess_type == 'info')
							systemMessages(data.message, 'message-' + data.mess_type);

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
				if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1) {
                    $('#dtTable_wrapper .dataTables_paginate').css("display", "block"); 
                    $('#dtTable_wrapper .dataTables_filter').css("display", "block"); 
                } else {
                    $('#dtTable_wrapper .dataTables_paginate').css("display", "none");
                    $('#dtTable_wrapper .dataTables_filter').css("display", "none");
                }
			}
		});

		$('#video_popup_form').on('hidden.bs.modal', function(){
			video_players[0].destroy();
			$(this).html('');
		});
	});

	function manage_video_callback(){
		dtTable.fnDraw(false);
	}

	var delete_action = function(btn){
		var $this = $(btn);
		var video = $this.data('video');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/video/ajax_operations/delete',
			data: {video:video},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}

	var change_status = function(btn){
		var $this = $(btn);
		var video = $this.data('video');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/video/ajax_operations/change_status',
			data: {video:video},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}

	var change_marketing = function(btn){
		var $this = $(btn);
		var video = $this.data('video');
		var marketing = $this.data('marketing');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/video/ajax_operations/change_marketing',
			data: {video:video,marketing:marketing},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}
	
</script>
