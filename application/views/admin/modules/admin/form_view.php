<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Редактирование</h4>
        </div>
        <form id="add_category_form" autocomplete="off">			
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label>Email <small>(используеться как логин)</small></label>
                            <input class="form-control" placeholder="Email" name="user_email" value="<?php if(!empty($manager)){echo $manager->user_email;}?>">
                            <p class="help-block mb-0">Не более 50 символов.</p>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label>Новый пароль</label>
                            <input class="form-control" type="password" placeholder="Новый пароль" name="user_password" value="">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label>Повторите новый пароль</label>
                            <input class="form-control" type="password" placeholder="Повторите новый пароль" name="confirm_password" value="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-success call-function" data-callback="manage_manager" data-action="edit">Сохранить</button>
            </div>
        </form>
    </div>
</div>
<script>
    var manage_manager = function(btn){
        var $this = $(btn);
        var action = $this.data('action');
        var $form = $this.closest('form');
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url();?>admin/ajax_operations/'+action,
            data: $form.serialize(),
            dataType: 'JSON',
            beforeSend: function(){
                clearSystemMessages();
                $this.addClass('disabled');
            },
            success: function(resp){
                systemMessages( resp.message, resp.mess_type );
                $this.removeClass('disabled');

                if(resp.mess_type == 'success'){
                    $('#general_popup_form').modal('hide');
                    $form.trigger( 'reset' );
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
        });
    }
</script>