<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php if(!empty($client)){echo 'Редактирование';} else{echo 'Добавление';}?> клиента</h4>
        </div>
        <form id="add_service_form">
            <div class="modal-body">
                <div class="form-group clearfix">
                    <p class="help-block fs-12"><strong>Размеры фото: 100 x 100, px</strong></p>
                    <span class="btn btn-default btn-sm btn-file pull-left">
                        <i class="fa fa-picture-o"></i>
                        Загрузить фото
                        <input id="select_photo" type="file" name="userfile">
                    </span>
                </div>
                <div class="form-group clearfix" id="client_photo">
                    <?php if(!empty($client)){?>
                        <div class="user-image-thumbnail-wr w-100 mb-0 mr-0">
                            <div class="user-image-thumb" style="height:auto;">
                                <img src="<?php echo base_url('files/clients/'.$client['client_photo']);?>"/>
                            </div>
                            <a href="#" class="btn btn-danger btn-xs confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="Удалить" data-photo="<?php echo $client['client_photo'];?>">
                                <span class="glyphicon glyphicon-remove-circle"></span>
                            </a>
                            <input type="hidden" name="client_photo" value="<?php echo $client['client_photo'];?>">
                        </div>
                    <?php }?>
                </div>
                <div class="form-group">
                    <label>Название</label>
                    <input class="form-control" placeholder="Название" name="title" value="<?php if(!empty($client)){echo $client['client_title'];}?>">
                    <p class="help-block">Не должно содержать более 250 символов.</p>
                </div>
                <div class="form-group">
                    <label>Ссылка</label>
                    <input class="form-control" placeholder="Ссылка" name="link" value="<?php if(!empty($client)){echo $client['client_link'];}?>">
                    <p class="help-block">Не должно содержать более 250 символов.</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <?php if(!empty($client)){?>
                    <div class="form-group pull-left w-185">
                        <div class="input-group date" id="datetimepicker">
                            <input type="text" class="form-control" name="client_published_date" value="<?php echo formatDate($client['client_published_date'], 'd.m.Y H:i:s');?>"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <input type="hidden" name="id_client" value="<?php echo $client['id_client'];?>">
                    <button type="button" class="btn btn-success call-function" data-callback="manage_client" data-action="edit">Сохранить</button>
                <?php } else{?>
                    <div class="form-group pull-left w-185">
                        <div class="input-group date" id="datetimepicker">
                            <input type="text" class="form-control" name="client_published_date" value="<?php echo date('d.m.Y H:i:s');?>"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <button type="button" class="btn btn-success call-function" data-callback="manage_client" data-action="add">Сохранить</button>
                <?php }?>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
        'use strict';        
		$('#select_photo').fileupload({
			url: base_url+'admin/clients/ajax_operations/upload_photo',
			dataType: 'json',
			done: function (e, data) {
				if(data.result.mess_type == 'error'){
					systemMessages( data.result.message, data.result.mess_class );
				} else{
					$.each(data.result.files, function (index, file) {
						var template = '<div class="user-image-thumbnail-wr w-100 mb-0 mr-0">\
											<div class="user-image-thumb" style="height:auto;">\
												<img src="'+base_url+'files/clients/'+file.filename+'"/>\
											</div>\
											<a href="#" class="btn btn-danger btn-xs confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="Удалить" data-photo="'+file.filename+'">\
												<span class="glyphicon glyphicon-remove-circle"></span>\
											</a>\
											<input type="hidden" name="client_photo" value="'+file.filename+'">\
										</div>';

						if($('#client_photo .user-image-thumbnail-wr').length > 0){
							var unused_photo = $('#client_photo .user-image-thumbnail-wr').find('input[name="client_photo"]').val();
							$('#client_photo').closest('form').append('<input type="hidden" name="remove_photos[]" value="'+unused_photo+'"/>');
						}
						$('#client_photo').html(template);
					});
				}
			}
		}).prop('disabled', !$.support.fileInput)
			.parent().addClass($.support.fileInput ? undefined : 'disabled');

        
        $('#datetimepicker').datetimepicker({
            locale: 'ru',
            format: 'DD.MM.YYYY HH:mm:ss',
            tooltips:{
                today:"Сегодня",
                clear:"Очистить",
                close:"Закрыть",
                selectMonth:"Выберите месяц",
                prevMonth:"Пред. месяц",
                nextMonth:"След. месяц",
                selectYear:"Выберите год",
                prevYear:"Пред. год",
                nextYear:"След. год",
                selectDecade:"Выберите десятилетие",
                prevDecade:"Пред. десятилетие",
                nextDecade:"След. десятилетие",
                prevCentury:"Пред. век",
                nextCentury:"След. век",
                pickHour:"Выберите час",
                incrementHour:"Добавить час",
                decrementHour:"Убавить час",
                pickMinute:"Выберите минуты",
                incrementMinute:"Добавить минуту",
                decrementMinute:"Убавить минуту",
                pickSecond:"Выберите секунду",
                incrementSecond:"Добавить секунду",
                decrementSecond:"Убавить секунду",
                togglePeriod:"Переключить период",
                selectTime:"Выберите время"
            }
        });
    });

	var remove_photo = function(btn){
		var $this = $(btn);
		var photo = $this.data('photo');
		$this.closest('form').append('<input type="hidden" name="remove_photos[]" value="'+photo+'"/>');
		$this.closest('.user-image-thumbnail-wr').remove();
	}

    var manage_client = function(btn){
        var $this = $(btn);
        var action = $this.data('action');
        var $form = $this.closest('form');
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url();?>admin/clients/ajax_operations/'+action,
            data: $form.serialize(),
            dataType: 'JSON',
            beforeSend: function(){
                clearSystemMessages();
                $this.addClass('disabled');
            },
            success: function(resp){
                systemMessages( resp.message, resp.mess_type );
                $this.removeClass('disabled');

                if(resp.mess_type == 'success'){
                    $('#general_popup_form').modal('hide');
                    $form.trigger( 'reset' );
					manage_clients_callback();
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
        });

        return false;
    }
</script>