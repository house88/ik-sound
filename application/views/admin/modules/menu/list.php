<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
			<div class="box-tools pull-right">
				<a href="<?php echo base_url('admin/menu/add');?>" class="btn btn-box-tool">
					<i class="fa fa-plus"></i> Добавить раздел
				</a>
			</div>
		</div>
		<!-- /.box-header -->			
        <div class="box-body">
            <div class="directory-search-key-b mt-15"></div>
            <table class="table table-bordered table-striped" id="dtTable">
                <thead>
                    <tr>
                        <th class="w-150 text-center dt_alias">Код</th>
                        <th class="dt_name">Название</th>
                        <th class="w-120 text-center dt_actions">Операций</th>
                    </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
	</div>
</div>
<script>
	var dtTable; //obj of datatable
	$(function(){
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "/theme/admin/plugins/datatable/js/Russian.json"
			},
			"paging": true,
			"lengthChange": false,
			"searching": false,
			"ordering": true,
			"info": true,
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/menu/ajax_operations/list_dt",
			"sServerMethod": "POST",
			"iDisplayLength": 100,
			"aoColumnDefs": [
				{ "sClass": "w-150 text-center vam", "aTargets": ["dt_alias"], "mData": "dt_alias", "bSortable": false},
				{ "sClass": "text-left vam", "aTargets": ["dt_name"], "mData": "dt_name"},
				{ "sClass": "w-120 text-center vam", "aTargets": ["dt_actions"], "mData": "dt_actions" , "bSortable": false }

			],
			"aaSorting" : [],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type == 'error')
							systemMessages(data.message, 'message-' + data.mess_type);
						if(data.mess_type == 'info')
							systemMessages(data.message, 'message-' + data.mess_type);

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {}
		});
	});
    var delete_action = function(btn){
        var $this = $(btn);
        var menu = $this.data('menu');
        $.ajax({
            type: 'POST',
            url: base_url+'admin/menu/ajax_operations/delete',
            data: {menu:menu},
            dataType: 'JSON',
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                if(resp.mess_type == 'success'){
                    dtTable.fnDraw(false);
                }
            }
        });
        return false;
    }
    var change_status = function(btn){
		var $this = $(btn);
		var menu = $this.data('menu');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/menu/ajax_operations/change_status',
			data: {menu:menu},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){					
                    dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}
</script>
