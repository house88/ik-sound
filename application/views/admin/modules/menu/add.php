<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
			<div class="box-tools pull-right">
				<a href="<?php echo base_url('admin/menu/add');?>" class="btn btn-box-tool">
					<i class="fa fa-plus"></i> Добавить меню
				</a>
			</div>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form id="add_form">			
			<div class="box-body">
				<div class="form-group">
					<label>Код</label>
					<input class="form-control" placeholder="Код" name="alias">
					<p class="help-block">Пример footer_menu.</p>
				</div>
				<div class="form-group">
					<label>Название меню RU</label>
					<input class="form-control" placeholder="Название RU" name="title_ru">
					<p class="help-block">Название не должно содержать более 100 символов.</p>
				</div>
				<div class="form-group">
					<label>Название меню RO</label>
					<input class="form-control" placeholder="Название RO" name="title_ro">
					<p class="help-block">Название не должно содержать более 100 символов.</p>
				</div>
				<div class="form-group">
					<label>Елементы меню</label>
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th class="text-center">Название RU</th>
								<th class="text-center">Название RO</th>
								<th class="text-center">Ссылка</th>
								<th class="text-center w-50"></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class=""><input class="form-control" placeholder="Название" name="menu_element_title_ru" value=""></td>
								<td class=""><input class="form-control" placeholder="Ссылка" name="menu_element_title_ro" value=""></td>
								<td class="">
									<select class="form-control" name="menu_element_link">
										<?php foreach($menu_links as $menu_link){?>
											<?php if(!empty($menu_link['elements'])){?>
												<optgroup label="<?php echo $menu_link['title'];?>">
													<?php foreach($menu_link['elements'] as $menu_link_element){?>
														<option value="<?php echo $menu_link_element['value'];?>" data-type="<?php echo $menu_link_element['type'];?>"><?php echo $menu_link_element['name'];?></option>
													<?php }?>
												</optgroup>
											<?php } else{?>
												<option value="<?php echo $menu_link['value'];?>" data-type="<?php echo $menu_link['type'];?>"><?php echo $menu_link['title'];?></option>
											<?php }?>
										<?php }?>
									</select>
								</td>
								<td class=""><span class="btn btn-primary btn-flat call-function" data-callback="add_element_row"><i class="fa fa-plus"></i> Добавить</span></td>
							</tr>
						</tbody>
					</table>
					<table class="table table-striped table-bordered" id="menu_values">
						<thead>
							<tr>
								<th class="text-left">Название RU</th>
								<th class="text-left">Название RO</th>
								<th class="text-center w-270"></th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<!-- /.box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-primary btn-flat">Сохранить</button>
			</div>
		</form>
	</div>
</div>

<script>
	var add_element_row = function(btn){
		var $this = $(btn);
		var $this_table = $this.closest('table');
		var menu_element_title_ru = $this_table.find('input[name="menu_element_title_ru"]').val();
		$this_table.find('input[name="menu_element_title_ru"]').val('');
		var menu_element_title_ro = $this_table.find('input[name="menu_element_title_ro"]').val();
		$this_table.find('input[name="menu_element_title_ro"]').val('');
		var $menu_element_link = $this_table.find('select[name="menu_element_link"] option:selected');
		$this_table.find('select[name="menu_element_link"] option:selected').prop("selected", false);
		var	menu_element_type = $menu_element_link.data('type');
		var menu_element_link_value = $menu_element_link.val();

		if(menu_element_title_ru == '' || menu_element_title_ro == ''){
			systemMessages('Поля Название RU, Название RO обязательны.', 'error');
			return false;
		}

		var new_row = '	<tr>\
							<td class="">'+menu_element_title_ru+'</td>\
							<td class="">'+menu_element_title_ro+'</td>\
							<td class="">\
								<input type="hidden" name="menu_elements[title_ru][]" value="'+menu_element_title_ru+'">\
								<input type="hidden" name="menu_elements[title_ro][]" value="'+menu_element_title_ro+'">\
								<input type="hidden" name="menu_elements[link_type][]" value="'+menu_element_type+'">\
								<input type="hidden" name="menu_elements[link_value][]" value="'+menu_element_link_value+'">\
								<div class="btn-group">\
									<span class="btn btn-default btn-flat call-function" data-callback="order_element_row" data-order="up"><i class="fa fa-caret-square-o-up"></i> В верх</span>\
									<span class="btn btn-default btn-flat call-function" data-callback="order_element_row" data-order="down"><i class="fa fa-caret-square-o-down"></i> В низ</span>\
									<span class="btn btn-danger btn-flat call-function" data-callback="delete_element_row"><i class="fa fa-trash"></i> Удалить</span>\
								</div>\
							</td>\
						</tr>';
		$('table#menu_values tbody').append(new_row);
	}

	var delete_element_row = function(btn){
		var $this = $(btn);
		$this.closest('tr').remove();
	}

	var order_element_row = function(btn){
		var $this = $(btn);
		var $element_row = $this.closest('tr');
		switch($this.data('order')){
			case 'up':
				var $prev_element_row = $element_row.prev();
				$element_row.insertBefore($prev_element_row);
			break;
			case 'down':
				var $next_element_row = $element_row.next();
				$element_row.insertAfter($next_element_row);
			break;
		}
	}

    $(function(){
        var add_form = $('#add_form');
        add_form.submit(function () {
            var fdata = add_form.serialize();
            $.ajax({
                type: 'POST',
                url: base_url+'admin/menu/ajax_operations/add',
                data: fdata,
                dataType: 'JSON',
                success: function(resp){
                    if(resp.mess_type == 'success'){
                        add_form.replaceWith('<div class="alert alert-success mb-0 ml-5 mr-5">'+resp.message+'</div>');
                    } else{
                        systemMessages(resp.message, resp.mess_type);
                    }
                }
            });
            return false;
        });
    });
</script>
