<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
		</div>
		<!-- /.box-header -->
			<!-- form start -->
		<form id="contacts_form">
			<div class="box-body">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#contacts_ru" data-toggle="tab" aria-expanded="true">Русский</a>
						</li>
						<li class="">
							<a href="#contacts_en" data-toggle="tab" aria-expanded="false">English</a>
						</li>
						<li class="">
							<a href="#contacts_address" data-toggle="tab" aria-expanded="false">Адреса</a>
						</li>
						<li class="">
							<a href="#contacts_social" data-toggle="tab" aria-expanded="false">Соц. сети</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="contacts_ru">
							<div class="form-group">
								<label>Текст</label>
								<textarea name="text_ru" class="description_custom"><?php echo $contacts['contacts_text_ru'];?></textarea>
							</div>
							<div class="form-group">
								<label>Meta keywords</label>
								<input class="form-control" type="text" name="mk_ru" value="<?php echo $contacts['mk_ru'];?>">
							</div>
							<div class="form-group">
								<label>Meta description</label>
								<input class="form-control" type="text" name="md_ru" value="<?php echo $contacts['md_ru'];?>">
							</div>
						</div>
						<!-- /.tab-pane -->
						<div class="tab-pane" id="contacts_en">
							<div class="form-group">
								<label>Текст</label>
								<textarea name="text_en" class="description_custom"><?php echo $contacts['contacts_text_en'];?></textarea>
							</div>
							<div class="form-group">
								<label>Meta keywords</label>
								<input class="form-control" type="text" name="mk_en" value="<?php echo $contacts['mk_en'];?>">
							</div>
							<div class="form-group">
								<label>Meta description</label>
								<input class="form-control" type="text" name="md_en" value="<?php echo $contacts['md_en'];?>">
							</div>
						</div>
						<div class="tab-pane" id="contacts_address">
							<table class="table table-striped" id="contacts_address_values">
								<thead>
									<tr>
										<th class="w-150">Тип</th>
										<th class="w-150">Название</th>
										<th>Значение</th>
										<th class="w-300">Ссылка</th>
										<th class="w-130 text-center">Активен</th>
										<th class="w-130 text-center">В нижнем колонтитуле</th>
										<th class="w-130"></th>
									</tr>
								</thead>
								<tbody>
									<?php $addresses = !empty($contacts['contacts_addresses']) ? json_decode($contacts['contacts_addresses'], true) : array();?>
									<?php if(!empty($addresses)){?>
										<?php foreach($addresses as $address_key => $address){?>
											<tr>
												<td>
													<select name="address[<?php echo $address_key;?>][icon]" class="form-control">
														<option value="ik ik-envelope" <?php echo set_select('address['.$address_key.'][icon]', 'ik ik-envelope', 'ik ik-envelope' == $address['icon'])?>>Email</option>
														<option value="ik ik-skype" <?php echo set_select('address['.$address_key.'][icon]', 'ik ik-skype', 'ik ik-skype' == $address['icon'])?>>Skype</option>
														<option value="ik ik-discord" <?php echo set_select('address['.$address_key.'][icon]', 'ik ik-discord', 'ik ik-discord' == $address['icon'])?>>Discord</option>
														<option value="ik ik-telegram" <?php echo set_select('address['.$address_key.'][icon]', 'ik ik-telegram', 'ik ik-telegram' == $address['icon'])?>>Telegram</option>
														<option value="ik ik-whatsapp" <?php echo set_select('address['.$address_key.'][icon]', 'ik ik-whatsapp', 'ik ik-whatsapp' == $address['icon'])?>>Whatsapp</option>
														<option value="ik ik-slack" <?php echo set_select('address['.$address_key.'][icon]', 'ik ik-slack', 'ik ik-slack' == $address['icon'])?>>Slack</option>
													</select>
												</td>
												<td>
													<input type="text" class="form-control" name="address[<?php echo $address_key;?>][name]" value="<?php echo $address['name'];?>">
												</td>
												<td>
													<input type="text" class="form-control" name="address[<?php echo $address_key;?>][value]" value="<?php echo $address['value'];?>">
												</td>
												<td>
													<input type="text" class="form-control" name="address[<?php echo $address_key;?>][link]" value="<?php echo $address['link'];?>">
												</td>
												<td class="text-center">
													<input 
														type="checkbox" 
														name="address[<?php echo $address_key;?>][is_visible]" 
														<?php echo set_checkbox('address['. $address_key .'][is_visible]', '', $address['is_visible'])?> >
												</td>
												<td class="text-center">
													<input 
														type="checkbox" 
														name="address[<?php echo $address_key;?>][in_footer]" 
														<?php echo set_checkbox('address['. $address_key .'][in_footer]', '', $address['in_footer'])?> >
												</td>
												<td>
													<div class="input-group-btn">
														<span class="btn btn-default call-function" data-callback="change_weight_value" data-order="up"><i class="fa fa-caret-square-o-up"></i></span>
														<span class="btn btn-default call-function" data-callback="change_weight_value" data-order="down"><i class="fa fa-caret-square-o-down"></i></span>
														<span class="btn btn-danger confirm-dialog" data-callback="remove_value" data-message="Вы точно хотите удалить это значение?" data-dtype="danger" data-title="Удалить"><i class="fa fa-trash"></i></span>
													</div>
												</td>
											</tr>
										<?php }?>
									<?php }?>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="3">
											<button type="button" class="btn btn-default call-function pull-left" data-callback="add_value"><i class="fa fa-plus"></i> Добавить адрес</button>
										</td>
									</tr>									
								</tfoot>
							</table>
						</div>
						<div class="tab-pane" id="contacts_social">
							<table class="table table-striped" id="contacts_socials_values">
								<thead>
									<tr>
										<th class="w-150">Тип</th>
										<th class="w-150">Название</th>
										<th>Ссылка</th>
										<th class="w-130 text-center">Активен</th>
										<th class="w-130"></th>
									</tr>
								</thead>
								<tbody>
									<?php $socials = !empty($contacts['contacts_socials']) ? json_decode($contacts['contacts_socials'], true) : array();?>
									<?php if(!empty($socials)){?>
										<?php foreach($socials as $social_key => $social){?>
											<tr>
												<td>
													<select name="socials[<?php echo $social_key;?>][icon]" class="form-control">
														<option value="ik ik-social-youtube" <?php echo set_select('socials['.$social_key.'][icon]', 'ik ik-social-youtube', 'ik ik-social-youtube' == $social['icon'])?>>Youtube</option>
														<option value="ik ik-social-fb" <?php echo set_select('socials['.$social_key.'][icon]', 'ik ik-social-fb', 'ik ik-social-fb' == $social['icon'])?>>Facebook</option>
														<option value="ik ik-social-vk" <?php echo set_select('socials['.$social_key.'][icon]', 'ik ik-social-vk', 'ik ik-social-vk' == $social['icon'])?>>Вконтакте</option>
														<option value="ik ik-social-soundcloud" <?php echo set_select('socials['.$social_key.'][icon]', 'ik ik-social-soundcloud', 'ik ik-social-soundcloud' == $social['icon'])?>>SoundCloud</option>
														<option value="ik ik-social-twitter" <?php echo set_select('socials['.$social_key.'][icon]', 'ik ik-social-twitter', 'ik ik-social-twitter' == $social['icon'])?>>Twitter</option>
														<option value="ik ik-social-instagram" <?php echo set_select('socials['.$social_key.'][icon]', 'ik ik-social-instagram', 'ik ik-social-instagram' == $social['icon'])?>>Instagram</option>
													</select>
												</td>
												<td>
													<input type="text" class="form-control" name="socials[<?php echo $social_key;?>][name]" value="<?php echo $social['name'];?>">
												</td>
												<td>
													<input type="text" class="form-control" name="socials[<?php echo $social_key;?>][link]" value="<?php echo $social['link'];?>">
												</td>
												<td class="text-center">
													<input 
														type="checkbox" 
														name="socials[<?php echo $social_key;?>][is_visible]" 
														<?php echo set_checkbox('socials['. $social_key .'][is_visible]', '', $social['is_visible'])?> >
												</td>
												<td>
													<div class="input-group-btn">
														<span class="btn btn-default call-function" data-callback="change_weight_value" data-order="up"><i class="fa fa-caret-square-o-up"></i></span>
														<span class="btn btn-default call-function" data-callback="change_weight_value" data-order="down"><i class="fa fa-caret-square-o-down"></i></span>
														<span class="btn btn-danger confirm-dialog" data-callback="remove_value" data-message="Вы точно хотите удалить это значение?" data-dtype="danger" data-title="Удалить"><i class="fa fa-trash"></i></span>
													</div>
												</td>
											</tr>
										<?php }?>
									<?php }?>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="3">
											<button type="button" class="btn btn-default call-function pull-left" data-callback="add_social_value"><i class="fa fa-plus"></i> Добавить адрес</button>
										</td>
									</tr>									
								</tfoot>
							</table>
						</div>
					</div>
					<!-- /.tab-content -->
				</div>
			</div>
				<!-- /.box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-primary">Сохранить</button>
			</div>
		</form>
	</div>
</div>

<script type="text/template" id="js--address-template">
	<tr>
		<td>
			<select name="address[{{index}}][icon]" class="form-control">
				<option value="ik ik-envelope">Email</option>
				<option value="ik ik-skype">Skype</option>
				<option value="ik ik-discord">Discord</option>
				<option value="ik ik-telegram">Telegram</option>
				<option value="ik ik-whatsapp">Whatsapp</option>
				<option value="ik ik-slack">Slack</option>
			</select>
		</td>
		<td>
			<input type="text" class="form-control" name="address[{{index}}][name]">
		</td>
		<td>
			<input type="text" class="form-control" name="address[{{index}}][value]">
		</td>
		<td>
			<input type="text" class="form-control" name="address[{{index}}][link]">
		</td>
		<td class="text-center">
			<input type="checkbox" name="address[{{index}}][is_visible]" checked>
		</td>
		<td class="text-center">
			<input type="checkbox" name="address[{{index}}][in_footer]" checked>
		</td>
		<td>
			<div class="input-group-btn">
				<span class="btn btn-default call-function" data-callback="change_weight_value" data-order="up"><i class="fa fa-caret-square-o-up"></i></span>
				<span class="btn btn-default call-function" data-callback="change_weight_value" data-order="down"><i class="fa fa-caret-square-o-down"></i></span>
				<span class="btn btn-danger confirm-dialog" data-callback="remove_value" data-message="Вы точно хотите удалить это значение?" data-dtype="danger" data-title="Удалить"><i class="fa fa-trash"></i></span>
			</div>
		</td>
	</tr>
</script>

<script type="text/template" id="js--social-template">
	<tr>
		<td>
			<select name="socials[{{index}}][icon]" class="form-control">
				<option value="ik ik-social-youtube">Youtube</option>
				<option value="ik ik-social-fb">Facebook</option>
				<option value="ik ik-social-vk">Вконтакте</option>
				<option value="ik ik-social-soundcloud">SoundCloud</option>
				<option value="ik ik-social-twitter">Twitter</option>
				<option value="ik ik-social-instagram">Instagram</option>
			</select>
		</td>
		<td>
			<input type="text" class="form-control" name="socials[{{index}}][name]">
		</td>
		<td>
			<input type="text" class="form-control" name="socials[{{index}}][link]">
		</td>
		<td class="text-center">
			<input type="checkbox" name="socials[{{index}}][is_visible]" checked>
		</td>
		<td>
			<div class="input-group-btn">
				<span class="btn btn-default call-function" data-callback="change_weight_value" data-order="up"><i class="fa fa-caret-square-o-up"></i></span>
				<span class="btn btn-default call-function" data-callback="change_weight_value" data-order="down"><i class="fa fa-caret-square-o-down"></i></span>
				<span class="btn btn-danger confirm-dialog" data-callback="remove_value" data-message="Вы точно хотите удалить это значение?" data-dtype="danger" data-title="Удалить"><i class="fa fa-trash"></i></span>
			</div>
		</td>
	</tr>
</script>

<script>
	var change_checked_btn = function(element){
		var $this = $(element);
		var $inner_checkbox = $this.find('input[type="checkbox"]');
		if($inner_checkbox.is(":checked")){
			$inner_checkbox.prop('checked', false);
			$this.toggleClass('btn-success btn-warning');
			$this.find('.fa').toggleClass('fa-eye fa-eye-slash');
		} else{
			$inner_checkbox.prop('checked', true);
			$this.toggleClass('btn-warning btn-success');
			$this.find('.fa').toggleClass('fa-eye-slash fa-eye');
		}
	}

	var add_value = function(btn){
        var $this = $(btn);
		var template = $('#js--address-template').text();
		var index = uniqid('address_');
		$('#contacts_address_values tbody').append($(template.replace(/{{index}}/g, index)));
		theme_checkboxes();
    }

	var add_social_value = function(btn){
        var $this = $(btn);
		var template = $('#js--social-template').text();
		var index = uniqid('social_');
		$('#contacts_socials_values tbody').append($(template.replace(/{{index}}/g, index)));
		theme_checkboxes();
    }

    var remove_value = function(btn){
        var $this = $(btn);
        var $parent_row = $this.closest('tr');
        $parent_row.remove();
    }
    
    var change_weight_value = function(btn){
		var $this = $(btn);
        var $element_row = $this.closest('tr');
		switch($this.data('order')){
			case 'up':
				var $prev_element_row = $element_row.prev();
				$element_row.insertBefore($prev_element_row);
			break;
			case 'down':
				var $next_element_row = $element_row.next();
				$element_row.insertAfter($next_element_row);
			break;
		}
	}
	
	$(function(){
		tinymce.init({
			relative_urls: false,
			selector: ".description_custom",
			height: 500,
			plugins: [
				"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
				"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
				"save table contextmenu directionality emoticons template paste textcolor responsivefilemanager"
			],
			toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | fontsizeselect | link unlink anchor | responsivefilemanager image media | forecolor backcolor  | preview fullpage code",
			image_advtab: true ,
			content_css : "/theme/ik-sound/fonts/ik/style.css,/theme/ik-sound/css/tinymce_custom_css.css",
			fontsize_formats: '8px 10px 12px 14px 16px 18px 20px 22px 24px 36px',
			external_filemanager_path:"/theme/admin/plugins/filemanager/",
			filemanager_title:"Responsive Filemanager" ,
			external_plugins: { "filemanager" : "/theme/admin/plugins/filemanager/plugin.min.js"},
			language_url : '/theme/admin/plugins/tinymce/langs/ru.js',
			language : 'ru'
		});
	});
	var contacts_form = $('#contacts_form');
	contacts_form.submit(function () {
		tinyMCE.triggerSave();
		var fdata = contacts_form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/ajax_operations/save_contacts',
			data: fdata,
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
			}
		});
		return false;
	});
</script>