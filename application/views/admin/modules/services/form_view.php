<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php if(!empty($service)){echo 'Редактирование';} else{echo 'Добавление';}?> услуги</h4>
        </div>
        <form id="add_service_form">
            <div class="modal-body">
                <div class="form-group clearfix">
                    <p class="help-block fs-12"><strong>Размеры иконки: 100 x 100, px</strong></p>
                    <span class="btn btn-default btn-sm btn-file pull-left">
                        <i class="fa fa-picture-o"></i>
                        Загрузить иконку
                        <input id="select_photo" type="file" name="userfile">
                    </span>
                </div>
                <div class="form-group clearfix" id="service_photo">
                    <?php if(!empty($service)){?>
                        <div class="user-image-thumbnail-wr w-100 mb-0 mr-0">
                            <div class="user-image-thumb" style="height:auto;">
                                <img src="<?php echo base_url('files/services/'.$service['service_photo']);?>"/>
                            </div>
                            <a href="#" class="btn btn-danger btn-xs confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="Удалить" data-photo="<?php echo $service['service_photo'];?>">
                                <span class="glyphicon glyphicon-remove-circle"></span>
                            </a>
                            <input type="hidden" name="service_photo" value="<?php echo $service['service_photo'];?>">
                        </div>
                    <?php }?>
                </div>
                <div class="nav-tabs-custom no-shadow mb-0">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#form_ru" data-toggle="tab" aria-expanded="true">Русский</a>
						</li>
						<li class="">
							<a href="#form_en" data-toggle="tab" aria-expanded="false">English</a>
						</li>
					</ul>
					<div class="tab-content pl-0_i pr-0_i">
						<div class="tab-pane active" id="form_ru">
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_ru" value="<?php if(!empty($service)){echo $service['service_title_ru'];}?>">
								<p class="help-block">Не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group mb-0">
								<label>Описание</label>
                                <textarea name="description_ru" rows="4" class="form-control" placeholder="Описание"><?php if(!empty($service)){echo $service['service_description_ru'];}?></textarea>
								<p class="help-block mb-0">Не должно содержать более 1000 символов.</p>
							</div>
						</div>
						<!-- /.tab-pane -->
						<div class="tab-pane" id="form_en">
                            <div class="form-group">
                                <label>Название</label>
                                <input class="form-control" placeholder="Название" name="title_en" value="<?php if(!empty($service)){echo $service['service_title_en'];}?>">
                                <p class="help-block">Не должно содержать более 250 символов.</p>
                            </div>
                            <div class="form-group mb-0">
                                <label>Описание</label>
                                <textarea name="description_en" rows="4" class="form-control" placeholder="Описание"><?php if(!empty($service)){echo $service['service_description_en'];}?></textarea>
                                <p class="help-block mb-0">Не должно содержать более 1000 символов.</p>
                            </div>
                        </div>
					</div>
					<!-- /.tab-content -->
				</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <?php if(!empty($service)){?>
                    <input type="hidden" name="id_service" value="<?php echo $service['id_service'];?>">
                    <button type="button" class="btn btn-success call-function" data-callback="manage_service" data-action="edit">Сохранить</button>
                <?php } else{?>
                    <button type="button" class="btn btn-success call-function" data-callback="manage_service" data-action="add">Сохранить</button>
                <?php }?>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
        'use strict';        
		$('#select_photo').fileupload({
			url: base_url+'admin/services/ajax_operations/upload_photo',
			dataType: 'json',
			done: function (e, data) {
				if(data.result.mess_type == 'error'){
					systemMessages( data.result.message, data.result.mess_class );
				} else{
					$.each(data.result.files, function (index, file) {
						var template = '<div class="user-image-thumbnail-wr w-100 mb-0 mr-0">\
											<div class="user-image-thumb" style="height:auto;">\
												<img src="'+base_url+'files/services/'+file.filename+'"/>\
											</div>\
											<a href="#" class="btn btn-danger btn-xs confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="Удалить" data-photo="'+file.filename+'">\
												<span class="glyphicon glyphicon-remove-circle"></span>\
											</a>\
											<input type="hidden" name="service_photo" value="'+file.filename+'">\
										</div>';

						if($('#service_photo .user-image-thumbnail-wr').length > 0){
							var unused_photo = $('#service_photo .user-image-thumbnail-wr').find('input[name="service_photo"]').val();
							$('#service_photo').closest('form').append('<input type="hidden" name="remove_photos[]" value="'+unused_photo+'"/>');
						}
						$('#service_photo').html(template);
					});
				}
			}
		}).prop('disabled', !$.support.fileInput)
			.parent().addClass($.support.fileInput ? undefined : 'disabled');
    });

	var remove_photo = function(btn){
		var $this = $(btn);
		var photo = $this.data('photo');
		$this.closest('form').append('<input type="hidden" name="remove_photos[]" value="'+photo+'"/>');
		$this.closest('.user-image-thumbnail-wr').remove();
	}

    var manage_service = function(btn){
        var $this = $(btn);
        var action = $this.data('action');
        var $form = $this.closest('form');
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url();?>admin/services/ajax_operations/'+action,
            data: $form.serialize(),
            dataType: 'JSON',
            beforeSend: function(){
                clearSystemMessages();
                $this.addClass('disabled');
            },
            success: function(resp){
                systemMessages( resp.message, resp.mess_type );
                $this.removeClass('disabled');

                if(resp.mess_type == 'success'){
                    $('#general_popup_form').modal('hide');
                    $form.trigger( 'reset' );
					manage_services_callback();
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                systemMessages( 'Ошибка: Запрос не может быть отправлено. Попробуйте позже.', 'error' );
                jqXHR.abort();
            }
        });

        return false;
    }
</script>