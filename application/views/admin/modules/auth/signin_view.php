<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Панель управления</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url();?>theme/admin/plugins/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url();?>theme/admin/fonts/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url();?>theme/admin/fonts/ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url();?>theme/admin/css/AdminLTE.css">
    <link rel="stylesheet" href="<?php echo base_url();?>theme/admin/css/skins/skin-blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url();?>theme/admin/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url();?>theme/admin/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>theme/admin/js/scripts.js"></script>
    <script type="text/javascript">
        var base_url = '<?php echo base_url();?>';
    </script>
</head>
<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a><b>Панель</b> управления</a>
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">Авторизация</p>            
            <form id="signin_form">
                <div class="form-group has-feedback">
                    <input type="text" name="user_email" class="form-control" placeholder="Email">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="password" class="form-control mb-0" placeholder="Пароль">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Войти</button>
                    </div>
                </div>
            </form>
            <script>
                $(function(){
                    $('#signin_form').on('submit', function(e){
                        var $form = $(this);
                        var fdata = $form.serialize();

                        $.ajax({
                            type: 'POST',
                            url: base_url+'admin/ajax_operations/signin',
                            data: fdata,
                            dataType: 'JSON',
                            beforeSend: function(){
                                
                            },
                            success: function(resp){
                                if(resp.mess_type == 'success'){
                                    window.location.href = base_url + resp.redirect;
                                } else{
                                    // systemMessages(resp.message, resp.mess_type);;
                                }
                            }
                        });
                        return false;
                    });
                });
            </script>
        </div>
    </div>
</body>
</html>
