<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Панель управления</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url();?>theme/ik-sound/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>theme/ik-sound/css/sizes.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="<?php echo base_url();?>theme/ik-sound/js/jquery.js"></script>
    <script src="<?php echo base_url();?>theme/ik-sound/js/bootstrap.js"></script>
    <script>
        var base_url = '<?php echo base_url();?>';
    </script>
</head>
<body>
    <div class="auth-box">
        <!-- /.login-logo -->
        <div class="auth-box-body">
            <?php $this->load->view($main_content); ?>
        </div>
    </div>
    <!-- /.login-box -->
</body>
</html>
