<header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url();?>admin" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Admin</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li>
                            <a href="<?php echo base_url();?>">
                                <i class="fa fa-home"></i> Перейти на сайт
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('admin/signout');?>">
                                <i class="fa fa-sign-out"></i> Выход
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
            </ul>        
        </div>
    </nav>
</header>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" id="side-menu">
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Панель управления</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url();?>admin"><i class="fa fa-cogs"></i> <span>Настройки</span></a></li>
                    <li><a href="<?php echo base_url();?>admin/translations"><i class="fa fa-globe"></i> <span>Переводы</span></a></li>
                    <li><a href="<?php echo base_url();?>admin/page_meta"><i class="fa fa-globe"></i> <span>Meta страниц</span></a></li>
                    <li>
                        <a href="#" class="call-popup" data-popup="#general_popup_form" data-href="<?php echo base_url('admin/popup_forms/edit');?>">
                            <i class="fa fa-lock"></i> <span>Личные данные</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li><a href="<?php echo base_url();?>admin/blog"><i class="fa fa-newspaper-o"></i> <span>Блог</span></a></li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-file-video-o"></i> <span>Видео</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url();?>admin/video/categories"><i class="fa fa-th-list"></i> <span>Категории</span></a></li>
                    <li><a href="<?php echo base_url();?>admin/video"><i class="fa fa-file-video-o"></i> <span>Видео</span></a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-file-audio-o"></i> <span>Аудио</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url();?>admin/audio/properties"><i class="fa fa-th-list"></i> <span>Свойства</span></a></li>
                    <li><a href="<?php echo base_url();?>admin/audio"><i class="fa fa-file-audio-o"></i> <span>Аудио</span></a></li>
                </ul>
            </li>
            <li><a href="<?php echo base_url();?>admin/services"><i class="fa fa-file-text"></i> <span>Услуги</span></a></li>
            <li><a href="<?php echo base_url();?>admin/clients"><i class="fa fa-users"></i> <span>Клиенты</span></a></li>
            <li><a href="<?php echo base_url();?>admin/contacts"><i class="fa fa-address-book"></i> <span>Контакты</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>