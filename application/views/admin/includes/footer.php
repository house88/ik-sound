<div class="system-text-messages-b" style="<?php if(empty($system_messages)){echo 'display: none;';}?>">	
	<div class="text-b">
		<ul>
			<?php if(!empty($system_messages)){echo $system_messages;}?>
		</ul>
	</div>
</div>

<footer class="main-footer">
	<strong>Copyright &copy; 2014-2017.</strong> All rights reserved.
</footer>

<div class="modal fade" id="general_popup_form" data-keyboard="false" data-backdrop="static"></div>