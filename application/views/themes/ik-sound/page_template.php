<!DOCTYPE html>
<html lang="en">
<head>
    <?php $this->load->view(get_theme_view('include/head')); ?>
</head>
<body>
    <div class="wrapper">
        <?php $this->load->view(get_theme_view('include/header')); ?>
    
        <div class="js-page-wr">
            <?php $this->load->view(get_theme_view($main_content)); ?>
        </div>
    </div>

    <?php $this->load->view(get_theme_view('include/footer')); ?>
</body>
</html>