<section class="content-wr">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <article class="article-post-wr">
                    <div class="article-date">
                        <span class="ik ik-calendar"></span>
                        <span><?php echo format_date_i18n($blog['blog_date'], 'j F Y', $lang);?></span>
                    </div>
                    <div class="article-title">
                        <h1>
                            <?php echo $blog[lang_column('blog_title')];?>
                        </h1>
                    </div>
                    <div class="row">
                        <?php $blog_audio_files = json_decode($blog['blog_audio'], true);?>
                        <div class="col-xs-12">
                            <div class="article-text article-padding">
                                <?php echo $blog[lang_column('blog_description')];?>
                            </div>
                            <div class="article-share">
                                <ul class="share-list">
                                    <li class="share-list_item">
                                        <a href="#" class="share-list_item-link call-function" data-callback="popup_share" data-title="<?php echo $blog[lang_column('blog_title')];?>" data-url="<?php echo current_url();?>" data-social="facebook">
                                            <span class="ik ik-share-facebook share-list_item-link-icon"></span>
                                            <span class="share-list_item-link-text">Share</span>
                                        </a>
                                    </li>
                                    <li class="share-list_item">
                                        <a href="#" class="share-list_item-link call-function" data-callback="popup_share" data-title="<?php echo $blog[lang_column('blog_title')];?>" data-url="<?php echo current_url();?>" data-social="twitter">
                                            <span class="ik ik-share-twitter share-list_item-link-icon"></span>
                                            <span class="share-list_item-link-text">Share</span>
                                        </a>
                                    </li>
                                    <li class="share-list_item">
                                        <a href="#" class="share-list_item-link call-function" data-callback="popup_share" data-title="<?php echo $blog[lang_column('blog_title')];?>" data-url="<?php echo current_url();?>" data-social="linkedin">
                                            <span class="ik ik-share-linkedin share-list_item-link-icon"></span>
                                            <span class="share-list_item-link-text">Share</span>
                                        </a>
                                    </li>
                                    <li class="share-list_item">
                                        <a href="#" class="share-list_item-link call-function" data-callback="popup_share" data-title="<?php echo $blog[lang_column('blog_title')];?>" data-url="<?php echo current_url();?>" data-social="vk">
                                            <span class="ik ik-share-vk share-list_item-link-icon"></span>
                                            <span class="share-list_item-link-text">Share</span>
                                        </a>
                                    </li>
                                </ul>
                                <!-- <div class="sharethis-inline-share-buttons"></div> -->
                            </div>
                            <?php $blog_playlist = array();?>
                            <?php if(!empty($blog_audio_files)){?>
                                <div class="article-audio">
                                    <div class="article-audio-stick">
                                        <div class="article-audio-scroll">
                                            <ul class="article-playlist">
                                                <?php $blog_audio_index = 0;?>
                                                <?php foreach($blog_audio_files as $blog_audio_file){?>
                                                    <li>
                                                        <a href="#" class="call-function" data-index="<?php echo $blog_audio_index;?>" data-play="blog-audio" data-callback="play_audio" data-song="<?php echo site_url('files/blog/audio/'.$blog_audio_file['name']);?>" data-title="<?php echo $blog_audio_file['title'];?>">
                                                            <span class="ik ik-play-circle-o"></span>
                                                            <?php echo $blog_audio_file['title'];?>
                                                        </a>
                                                    </li>

                                                    <?php 
                                                        $blog_playlist[] = array(
                                                            "title" => clean_output($blog_audio_file['title']),
                                                            "type" => "audio",
                                                            "sources" => array(
                                                                array(
                                                                    "src" => site_url('files/blog/audio/'.$blog_audio_file['name']),
                                                                    "type" => "audio/mp3"
                                                                )
                                                            )
                                                        );

                                                        $blog_audio_index++;
                                                    ?>
                                                <?php }?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>                   
                            <?php }?>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
    
    <script>
        custom_playlist = <?php echo json_encode($blog_playlist);?>;
        $(function(){
            if($(window).width() > 1000){
                $('.article-audio-scroll').css({"max-height":$('.article-text').height()});
                $(".article-audio-stick").stick_in_parent({
                    parent: ".article-post-wr",
                    offset_top:85
                })
                .on("sticky_kit:stick", function(e) {
                    $('.article-audio-scroll').scrollbox('update');
                })
                .on("sticky_kit:unstick", function(e) {
                    $('.article-audio-scroll').scrollbox('update');
                });
                
                $('.article-audio-scroll').scrollbox('update');
            }
            

        });

        $(window).resize(function(){
            if($(window).width() > 1000){
                $('.article-audio-scroll').scrollbox('update');
                $(".article-audio-stick").stick_in_parent({
                    parent: ".article-post-wr",
                    offset_top:85
                })
                .on("sticky_kit:stick", function(e) {
                    $('.article-audio-scroll').scrollbox('update');
                })
                .on("sticky_kit:unstick", function(e) {
                    $('.article-audio-scroll').scrollbox('update');
                });
            } else{
                $('.article-audio-scroll').scrollbox('destroy');
                $(".article-audio-stick").trigger("sticky_kit:detach");
            }
        });
    </script>
</section>
