<section class="content-wr">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="blogs-wr">
                    <div class="blogs-list">
                        <?php $this->load->view(get_theme_view('modules/blog/blogs_list_view')); ?>
                    </div>
                    <div id="list-pagination-wr">
                        <ul class="pagination pagination-sm"></ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var page = get_page();
        var limit = intval('<?php echo $limit;?>');
        var records_total = intval('<?php echo $records_total;?>');
        var get_records = true;
        var page_url = '<?php echo site_url('blog');?>';
        var page_title = '<?php echo $stitle;?>';
        $(function(){
            get_pagination();
        });
        
        function get_page(){
            var page = intval('<?php echo $page;?>');
            var whash = window.location.hash;
            if(whash.match(/^#page-[0-9]+$/i)){
                page = intval(whash.replace("#page-", ''));
            }
            
            return (page)?page:1;
        }
        
        function get_blogs(){
            $.ajax({
                type: 'POST',
                url: page_url,
                data: {limit:limit,page:page},
                dataType: 'JSON',
                beforeSend: function(){},
                success: function(resp){
                    if(resp.mess_type == 'success'){
                        $('.blogs-list').html(resp.content);
                        records_total = resp.records_total;
                        page_title = resp.stitle;
                        get_pagination();
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    jqXHR.abort();
                }
            });
        }
        
        function get_pagination(){
            $('#list-pagination-wr > ul').pagination({
                items: records_total,
                itemsOnPage: limit,
                currentPage: page,
                cssStyle: '',
                prevText: '<span class="ik ik-arrow-left"></span>',
                nextText: '<span class="ik ik-arrow-right"></span>',
                onPageClick: function (p, evt) {
                    evt.preventDefault();
                    page = p;
                    push_state();
                    get_blogs();
                }
            });
        
            var total_pages = $('#list-pagination-wr > ul').pagination('getPagesCount');
            if(total_pages <= 1){
                $('#list-pagination-wr').hide();
            } else{
                $('#list-pagination-wr').show();
            }
        }

        function push_state(){
            var state = {};
            var url = page_url;

            if(page > 1){
                state['page'] = page;
                url += '/page/'+page;
            }
            
            state['url'] = url;
            header_menu(url);
            history.pushState(state, page_title, url);
        }
    </script>
</section>
