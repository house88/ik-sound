<?php if(!empty($first_blog)){?>
    <div class="row">
        <div class="col-xs-12">
            <div class="first-blog">
                <a class="first-blog-link call-function" data-callback="render_page" href="<?php echo site_url('blog/post/'.$first_blog[lang_column('url')]);?>" >                    
                    <div class="col-xs-12 col-sm-6 col-md-8 blog-image" style="background-image:url(<?php echo site_url('files/blog/'.$first_blog['blog_photo']);?>);"></div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <span class="blog-title"><?php echo $first_blog[lang_column('blog_title')];?></span>
                        <span class="blog-description"><?php echo $first_blog[lang_column('blog_small_description')];?></span>
                        <span class="blog-date">
                            <span class="ik ik-calendar"></span>
                            <span><?php echo format_date_i18n($first_blog['blog_date'], 'j F Y', $lang);?></span>    
                        </span>
                    </div>
                </a>
            </div>
        </div>
    </div>
<?php }?>
<div class="row">
    <?php if(!empty($blogs)){?>
        <?php foreach($blogs as $blog){?>
            <div class="col-xs-12 col-sm-6 col-md-4 blog-item">
                <a class="blog-link call-function" data-callback="render_page" href="<?php echo site_url('blog/post/'.$blog[lang_column('url')]);?>" >
                    <span class="blog-image" style="background-image:url(<?php echo site_url('files/blog/'.$blog['blog_photo']);?>);"></span>
                    <span class="blog-title"><?php echo $blog[lang_column('blog_title')];?></span>
                    <span class="blog-description"><?php echo $blog[lang_column('blog_small_description')];?></span>
                    <span class="blog-date">
                        <span class="ik ik-calendar"></span>
                        <span><?php echo format_date_i18n($blog['blog_date'], 'j F Y', $lang);?></span>    
                    </span>
                </a>
            </div>                            
        <?php }?>    
    <?php } else{?>
        <div class="col-xs-12 lh-40">
            Нет данных.
        </div>
    <?php }?>
</div>