<section class="content-wr">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="portfolio-wr">
                    <?php if(!empty($properties)){?>
                        <div class="properties-wr">
                            <div class="row">
                                <?php foreach($properties as $property){?>
                                    <?php $property_values = json_decode($property['property_values'], true);?>
                                    <?php if(!empty($property_values)){?>
                                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                                            <div class="property-wr">
                                                <div class="property-label"><?php echo $property[lang_column('property_title')];?></div>
                                                <div class="property-form-element">
                                                    <div class="dropdown">
                                                        <button class="btn btn-default btn-block dropdown-toggle" type="button" id="property_<?php echo $property['id_property'];?>" data-toggle="dropdown"><?php echo $property['selected_value_text'];?></button>
                                                        <ul class="dropdown-menu" aria-labelledby="property_<?php echo $property['id_property'];?>">
                                                            <li><a href="#" class="default_filter call-function" data-callback="set_audio_filter" data-property="<?php echo $property['id_property'];?>" data-value=""><?php lang_title('select_default_value_all');?></a></li>                                                            
                                                            <?php foreach($property_values as $id_property_value => $property_value){?>
                                                                <li><a href="#" class="call-function" data-callback="set_audio_filter" data-property="<?php echo $property['id_property'];?>" data-value="<?php echo $id_property_value;?>"><?php echo $property_value[lang_column('title')];?></a></li>                                                            
                                                            <?php }?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php }?>
                                <?php }?>
                                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                                    <div class="property-wr">
                                        <a href="#" class="clear-filters call-function" type="button" data-callback="clear_filters"><span class="ik ik-times"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>                            
                    <?php }?>
                    <div class="portfolio-list">
                        <?php $this->load->view(get_theme_view('modules/audio/portfolio_list_view')); ?>
                    </div>
                    <div id="list-pagination-wr">
                        <ul class="pagination pagination-sm"></ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var page = get_page();
        var limit = intval('<?php echo $limit;?>');
        var records_total = intval('<?php echo $records_total;?>');
        var get_records = true;
        var page_url = '<?php echo site_url('portfolio');?>';
        var filters = JSON.parse('<?php echo json_encode($selected_filters);?>');
        var page_title = '<?php echo $stitle;?>';
        $(function(){
            get_pagination();

            if(Object.keys(filters).length > 0){
                $('[data-callback="clear_filters"]').addClass('active');
            }
        });
        
        function get_page(){
            var page = intval('<?php echo $page;?>');
            var whash = window.location.hash;
            if(whash.match(/^#page-[0-9]+$/i)){
                page = intval(whash.replace("#page-", ''));
            }
            
            return (page)?page:1;
        }
        
        function get_projects(){
            $.ajax({
                type: 'POST',
                url: '<?php echo site_url('portfolio');?>',
                data: {limit:limit,page:page, filters:filters},
                dataType: 'JSON',
                beforeSend: function(){},
                success: function(resp){
                    if(resp.mess_type == 'success'){
                        $('.portfolio-list').html(resp.content);
                        records_total = resp.records_total;
                        page_title = resp.stitle;
                        get_pagination();
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    jqXHR.abort();
                }
            });
        }
        
        function get_pagination(){
            $('#list-pagination-wr > ul').pagination({
                items: records_total,
                itemsOnPage: limit,
                currentPage: page,
                cssStyle: '',
                prevText: '<span class="ik ik-arrow-left"></span>',
                nextText: '<span class="ik ik-arrow-right"></span>',
                onPageClick: function (p, evt) {
                    evt.preventDefault();
                    page = p;
                    push_state();
                    get_projects();
                }
            });
        
            var total_pages = $('#list-pagination-wr > ul').pagination('getPagesCount');
            if(total_pages <= 1){
                $('#list-pagination-wr').hide();
            } else{
                $('#list-pagination-wr').show();
            }
        }

        function push_state(){
            var state = {};
            var url = page_url;
            
            if(Object.keys(filters).length > 0){
                var filters_values = Object.values(filters).join(':');
                state['filters'] = filters_values;
                url += '/filters/'+filters_values;
            }

            if(page > 1){
                state['page'] = page;
                url += '/page/'+page;
            }
            
            state['url'] = url;
            header_menu(url);
            history.pushState(state, page_title, url);
        }

        var set_audio_filter = function(btn){
            var $this = $(btn);
            var property = intval($this.data('property'));
            var value = intval($this.data('value'));
            var text = $this.text();

            if(value > 0){
                filters[property] = property+'_'+value;
            } else{
                delete filters[property];
            }

            $this.closest('.dropdown').find('button.dropdown-toggle').text(text);
            $this.closest('.dropdown').find('button.dropdown-toggle').click();

            if(Object.keys(filters).length > 0){
                $('[data-callback="clear_filters"]').addClass('active');
            }

            page = 1;
            push_state();
            get_projects();
        }

        var clear_filters = function(btn){
            var $this = $(btn);
            var text = '';
            $('.default_filter').each(function( index ) {
                text = $(this).text();
            });
            
            filters = {};

            $('[data-callback="clear_filters"]').removeClass('active');
            $this.closest('.properties-wr').find('button.dropdown-toggle').text(text);

            page = 1;
            push_state();
            get_projects();
        }
    </script>
</section>
