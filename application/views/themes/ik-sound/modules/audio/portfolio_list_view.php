<div class="row">
    <?php if(!empty($projects)){?>
        <?php foreach($projects as $project){?>
            <div class="col-xs-12 col-sm-6 col-md-3 portfolio-item">
                <?php if(!empty($project['audio_file'])){?>
                    <a class="item-image call-function" data-play="projects" data-callback="play_audio" data-song="<?php echo site_url('files/audio/'.$project['audio_file']);?>" data-title="<?php echo $project['audio_client_name'];?>" style="background-image:url(<?php echo site_url('files/audio/poster/'.$project['audio_image']);?>);">
                        <span class="play-audio"></span>
                        <span class="play-title-text"><?php echo $project[lang_column('audio_title')];?></span>
                        <?php if(array_key_exists($project['id_audio'], $type_of_works)){?>
                            <span class="play-category-text"><?php echo implode(', ', $type_of_works[$project['id_audio']]);?></span>                    
                        <?php }?>
                    </a>
                <?php } else{?>
                    <span class="item-image no-play" data-title="<?php echo $project[lang_column('audio_title')];?>" style="background-image:url(<?php echo site_url('files/audio/poster/'.$project['audio_image']);?>);">
                        <span class="play-audio"></span>
                        <span class="play-title-text"><?php echo $project[lang_column('audio_title')];?></span>
                        <?php if(array_key_exists($project['id_audio'], $type_of_works)){?>
                            <span class="play-category-text"><?php echo implode(', ', $type_of_works[$project['id_audio']]);?></span>                    
                        <?php }?>
                    </span>
                <?php }?>
            </div>                            
        <?php }?>    
    <?php } else{?>
        <div class="col-xs-12 lh-40">
            <?php lang_line('no_data');?>
        </div>
    <?php }?>
</div>