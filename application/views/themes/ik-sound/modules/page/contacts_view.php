<section class="content-wr">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <article class="page-wr contacts-page">
                    <div class="page-header">
                        <img src="<?php echo site_url('files/uploads/page/contacts-header.jpg');?>" alt="Contacts">
                    </div>
                    
                    <div class="page-inside-wr">
                        <div class="row">
                            <div class="col-xs-12 col-md-7">
                                <div class="page-text">
                                    <?php echo $contacts[lang_column('contacts_text')];?>
                                </div>
                            </div>
                            <?php $addresses = !empty($contacts['contacts_addresses']) ? json_decode($contacts['contacts_addresses'], true) : array();?>
                            <?php $socials = !empty($contacts['contacts_socials']) ? json_decode($contacts['contacts_socials'], true) : array();?>
                            <?php if(!empty($addresses) || !empty($socials)){?>
                                <div class="col-xs-12 col-md-5">
                                    <div class="contacts-block">
                                        <div class="contacts-header">
                                            <?php lang_title('section_contacts_header');?>
                                        </div>
                                        <div class="contacts-wr">
                                            <?php if(!empty($addresses)){?>
                                                <?php foreach($addresses as $address){?>
                                                    <?php if($address['is_visible'] == false){continue;}?>
                                                    
                                                    <?php if(empty($address['link'])){?>
                                                        <span class="contact-wr" href="<?php echo $address['link'];?>">
                                                            <span class="<?php echo $address['icon'];?>"></span>
                                                            <span class="contact-item"><?php echo $address['value'];?></span>
                                                        </span>
                                                    <?php } else{?>
                                                        <a class="contact-wr" href="<?php echo $address['link'];?>">
                                                            <span class="<?php echo $address['icon'];?>"></span>
                                                            <span class="contact-item"><?php echo $address['value'];?></span>
                                                        </a>
                                                    <?php }?>
                                                <?php }?>
                                            <?php }?>

                                            <?php if(!empty($addresses) && !empty($socials)){?>
                                                <div class="divider"></div>
                                            <?php }?>

                                            <?php if(!empty($socials)){?>
                                                <div class="social-wr">
                                                    <?php foreach($socials as $social){?>
                                                        <?php if($social['is_visible'] == false){continue;}?>
                                                        
                                                        <a href="<?php echo $social['link'];?>" target="_blank" class="social_link">
                                                            <span class="<?php echo $social['icon'];?>"></span>
                                                        </a>
                                                    <?php }?>
                                                </div>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                            <?php }?>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>