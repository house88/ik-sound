<section id="home-carousel-slider">
    <template>
        <carousel-3d :controls-visible="true" :controls-prev-html="''" :controls-next-html="''" :controls-width="30" :controls-height="60" :width="555" :max-height="350" :space="270">
            <slide v-for="(header_slide, i) in header_slides" :index="i">
                <figure>
                    <img :src="header_slide.image">
                    <figcaption v-bind:class="{'pr-15_i': canPlay(header_slide)}">
                        <div class="title">{{header_slide.title}}</div>
                        <div class="text">{{header_slide.description}}</div>
                        <a href="#" class="play call-function" data-play="banner-slide" data-callback="play_audio" v-if="header_slide.play_btn" :data-song="header_slide.audio" :data-title="header_slide.title">
                            <span class="play-audio ik"></span>
                            <span><?php lang_line('button_audio_play_text');?></span>
                        </a>
                    </figcaption>
                </figure>
            </slide>
        </carousel-3d>
    </template>
</section>
<script>
    var slider_elements = JSON.parse('<?php echo Modules::run('audio/_get_home_banner_slides');?>');
</script>
<script src="<?php echo base_url();?>theme/ik-sound/js/vue-apps.js"></script>

<?php $services = Modules::run('services/_get_services', array('service_active' => 1));?>
<?php if(!empty($services)){?>
    <section class="services">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="section-title">
                        <span class="text"><?php lang_line('section_services_header');?></span>
                    </h1>
                </div>
                <?php foreach($services as $service){?>
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        <div class="service">
                            <div class="icon" style="background-image: url('<?php echo site_url('files/services/'.$service['service_photo']);?>');"></div>
                            <div class="title"><?php echo $service[lang_column('service_title')];?></div>
                            <div class="text"><?php echo $service[lang_column('service_description')];?></div>
                        </div>
                    </div>                
                <?php }?>
            </div>
        </div>
    </section>
<?php }?>

<section class="projects" id="projects">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="section-title">
                    <span class="text"><?php lang_line('section_projects_header');?></span>
                </h1>

                <div class="projects-counter">
                    <div class="projects-counter-wr">
                        <span class="counter" id="countup-projects" data-target="countup-projects" data-number="<?php echo $settings['portfolio_finished'][lang_column('setting_value')];?>" data-count>0</span>
                        <span class="text"><?php lang_line('section_projects_counter_text');?></span>
                    </div>
                </div>
                <script>
                    $(function(){
                        var waypoint = new Waypoint({
                            element: document.getElementById('projects'),
                            handler: function(direction) {    
                                if (direction === 'down') {
                                    countAnimation();
                                }
                            },
                            offset: '80%',
                            triggerOnce:true
                        });
                    });
                </script>
            </div>
        </div>
    </div>
    <div class="portfolio-wr">
        <?php $audio_projects = Modules::run('audio/_get_home_projects');?>
        <?php $video_projects = Modules::run('video/_get_home_videos');?>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="thumbs audio_projects">
                        <?php foreach($audio_projects as $audio_project){?>
                            <li>
                                <?php if($audio_project['play_btn']){?>
                                    <a href="#" class="thumbnail call-function" data-play="home-projects" data-callback="play_audio" data-song="<?php echo $audio_project['audio'];?>" data-title="<?php echo $audio_project['title'];?>" style="background-image: url('<?php echo $audio_project['image'];?>')">
                                        <span class="play-audio"></span>
                                        <span class="play-title-text"><?php echo $audio_project['title'];?></span>
                                        <span class="play-category-text"><?php echo $audio_project['type_of_works'];?></span>
                                    </a>
                                <?php } else{?>
                                    <span class="thumbnail no-play" data-title="<?php echo $audio_project['title'];?>" style="background-image: url('<?php echo $audio_project['image'];?>')">
                                        <span class="play-audio"></span>
                                        <span class="play-title-text"><?php echo $audio_project['title'];?></span>
                                        <span class="play-category-text"><?php echo $audio_project['type_of_works'];?></span>
                                    </span>
                                <?php }?>
                            </li>            
                        <?php }?>
                    </ul>
                </div>
            </div>
        </div>
        <?php if(!empty($video_projects)){?>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="thumbs video_projects">
                            <?php foreach($video_projects as $video_project){?>
                                <li class="video-item">
                                    <a href="#" class="thumbnail call-function" data-callback="play_video" style="background-image: url('<?php echo $video_project['image'];?>')">
                                        <span class="play-video"></span>
                                        <span class="play-text"><?php lang_line('button_video_play_text')?></span>
                                    </a>
                                    <button class="play-video-btn js-<?php echo $video_project['type'];?>-video-button" data-video-id="<?php echo $video_project['code'];?>"></button>
                                </li>            
                            <?php }?>
                        </ul>
                    </div>
                </div>
            </div>
        <?php }?>
    </div>
    <script>
        $(function(){
            $('.portfolio-wr .audio_projects').portfolio({
                cols: 5,
                transition: 'slideDown'
            });
            $('.portfolio-wr .video_projects').portfolio({
                cols: 3,
                transition: 'slideDown'
            });
            $(".js-youtube-video-button").modalVideo({
                youtube:{
                    autoplay: true,
                    nocookie: true
                }
            });
            $(".js-vimeo-video-button").modalVideo({
                channel:'vimeo',
                vimeo:{
                    autoplay: true,
                    nocookie: true
                }
            });
        });
    </script>
</section>

<?php $clients = Modules::run('clients/_get_clients', array('client_active' => 1));?>
<?php if(!empty($clients)){?>
    <section class="clients">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="section-title">
                        <span class="text"><?php lang_line('section_our_clients_header');?></span>
                    </h1>
                    <script>
                        $(function(){
                            $('#home-clients-slider').slick({
                                prevArrow: '<a href="" class="slick-prev"></a>',
                                nextArrow: '<a href="" class="slick-next"></a>',
                                slidesToShow: 5,
                                slidesToScroll: 1,
                                autoplay: true,
                                autoplaySpeed: 2000,
                                infinite: true,
                                responsive: [
                                {
                                    breakpoint: 1024,
                                    settings: {
                                        slidesToShow: 5,
                                        slidesToScroll: 1,
                                        autoplay: true,
                                        autoplaySpeed: 2000,
                                        infinite: true,
                                    }
                                },
                                {
                                    breakpoint: 600,
                                    settings: {
                                        arrows: false,
                                        centerMode: true,
                                        slidesToShow: 3,
                                        slidesToScroll: 1,
                                        autoplay: true,
                                        autoplaySpeed: 2000,
                                        infinite: true,
                                    }
                                },
                                {
                                    breakpoint: 480,
                                    settings: {
                                        arrows: false,
                                        centerMode: true,
                                        slidesToShow: 2,
                                        slidesToScroll: 1,
                                        autoplay: true,
                                        autoplaySpeed: 2000,
                                        infinite: true,
                                    }
                                }]
                            });
                        });
                    </script>
                    <div id="home-clients-slider">
                        <?php foreach($clients as $client){?>
                            <div>
                                <a href="<?php echo $client['client_link'];?>" target="_blank">
                                    <img src="<?php echo site_url('files/clients/'.$client['client_photo']);;?>" alt="<?php echo $client['client_title'];?>" width="100px">
                                </a>    
                            </div>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php }?>