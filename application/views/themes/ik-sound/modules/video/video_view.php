<section class="content-wr">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="video-wr">
                    <?php if(!empty($categories)){?>
                        <div class="categories-wr">
                            <span class="btn btn-default category-item <?php if($selected_category == 0){echo 'active';}?> call-function" data-callback="reset_category"><?php lang_title('select_default_value_all');?></span>
                            <?php foreach($categories as $category){?>
                                <span class="btn btn-default category-item call-function <?php if($selected_category == $category['id_category']){echo 'active';}?>" data-callback="set_category" data-category="<?php echo $category['id_category'];?>"><?php echo $category[lang_column('category_title')];?></span>
                            <?php }?>
                        </div>                            
                    <?php }?>
                    <div class="video-list">
                        <?php $this->load->view(get_theme_view('modules/video/video_list_view')); ?>
                    </div>
                    <div id="list-pagination-wr">
                        <ul class="pagination pagination-sm"></ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var page = get_page();
        var limit = intval('<?php echo $limit;?>');
        var records_total = intval('<?php echo $records_total;?>');
        var get_records = true;
        var page_url = '<?php echo site_url('video');?>';
        var page_title = '<?php echo $stitle;?>';
        var category = intval('<?php echo $selected_category;?>');
        $(function(){
            get_pagination();
        });
        
        function get_page(){
            var page = intval('<?php echo $page;?>');
            var whash = window.location.hash;
            if(whash.match(/^#page-[0-9]+$/i)){
                page = intval(whash.replace("#page-", ''));
            }
            
            return (page)?page:1;
        }
        
        function get_videos(){
            $.ajax({
                type: 'POST',
                url: page_url,
                data: {limit:limit,page:page,category:category},
                dataType: 'JSON',
                beforeSend: function(){},
                success: function(resp){
                    if(resp.mess_type == 'success'){
                        $('.video-list').html(resp.content);
                        records_total = resp.records_total;
                        page_title = resp.stitle;
                        get_pagination();
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    jqXHR.abort();
                }
            });
        }
        
        function get_pagination(){
            $('#list-pagination-wr > ul').pagination({
                items: records_total,
                itemsOnPage: limit,
                currentPage: page,
                cssStyle: '',
                prevText: '<span class="ik ik-arrow-left"></span>',
                nextText: '<span class="ik ik-arrow-right"></span>',
                onPageClick: function (p, evt) {
                    evt.preventDefault();
                    page = p;
                    push_state();
                    get_videos();
                }
            });
        
            var total_pages = $('#list-pagination-wr > ul').pagination('getPagesCount');
            if(total_pages <= 1){
                $('#list-pagination-wr').hide();
            } else{
                $('#list-pagination-wr').show();
            }
        }

        function push_state(){
            var state = {};
            var url = page_url;

            if(page > 1){
                state['page'] = page;
                url += '/page/'+page;
            }

            if(category > 0){
                state['category'] = category;
                url += '/category/'+category;
            }
            
            state['url'] = url;
            header_menu(url);
            history.pushState(state, page_title, url);
        }

        var set_category = function(btn){
            var $this = $(btn);
            category = intval($this.data('category'));
            $this.closest('.categories-wr').find('.category-item').removeClass('active');
            $this.addClass('active');

            page = 1;
            push_state();
            get_videos();
        }

        var reset_category = function(btn){
            var $this = $(btn);
            category = 0;
            $this.closest('.categories-wr').find('.category-item').removeClass('active');
            $this.addClass('active');

            page = 1;
            push_state();
            get_videos();
        }
    </script>
</section>
