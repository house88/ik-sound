<div class="row">
    <?php if(!empty($videos)){?>
        <?php foreach($videos as $video){?>            
            <div class="col-xs-12 col-sm-6 col-md-4 video-item">
                <a class="video-link call-function" data-callback="play_video">
                    <span class="video-image" style="background-image:url(<?php echo site_url('files/video/'.$video['video_image']);?>);">
                        <span class="play-video"></span>
                    </span>
                    <span class="video-title"><?php echo $video[lang_column('video_title')];?></span>
                    <span class="video-description"><?php echo $video[lang_column('video_description')];?></span>
                </a>
                <button class="play-video-btn js-<?php echo $video['video_type'];?>-video-button" data-video-id="<?php echo $video['video_code'];?>"></button>
            </div> 
        <?php }?>    
    <?php } else{?>
        <div class="col-xs-12 lh-40">
            Нет данных.
        </div>
    <?php }?>
    <script>
        $(function(){
            $(".js-youtube-video-button").modalVideo({
                youtube:{
                    autoplay: true,
                    nocookie: true
                }
            });
            $(".js-vimeo-video-button").modalVideo({
                channel:'vimeo',
                vimeo:{
                    autoplay: true,
                    nocookie: true
                }
            });
        });
    </script>
</div>