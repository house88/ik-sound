<div class="content-wr">
    <div class="container">
        <div class="error error__404">
            <h1 class="h1-title">
                <?php echo $error_page[lang_column('header')];?>
            </h1>
            <p class="error__404-message"><?php echo $error_page[lang_column('text')];?></p>
        </div>
    </div>
</div>