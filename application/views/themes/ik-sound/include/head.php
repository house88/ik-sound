<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111450701-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-111450701-1');
</script>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="Cravciuc Andrei - Senior Deweloper">
<base href="<?php echo base_url();?>">

<link rel="apple-touch-icon" sizes="57x57" href="<?php echo file_modification_time('theme/ik-sound/favicon/apple-icon-57x57.png');?>">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo file_modification_time('theme/ik-sound/favicon/apple-icon-60x60.png');?>">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo file_modification_time('theme/ik-sound/favicon/apple-icon-72x72.png');?>">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo file_modification_time('theme/ik-sound/favicon/apple-icon-76x76.png');?>">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo file_modification_time('theme/ik-sound/favicon/apple-icon-114x114.png');?>">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo file_modification_time('theme/ik-sound/favicon/apple-icon-120x120.png');?>">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo file_modification_time('theme/ik-sound/favicon/apple-icon-144x144.png');?>">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo file_modification_time('theme/ik-sound/favicon/apple-icon-152x152.png');?>">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo file_modification_time('theme/ik-sound/favicon/apple-icon-180x180.png');?>">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo file_modification_time('theme/ik-sound/favicon/android-icon-192x192.png');?>">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo file_modification_time('theme/ik-sound/favicon/favicon-32x32.png');?>">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo file_modification_time('theme/ik-sound/favicon/favicon-96x96.png');?>">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo file_modification_time('theme/ik-sound/favicon/favicon-16x16.png');?>">
<link rel="manifest" href="<?php echo file_modification_time('theme/ik-sound/favicon/manifest.json');?>">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo file_modification_time('theme/ik-sound/favicon/ms-icon-144x144.png');?>">
<meta name="theme-color" content="#ffffff">

<!-- og_meta -->
<meta property="og:type" content="website" />
<meta property="og:site_name" content="<?php echo @$settings['default_title'][lang_column('setting_value')];?>">
<meta property="og:title" content="<?php echo @$og_meta[lang_column('page_title')];?>" />
<meta property="og:description" content="<?php echo @$og_meta[lang_column('page_md')];?>" />
<meta property="og:url" content="<?php echo current_url();?>" />
<meta property="og:image" content="<?php echo @$og_meta['page_image_src'];?>" />

<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="<?php echo @$settings['default_title'][lang_column('setting_value')];?>">
<meta name="twitter:description" content="<?php echo @$og_meta[lang_column('page_md')];?>">
<meta name="twitter:url" content="<?php echo current_url();?>">
<meta name="twitter:domain" content="<?php echo base_url();?>">
<meta name="twitter:site" content="@<?php echo @$settings['default_title'][lang_column('setting_value')];?>">
<meta name="twitter:image" content="<?php echo @$og_meta['page_image_src'];?>">

<!-- og_meta END -->

<title><?php echo ((isset($stitle))?$stitle:$settings['default_title'][lang_column('setting_value')]);?></title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="keywords" content="<?php echo ((isset($skeywords))?$skeywords:$settings['mk'][lang_column('setting_value')]);?>">
<meta name="description" content="<?php echo ((isset($sdescription))?$sdescription:$settings['md'][lang_column('setting_value')]);?>">

<script>
    var site_url = '<?php echo site_url();?>';
    var site_lang_url = '<?php echo base_url($lang);?>';
    var site_lang = '<?php echo $lang;?>';
    var playlist = JSON.parse('<?php echo Modules::run('audio/_get_playlist');?>');
    var playlist_lenght = Object.keys(playlist).length;
    var default_play = <?php echo $settings['default_playlist'][lang_column('setting_value')];?>;
</script>
<script src="<?php echo file_modification_time('theme/ik-sound/js/jquery.js');?>"></script>
<script src="<?php echo file_modification_time('theme/ik-sound/js/jquery.mobile.custom.min.js');?>"></script>
<script src="<?php echo file_modification_time('theme/ik-sound/js/bootstrap.min.js');?>"></script>

<script src="<?php echo file_modification_time('theme/ik-sound/plugins/plyr/plyr.js');?>"></script>
<link href="<?php echo file_modification_time('theme/ik-sound/plugins/plyr/plyr.css');?>" rel="stylesheet">
<script src="<?php echo file_modification_time('theme/ik-sound/js/plyr.player.js');?>"></script>

<script src="<?php echo file_modification_time('theme/ik-sound/plugins/portfolio/portfolio.js');?>"></script>
<link href="<?php echo file_modification_time('theme/ik-sound/plugins/portfolio/portfolio.css');?>" rel="stylesheet">

<script src="<?php echo file_modification_time('theme/ik-sound/plugins/countup/countUp.js');?>"></script>
<script src="<?php echo file_modification_time('theme/ik-sound/plugins/waypoints/waypoints.js');?>"></script>

<script src="<?php echo file_modification_time('theme/ik-sound/plugins/vue/vue.min.js');?>"></script>
<script src="<?php echo file_modification_time('theme/ik-sound/plugins/vue/vue-carousel-3d.min.js');?>"></script>

<link href="<?php echo file_modification_time('theme/ik-sound/plugins/slick/slick.css');?>" rel="stylesheet" type="text/css">
<link href="<?php echo file_modification_time('theme/ik-sound/plugins/slick/slick-theme.css');?>" rel="stylesheet" type="text/css">
<script src="<?php echo file_modification_time('theme/ik-sound/plugins/slick/slick.js');?>"></script>

<link rel="stylesheet" href="<?php echo file_modification_time('theme/ik-sound/plugins/modal-video/css/modal-video.min.css');?>">
<script src="<?php echo file_modification_time('theme/ik-sound/plugins/modal-video/js/jquery-modal-video.min.js');?>"></script>

<link rel="stylesheet" href="<?php echo file_modification_time('theme/ik-sound/plugins/scrollbox/css/scrollbox.css');?>">
<script src="<?php echo file_modification_time('theme/ik-sound/plugins/scrollbox/js/scrollbox.js');?>"></script>
<script src="<?php echo file_modification_time('theme/ik-sound/plugins/mousewheel/jquery.mousewheel.min.js');?>"></script>

<script src="<?php echo file_modification_time('theme/ik-sound/plugins/sticky-kit/sticky-kit.js');?>"></script>
<script src="<?php echo file_modification_time('theme/ik-sound/plugins/pagination/jquery.pagination.js');?>"></script>

<link href="<?php echo file_modification_time('theme/ik-sound/css/font-awesome/css/font-awesome.css');?>" rel="stylesheet">

<link href="<?php echo file_modification_time('theme/ik-sound/fonts/ik/style.css');?>" rel="stylesheet">
<link href="<?php echo file_modification_time('theme/ik-sound/css/style.css');?>" rel="stylesheet">
<link href="<?php echo file_modification_time('theme/ik-sound/css/animate.css');?>" rel="stylesheet">
<link href="<?php echo file_modification_time('theme/ik-sound/css/sizes.css');?>" rel="stylesheet">
<script src="<?php echo file_modification_time('theme/ik-sound/js/scripts.js');?>"></script>