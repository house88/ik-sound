<header class="header-wr">
    <div class="header-navbar">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <nav class="navbar-ik">
                        <a class="navbar-ik-logo" href="<?php echo site_url($lang);?>"></a>
                        <?php
                            $uri_ru = (isset($uri_ru))?$uri_ru:'ru';
                            $uri_en = (isset($uri_en))?$uri_en:'en';
                        ?>
                        <ul class="navbar-ik-langs on-mobile">
                            <li class="dropdown lang-dropdown active">
                                <a class="btn btn-default navbar-ik-lang dropdown-toggle active" href="#" id="lang_switch_dropdown" type="button" data-toggle="dropdown">
                                    <?php echo $lang;?>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right lang_switch_dropdown" aria-labelledby="lang_switch_dropdown">
                                    <?php if($lang == 'ru'){?>
                                        <li>
                                            <?php echo anchor($this->lang->switch_uri('en', $uri_en), $this->lang->lang_name('en'), array('class' => 'navbar-ik-lang switch-lang-en call-function', 'data-callback' => 'switch_lang', 'data-lang' => 'en', 'target' => '_self'));?>
                                        </li>
                                    <?php } else{?>
                                        <li>
                                            <?php echo anchor($this->lang->switch_uri('ru', $uri_ru), $this->lang->lang_name('ru'), array('class' => 'navbar-ik-lang switch-lang-ru call-function', 'data-callback' => 'switch_lang', 'data-lang' => 'ru', 'target' => '_self'));?>
                                        </li>
                                    <?php }?>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a class="btn btn-default navbar-ik-menu dropdown-toggle" id="header_menu_dropdown" type="button" data-toggle="dropdown">
                                    <span class="ik ik-menu"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right header_menu_dropdown" aria-labelledby="header_menu_dropdown">
                                    <li><a class="call-function" data-callback="render_page" href="<?php echo site_url($lang);?>"><?php lang_line('header_menu_general_home');?></a></li>
                                    <li><a class="call-function" data-callback="render_page" href="<?php echo site_url('portfolio');?>"><?php lang_line('header_menu_general_portfolio');?></a></li>
                                    <li><a class="call-function" data-callback="render_page" href="<?php echo site_url('video');?>"><?php lang_line('header_menu_general_video');?></a></li>
                                    <?php if($settings['blog_is_active'][lang_column('setting_value')] == 1){?>
                                        <li><a class="call-function" data-callback="render_page" href="<?php echo site_url('blog');?>"><?php lang_line('header_menu_general_blog');?></a></li>                                    
                                    <?php }?>
                                    <li><a class="call-function" data-callback="render_page" href="<?php echo site_url('contacts');?>"><?php lang_line('header_menu_general_contacts');?></a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="navbar-ik-langs">
                            <li class="<?php echo ($lang == 'ru')?'active':'';?>">
                                <?php echo anchor($this->lang->switch_uri('ru', $uri_ru), $this->lang->lang_name('ru'), array('class' => 'navbar-ik-lang switch-lang-ru call-function', 'data-callback' => 'switch_lang', 'data-lang' => 'ru', 'target' => '_self'));?>
                            </li>
                            <li class="<?php echo ($lang == 'en')?'active':'';?>">
                                <?php echo anchor($this->lang->switch_uri('en', $uri_en), $this->lang->lang_name('en'), array('class' => 'navbar-ik-lang switch-lang-en call-function', 'data-callback' => 'switch_lang', 'data-lang' => 'en', 'target' => '_self'));?>
                            </li>
                        </ul>
                        <ul class="navbar-ik-links">
                            <li><a class="navbar-ik-link call-function" data-callback="render_page" href="<?php echo site_url($lang);?>"><?php lang_line('header_menu_general_home')?></a></li>
                            <li><a class="navbar-ik-link call-function" data-callback="render_page" href="<?php echo site_url('portfolio');?>"><?php lang_line('header_menu_general_portfolio')?></a></li>
                            <li><a class="navbar-ik-link call-function" data-callback="render_page" href="<?php echo site_url('video');?>"><?php lang_line('header_menu_general_video')?></a></li>
                            <?php if($settings['blog_is_active'][lang_column('setting_value')] == 1){?>
                                <li><a class="navbar-ik-link call-function" data-callback="render_page" href="<?php echo site_url('blog');?>"><?php lang_line('header_menu_general_blog')?></a></li>
                            <?php }?>
                            <li><a class="navbar-ik-link call-function" data-callback="render_page" href="<?php echo site_url('contacts');?>"><?php lang_line('header_menu_general_contacts')?></a></li>
                        </ul>

                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>