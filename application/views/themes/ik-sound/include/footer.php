<footer>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="site_name">&copy; <?php echo $settings['default_title'][lang_column('setting_value')];?>, 2017</div>
            </div>
            <?php $addresses = Modules::run('page/_get_footer_contacts_address');?>
            <?php if(!empty($addresses)){?>
                <div class="col-xs-12 col-sm-6 contacts-wr">
                    <?php foreach($addresses as $address){?>
                        <?php if(!empty($address['link'])){?>
                            <a class="contact_email" href="<?php echo $address['link'];?>"><?php echo $address['value'];?></a>
                        <?php } else{?>
                            <span class="contact_email"><?php echo $address['value'];?></span>
                        <?php }?>
                    <?php }?>
                </div>
            <?php }?>

            <?php $socials = Modules::run('page/_get_footer_contacts_social');?>
            <?php if(!empty($socials)){?>
                <div class="col-xs-12 col-sm-6 social-wr">
                    <?php foreach($socials as $social){?>
                        <a href="<?php echo $social['link'];?>" class="social_link" target="_blank">
                            <span class="<?php echo $social['icon'];?>-o"></span>
                        </a>
                    <?php }?>
                </div>
            <?php }?>
        </div>
    </div>
</footer>
<div class="modal fade" id="video_popup_form" data-keyboard="false" data-backdrop="static"></div>