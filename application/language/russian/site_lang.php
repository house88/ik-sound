<?php 
$lang['header_menu_general_home'] = 'Главная';
$lang['header_menu_general_portfolio'] = 'Портфолио';
$lang['header_menu_general_video'] = 'Видео';
$lang['header_menu_general_blog'] = 'Блог';
$lang['header_menu_general_contacts'] = 'Контакты';
$lang['button_audio_play_text'] = 'прослушать';
$lang['button_video_play_text'] = 'Пройграть видео';
$lang['section_services_header'] = 'Наши услуги';
$lang['section_projects_header'] = 'Наши проекты';
$lang['section_projects_counter_text'] = 'завершенный проект';
$lang['section_our_clients_header'] = 'Наши клиенты';
$lang['section_contacts_header'] = 'Пишите нам!';
$lang['no_data'] = 'Нет данных для отображения.';
$lang['error_message_not_privileged'] = 'Ошибка! Нет прав.';
$lang['select_default_value_all'] = 'Все';
