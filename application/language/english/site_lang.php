<?php 
$lang['header_menu_general_home'] = 'Home';
$lang['header_menu_general_portfolio'] = 'Portfolio';
$lang['header_menu_general_video'] = 'Videos';
$lang['header_menu_general_blog'] = 'Blog';
$lang['header_menu_general_contacts'] = 'Contacts';
$lang['button_audio_play_text'] = 'listen';
$lang['button_video_play_text'] = 'Пройграть видео';
$lang['section_services_header'] = 'Our services';
$lang['section_projects_header'] = 'Our projects';
$lang['section_projects_counter_text'] = 'completed project';
$lang['section_our_clients_header'] = 'Our clients';
$lang['section_contacts_header'] = 'Write to us!';
$lang['no_data'] = 'No data for display.';
$lang['error_message_not_privileged'] = 'Error! Access deny.';
$lang['select_default_value_all'] = 'All';
