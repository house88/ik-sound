<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/


$route['default_controller'] = "home";
$route['^(ru|en)/portfolio'] 	= "audio/portfolio";
$route['^(ru|en)/portfolio/([a-zA-Z0-9\/\-:._]+)'] = function ($categories){
	return 'audio/portfolio/' . strtolower($categories);
};

$route['^(ru|en)/blog/post'] 	= "blog/post";
$route['^(ru|en)/blog/post/([a-zA-Z0-9\/\-:._]+)'] = function ($categories){
	return 'blog/post/' . strtolower($categories);
};

$route['^(ru|en)/blog'] 	= "blog/index";
$route['^(ru|en)/blog/([a-zA-Z0-9\/\-:._]+)'] = function ($categories){
	return 'blog/index/' . strtolower($categories);
};

$route['^(ru|en)/video'] 	= "video/index";
$route['^(ru|en)/video/([a-zA-Z0-9\/\-:._]+)'] = function ($categories){
	return 'video/index/' . strtolower($categories);
};

$route['^(ru|en)/contacts'] 	= "page/contacts";

$route['^(ru|en)$'] = $route['default_controller'];
$route['^(ru|en)/(.+)$'] = "$2";
$route['404_override'] = '';

// $route['signin'] 	        	= "auth/signin";
// $route['logout'] 	        	= "auth/logout";

// ADMIN
$route['admin/([a-zA-Z0-9\/\-:._]+)'] = function ($module){
	$full_route = explode('/', $module);
	$module_name = array_shift($full_route);
	switch ($module_name) {
		case 'signin':
		case 'signout':
		case 'settings':
		case 'contacts':
		case 'translations':
		case 'ajax_operations':
		case 'popup_forms':
			return 'admin/'.$module;
		break;
		default:
			$url[] = $module_name;
			$url[] = 'admin';			
		break;
	}
	
	if(!empty($full_route)){
		$url[] = implode('/', $full_route);	
	}
	
	return implode('/', $url);
};

//$route['translate_uri_dashes'] = FALSE;
$route['scaffolding_trigger'] = "";
