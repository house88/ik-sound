<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pagination_lib{
	public function get_settings($id, $total, $limit){
        $config = array();
		$config['total_rows'] = $total;
		$config['per_page'] = $limit;
		$config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
		$config['prev_link'] = '<i class="fa fa-angle-left"></i>';
		$config['next_link'] = '<i class="fa fa-angle-right"></i>';
		$config['last_link'] = '<i class="fa fa-angle-double-right"></i>';

		// открывющий тэг перед навигацией
        $config['full_tag_open'] = '<ul class="pagination pagination-centered">';

        // закрывающий тэг после навигации
        $config['full_tag_close'] = '</ul>';

        // первая страница открывающий тэг
        $config['first_tag_open'] = '<li>';

        // первая страница закрывающий тэг
        $config['first_tag_close'] = '</li>';

        // последняя страница открывающий тэг
        $config['last_tag_open'] = '<li>';

        // последняя страница закрывающий тэг
        $config['last_tag_close'] = '</li>';

        // предыдущая страница открывающий тэг
        $config['prev_tag_open'] = '<li>';

        // предыдущая страница закрывающий тэг
        $config['prev_tag_close'] = '</li>';

        // текущая страница открывающий тэг
        $config['cur_tag_open'] = '<li class="active"><a href="#">';

        // текущая страница закрывающий тэг
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li>'; // цифровая ссылка открывающий тэг
        $config['num_tag_close'] = '</li>'; // цифровая ссылка закрывающий тэг

        // следующая страница открывающий тэг
        $config['next_tag_open'] = '<li>';

        // следующая страница закрывающий тэг
        $config['next_tag_close'] = '</li>';

		switch($id){
			case 'products':
                $config['suffix'] = '';
                if (!empty($_GET)){
                    $config['suffix'] = '?' . http_build_query($_GET, '', "&");
                }                    

                $config['prefix'] = 'page/';
                $CI = get_instance();
                $link = $CI->uri->uri_to_assoc(1);

                if(isset($link['page'])){
                    unset($link['page']);
                    $segment = $CI->uri->total_segments();
                } else{
                    $segment = $CI->uri->total_segments()+3;
                }

                $link = $CI->uri->assoc_to_uri($link);
                $config['base_url'] = base_url().$link;
                $config['first_url'] = $config['base_url'].$config['suffix'];
                $config['uri_segment'] = $segment;
                $config['num_links'] = 2;
                $config['use_page_numbers'] = TRUE;
                return $config;
			break;
		}
	}
}
?>