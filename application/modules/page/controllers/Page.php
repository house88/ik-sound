<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Page extends MX_Controller{
	function __construct(){
		parent::__construct();
		
		$this->data = array();
		$this->breadcrumbs = array();
		$this->data['site_theme'] = $this->site_theme = $this->config->item('site_theme');
		$this->data['lang'] = $this->ulang = $this->lang->lang();
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title']['setting_value_'.$this->ulang];
		$this->data['skeywords'] = $this->data['settings']['mk']['setting_value_'.$this->ulang];
		$this->data['sdescription'] = $this->data['settings']['md']['setting_value_'.$this->ulang];
		
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		show_404();
	}

	function contacts(){
		$this->data['uri_ru'] = 'ru/contacts';
		$this->data['uri_en'] = 'en/contacts';
		$this->data['contacts'] = $this->settings->handler_get_contacts();
		if (!$this->input->is_ajax_request()) {
			$this->data['breadcrumbs'] = $this->breadcrumbs;
			$this->data['main_content'] = 'modules/page/contacts_view';
			$this->data['og_meta'] = $this->page_meta->handler_get('contacts');
			$this->load->view(get_theme_view('page_template'), $this->data);
		} else{
			$content = $this->load->view(get_theme_view('modules/page/contacts_view'), $this->data, true);
			jsonResponse('', 'success', array('content' => $content, 'stitle' => $this->data['stitle'], 'uri_ru' => $this->data['uri_ru'], 'uri_en' => $this->data['uri_en']));
		}
	}

	function _get_footer_contacts_address(){
		$contacts = $this->settings->handler_get_contacts();
		$addresses = !empty($contacts['contacts_addresses']) ? json_decode($contacts['contacts_addresses'], true) : array();

		$records = array();
		if(!empty($addresses)){
			foreach ($addresses as $address) {
				if($address['in_footer'] == false){
					continue;
				}

				$records[] = $address;
			}
		}

		return $records;
	}

	function _get_footer_contacts_social(){
		$contacts = $this->settings->handler_get_contacts();
		$socials = !empty($contacts['contacts_socials']) ? json_decode($contacts['contacts_socials'], true) : array();

		$records = array();
		if(!empty($socials)){
			foreach ($socials as $social) {
				if($social['is_visible'] == false){
					continue;
				}

				$records[] = $social;
			}
		}

		return $records;
	}

	function get_header(){
		if ($this->input->is_ajax_request()) {
			$link = $this->input->post('link');
			$link = str_replace(base_url('en'), base_url(), $link);
			$link = str_replace(base_url('ru'), base_url(), $link);
			$this->data['uri_ru'] = str_replace(base_url(), 'ru', $link);
			$this->data['uri_en'] = str_replace(base_url(), 'en', $link);
			$content = $this->load->view(get_theme_view('include/header_menu'), $this->data, true);
			jsonResponse('', 'success', array('header_menu' => $content, 'data' => $this->data));
		}
	}
}
