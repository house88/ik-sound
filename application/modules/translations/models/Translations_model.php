<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Translations_model extends CI_Model{
	
	var $translations = "translations";
	
	function __construct(){
		parent::__construct();
	}
	
	function set_translations($data = array()){
		if(empty($data)){
			return;
		}
		
		$this->db->insert($this->translations, $data);
		return $this->db->insert_id();
	}
	
	function update_translations($data = array()){
		if(empty($data)){
			return;
		}
		
		$this->db->update_batch($this->translations, $data, 'translation_key');
	}
	
	function get_translations(){
		return $this->db->get($this->translations)->result_array();
	}
}

?>