<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Page_meta_model extends CI_Model{
	var $page_meta = "page_meta";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->page_meta, $data);
		return $this->db->insert_id();
	}

	function handler_update($page_key, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('page_key', $page_key);
		$this->db->update($this->page_meta, $data);
	}

	function handler_get($page_key){
		$this->db->where('page_key', $page_key);

		$og_meta = $this->db->get($this->page_meta)->row_array();
		if(!empty($og_meta)){
			$og_meta['page_image_src'] = file_modification_time('files/page_meta/'.$og_meta['page_image']);
		}
		
		return $og_meta;
	}

	function handler_get_all(){
		return $this->db->get($this->page_meta)->result_array();
	}

	function handler_get_count(){
		return $this->db->count_all_results($this->page_meta);
	}
}
?>
