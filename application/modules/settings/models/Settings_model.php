<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Settings_model extends CI_Model{
	
	var $settings = "settings";
	var $contacts = "contacts";
	
	function __construct(){
		parent::__construct();
	}
	
	function set_settings($data = array()){
		if(empty($data)){
			return;
		}
		
		$this->db->insert($this->settings, $data);
		return $this->db->insert_id();
	}
	
	function update_settings($data = array()){
		if(empty($data)){
			return;
		}
		
		$this->db->update_batch($this->settings, $data, 'setting_alias');
	}
	
	function get_settings(){
		$this->db->order_by('id_setting', 'ASC');
		return $this->db->get($this->settings)->result_array();
	}
	
	function handler_get_contacts(){
		$this->db->limit(1);
		return $this->db->get($this->contacts)->row_array();
	}
	
	function update_contacts($id_contacts = 0, $data = array()){
		if(empty($data)){
			return;
		}
		
		$this->db->where('id_contacts', $id_contacts);
		$this->db->update($this->contacts, $data);
	}
}
