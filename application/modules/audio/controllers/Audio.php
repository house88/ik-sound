<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Audio extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->data['site_theme'] = $this->site_theme = $this->config->item('site_theme');
		$this->data['lang'] = $this->ulang = $this->lang->lang();
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title'][lang_column('setting_value')];
		$this->data['skeywords'] = $this->data['settings']['mk'][lang_column('setting_value')];
		$this->data['sdescription'] = $this->data['settings']['md'][lang_column('setting_value')];
		
		$this->load->model('Audio_model', 'audio');
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		return_404();
	}

	function portfolio(){
		$selected_filters = array();
		$uri = $this->uri->uri_to_assoc(3);
		$page = 1;
		if(isset($uri['page']) && (int)$uri['page'] > 0){
			$page = (int)$uri['page'];
		} elseif($this->input->post('page')){
			$page = (int) $this->input->post('page');
		}

		if($page < 1){
			$page = 1;
		}
		
		$limit = (int) $this->data['settings']['portfolio_limit'][lang_column('setting_value')];
		$start = ($page <= 1) ? 0 : (($page * $limit) - $limit);

		if(!empty($uri['filters'])){
			$uri_filters = array_filter(explode(':',$uri['filters']));
			foreach ($uri_filters as $filter) {
				if(!empty($filter)){
					$temp_filter = explode('_', $filter);
					if(isset($temp_filter[0], $temp_filter[1]) && (int)$temp_filter[0] > 0 && (int) $temp_filter[1] > 0){
						$selected_filters[(int)$temp_filter[0]] = $filter;
					}
				}
			}
		} elseif($filters = $this->input->post('filters')){
			if(!empty($filters) && is_array($filters)){
				foreach ($filters as $filter) {
					if(!empty($filter)){
						$temp_filter = explode('_', $filter);
						if(isset($temp_filter[0], $temp_filter[1]) && (int)$temp_filter[0] > 0 && (int) $temp_filter[1] > 0){
							$selected_filters[(int)$temp_filter[0]] = $filter;
						}
					}
				}
			}
		}
		
		$params = array(
			'limit' => $limit,
			'start' => $start,
			'audio_active' => 1,
			'sort_by' => array(
				'audio_favorite-DESC',
				'audio_date-DESC'
			)
		);

		if(!empty($selected_filters)){
			$id_audio = $this->audio->handler_get_audios_by_properties_values(array('selected_filters' => $selected_filters));
			$params['id_audio'] = $id_audio;
		}

		$this->data['limit'] = $limit;
		$this->data['records_total'] = $records_total = $this->audio->handler_get_count($params);
		$this->data['projects'] = $this->audio->handler_get_all($params);

		$type_of_works = $this->audio->handler_get_audio_type_of_works();
		$this->data['type_of_works'] = array();
		if(!empty($type_of_works)){
			foreach ($type_of_works as $type_of_work) {
				$values = json_decode($type_of_work['property_values'], true);
				$audio_values = explode(',', $type_of_work['audio_values']);
				foreach ($audio_values as $audio_value) {
					$components = explode('_', $audio_value);
					$this->data['type_of_works'][$type_of_work['id_audio']][] = $values[$components[1]][lang_column('title')];
				}
			}
		}

		$properties = $this->audio->handler_get_all_properties(array('property_active' => 1));
		$this->data['properties'] = array();
		if(!empty($properties)){
			foreach ($properties as $property) {
				$temp_property = $property;
				$temp_property['selected_value_text'] = lang_line('select_default_value_all', false);
				if(array_key_exists($property['id_property'], $selected_filters)){
					$property_values = json_decode($property['property_values'], true);
					foreach ($property_values as $id_value => $property_value) {
						$temp_key = $property['id_property'].'_'.$id_value;
						if($selected_filters[$property['id_property']] == $temp_key){
							$temp_property['selected_value_text'] = $property_value[lang_column('title')];
						}
					}
				}

				$this->data['properties'][] = $temp_property;
			}
		}
		$this->data['selected_filters'] = $selected_filters;
		$this->data['page'] = $page;

		if (!$this->input->is_ajax_request()) {
			$this->data['breadcrumbs'] = $this->breadcrumbs;
			$this->data['main_content'] = 'modules/audio/portfolio_view';
			$this->data['og_meta'] = $this->page_meta->handler_get('portfolio');
			$this->load->view(get_theme_view('page_template'), $this->data);
        } else{
			if($this->input->post('full_render')){
				$content = $this->load->view(get_theme_view('modules/audio/portfolio_view'), $this->data, true);
			} else{
				$content = $this->load->view(get_theme_view('modules/audio/portfolio_list_view'), $this->data, true);
			}
			jsonResponse('', 'success', array('content' => $content, 'records_total' => $records_total, 'stitle' => $this->data['stitle']));
		}
	}

	function _get_home_banner_slides(){
		$params = array(
			'audio_active' => 1,
			'audio_banner' => 1
		);

		$audio_files = $this->audio->handler_get_all($params);
		$banner_slides = array();
		foreach ($audio_files as $audio_file) {
			$banner_slides[] = array(
				'image' => site_url('files/audio/poster/'.$audio_file['audio_image']),
				'title' => $audio_file[lang_column('audio_title')],
				'description' => nl2br($audio_file[lang_column('audio_description')]),
				'audio' => site_url('files/audio/'.$audio_file['audio_file']),
				'play_btn' => !empty($audio_file['audio_file'])
			);
		}

		return json_encode($banner_slides);
	}

	function _get_home_projects(){
		$params = array(
			'audio_active' => 1,
			'audio_favorite' => 1
		);

		$audio_files = $this->audio->handler_get_all($params);
		$type_of_works = $this->audio->handler_get_audio_type_of_works();
		$audio_type_of_works = array();
		if(!empty($type_of_works)){
			foreach ($type_of_works as $type_of_work) {
				$values = json_decode($type_of_work['property_values'], true);
				$audio_values = explode(',', $type_of_work['audio_values']);
				foreach ($audio_values as $audio_value) {
					$components = explode('_', $audio_value);
					$audio_type_of_works[$type_of_work['id_audio']][] = $values[$components[1]][lang_column('title')];
				}
			}
		}
		$projects = array();
		foreach ($audio_files as $audio_file) {
			$projects[] = array(
				'image' => site_url('files/audio/poster/'.$audio_file['audio_image']),
				'title' => $audio_file[lang_column('audio_title')],
				'type_of_works' => (array_key_exists($audio_file['id_audio'], $audio_type_of_works))?implode(', ', $audio_type_of_works[$audio_file['id_audio']]):'',
				'description' => nl2br($audio_file[lang_column('audio_description')]),
				'audio' => site_url('files/audio/'.$audio_file['audio_file']),
				'play_btn' => !empty($audio_file['audio_file'])
			);
		}

		return $projects;
	}

	function _get_projects(){
		$params = array(
			'audio_active' => 1,
			'limit' => 12
		);

		$audio_files = $this->audio->handler_get_all($params);
		$projects = array();
		foreach ($audio_files as $audio_file) {
			$projects[] = array(
				'image' => site_url('files/audio/poster/'.$audio_file['audio_image']),
				'title' => $audio_file['audio_client_name'],
				'description' => nl2br($audio_file[lang_column('audio_description')]),
				'audio' => site_url('files/audio/'.$audio_file['audio_file'])
			);
		}

		return $projects;
	}

	function _get_playlist(){
		$playlist = array();
		$params = array(
			'audio_active' => 1,
			'audio_default_in_player' => 1
		);

		$audio_files = $this->audio->handler_get_all($params);
		foreach ($audio_files as $audio_file) {
			if(empty($audio_file['audio_file'])){
				continue;
			}

			$playlist[] = array(
				'title' => $audio_file['audio_client_name'],
				'type' => 'audio',
				'sources' => array(
					array(
						'src' => site_url('files/audio/'.$audio_file['audio_file']),
						'type' => 'audio/mp3'
					)
				)
			);
		}

		return json_encode($playlist);
	}
}
?>