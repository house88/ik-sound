<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse(lang_line('error_message_not_privileged', false));
			}
		}

		$this->data = array();
		$this->data['main_title'] = 'Аудио';

		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Audio_model", "audio");

        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$this->data['main_content'] = 'admin/modules/audio/list';
		$this->load->view('admin/page', $this->data);
	}

	function properties(){
		$this->data['main_title'] = 'Свойства';
		$this->data['main_content'] = 'admin/modules/audio/properties/list';
		$this->load->view('admin/page', $this->data);
	}
	
	function popup_forms(){
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$action = $this->uri->segment(4);
		switch ($action) {
			// DONE
			case 'add':
				$properties_params = array(
					'property_active' => 1
				);
				$this->data['properties'] = $this->audio->handler_get_all_properties($properties_params);
				$content = $this->load->view('admin/modules/audio/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			// DONE
			case 'edit':
				$id_audio = (int) $this->uri->segment(5);
				$this->data['audio'] = $this->audio->handler_get($id_audio);
				if(empty($this->data['audio'])){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$properties_params = array(
					'property_active' => 1
				);
				$this->data['properties'] = $this->audio->handler_get_all_properties($properties_params);
				
				$audio_properties_values = $this->audio->handler_get_audio_properties_values(array('id_audio' => $id_audio));
				$this->data['audio_properties_values'] = array();
				if(!empty($audio_properties_values)){
					foreach ($audio_properties_values as $audio_property_value) {
						$this->data['audio_properties_values'][$audio_property_value['id_property']][] = $audio_property_value['id_value'];
					}
				}
				
				$content = $this->load->view('admin/modules/audio/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			// DONE
			case 'add_property':
				$content = $this->load->view('admin/modules/audio/properties/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			// DONE
			case 'edit_property':
				$id_property = (int) $this->uri->segment(5);
				$this->data['property'] = $this->audio->handler_get_property($id_property);
				if(empty($this->data['property'])){
					jsonResponse('Ошибка: Данные не верны.');
				}
				
				$content = $this->load->view('admin/modules/audio/properties/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			// DONE
			case 'add':
				$this->form_validation->set_rules('title_ru', 'Название RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('description_ru', 'Описание RU', 'xss_clean|max_length[500]');
				$this->form_validation->set_rules('description_en', 'Описание En', 'xss_clean|max_length[500]');
				$this->form_validation->set_rules('audio_file', 'Аудио файл', 'trim|xss_clean');
				$this->form_validation->set_rules('audio_client_name', 'Имя файла', 'trim|xss_clean');
				$this->form_validation->set_rules('audio_image', 'Постер', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
				}
				
				$path = 'files/audio';
				create_dir($path);

				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink($path.'/poster/'.$remove_photo);
					}
				}

				$remove_files = $this->input->post('remove_files');
				if(!empty($remove_files)){
					foreach($remove_files as $remove_file){
						@unlink($path.'/'.$remove_file);
					}
				}

				$image_name = $this->input->post('audio_image');
				$insert = array(
					'audio_title_ru' => $this->input->post('title_ru'),
					'audio_title_en' => $this->input->post('title_en'),
					'audio_description_ru' => $this->input->post('description_ru'),
					'audio_description_en' => $this->input->post('description_en'),
					'audio_client_name' => ($this->input->post('audio_client_name'))?$this->input->post('audio_client_name', true):'',
					'audio_file' => ($this->input->post('audio_file'))?$this->input->post('audio_file', true):'',
					'audio_image' => $image_name
				);
				$id_audio = $this->audio->handler_insert($insert);

				$post_properties = $this->input->post('properties');
				$properties = arrayByKey($this->audio->handler_get_all_properties(), 'id_property');
				$audio_properties_values = array();
				if(!empty($post_properties) && is_array($post_properties)){
					foreach ($post_properties as $property_key => $post_property_values) {
						if(array_key_exists($property_key, $properties) && !empty($post_property_values)){
							$values = json_decode($properties[$property_key]['property_values'], true);
							foreach ($post_property_values as $post_property_value) {
								if(array_key_exists($post_property_value, $values)){
									$audio_properties_values[] = array(
										'id_property' => $property_key,
										'id_value' => $post_property_value,
										'id_audio' => $id_audio,
										'id_property_id_value' => $property_key.'_'.$post_property_value
									);
								}
							}
						}
					}
				}

				if(!empty($audio_properties_values)){
					$this->audio->handler_insert_audio_properties_values($audio_properties_values);
				}

				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'edit':
                $this->form_validation->set_rules('id_audio', 'Аудио', 'required|xss_clean');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('description_ru', 'Описание RU', 'xss_clean|max_length[500]');
				$this->form_validation->set_rules('description_en', 'Описание En', 'xss_clean|max_length[500]');
				$this->form_validation->set_rules('audio_file', 'Аудио файл', 'trim|xss_clean');
				$this->form_validation->set_rules('audio_client_name', 'Имя файла', 'trim|xss_clean');
				$this->form_validation->set_rules('audio_image', 'Постер', 'required|xss_clean');
				$this->form_validation->set_rules('audio_date', 'Дата', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
				}

				$id_audio = (int)$this->input->post('id_audio');
				$audio = $this->audio->handler_get($id_audio);
				if(empty($audio)){
					jsonResponse('Ошибка: Данные не верны.');
				}
				
				$path = 'files/audio';
				create_dir($path);

				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink($path.'/poster/'.$remove_photo);
					}
				}

				$remove_files = $this->input->post('remove_files');
				if(!empty($remove_files)){
					foreach($remove_files as $remove_file){
						@unlink($path.'/'.$remove_file);
					}
				}

				$update = array(
					'audio_title_ru' => $this->input->post('title_ru'),
					'audio_title_en' => $this->input->post('title_en'),
					'audio_description_ru' => $this->input->post('description_ru'),
					'audio_description_en' => $this->input->post('description_en'),
					'audio_client_name' => ($this->input->post('audio_client_name'))?$this->input->post('audio_client_name', true):'',
					'audio_file' => ($this->input->post('audio_file'))?$this->input->post('audio_file', true):'',
					'audio_image' => $this->input->post('audio_image', true),
					'audio_date' => formatDate($this->input->post('audio_date'), 'Y-m-d H:i:s')
				);

				$this->audio->handler_update($id_audio, $update);

				$post_properties = $this->input->post('properties');
				$properties = arrayByKey($this->audio->handler_get_all_properties(), 'id_property');
				$audio_properties_values = array();
				if(!empty($post_properties) && is_array($post_properties)){
					foreach ($post_properties as $property_key => $post_property_values) {
						if(array_key_exists($property_key, $properties) && !empty($post_property_values)){
							$values = json_decode($properties[$property_key]['property_values'], true);
							foreach ($post_property_values as $post_property_value) {
								if(array_key_exists($post_property_value, $values)){
									$audio_properties_values[] = array(
										'id_property' => $property_key,
										'id_value' => $post_property_value,
										'id_audio' => $id_audio,
										'id_property_id_value' => $property_key.'_'.$post_property_value
									);
								}
							}
						}
					}
				}

				$this->audio->handler_delete_audio_properties_values(array('id_audio' => $id_audio));
				if(!empty($audio_properties_values)){
					$this->audio->handler_insert_audio_properties_values($audio_properties_values);
				}

				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'list_dt':
				$params = array(
					'limit' => intVal($_POST['iDisplayLength']),
					'start' => intVal($_POST['iDisplayStart'])
				);

				if ($_POST['iSortingCols'] > 0) {
					for ($i = 0; $i < $_POST['iSortingCols']; $i++) {
						switch ($_POST["mDataProp_" . intval($_POST['iSortCol_' . $i])]) {
							case 'dt_name': $params['sort_by'][] = "{$this->audio->audio}.audio_title_ru-" . $_POST['sSortDir_' . $i];
							break;
							case 'dt_date': $params['sort_by'][] = "{$this->audio->audio}.audio_date-" . $_POST['sSortDir_' . $i];
							break;
						}
					}
				}

				$records = $this->audio->handler_get_all($params);
				$records_total = $this->audio->handler_get_count($params);

				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);

				foreach ($records as $record) {
					$status = '<a href="#" data-message="Вы уверены что хотите сделать аудио неактивным?" title="Сделать неактивным" data-title="Изменение статуса" data-callback="change_status" data-audio="'.$record['id_audio'].'" class="btn btn-success btn-xs confirm-dialog"><i class="fa fa-eye"></i></a>';
					if($record['audio_active'] == 0){
						$status = '<a href="#" data-message="Вы уверены что хотите сделать аудио активным?" title="Сделать активным" data-title="Изменение статуса" data-callback="change_status" data-audio="'.$record['id_audio'].'" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-eye-slash"></i></a>';
					}
					
					$audio_favorite = '<a href="#" data-message="Вы уверены что хотите снять признак избранное?" title="Снять признак избранное" data-title="Снять признак избранное" data-callback="change_marketing" data-audio="'.$record['id_audio'].'" data-marketing="audio_favorite" class="btn btn-warning btn-xs confirm-dialog"><i class="fa fa-star"></i></a>';
					if($record['audio_favorite'] == 0){
						$audio_favorite = '<a href="#" data-message="Вы уверены что хотите установить признак избранное?" title="Установить признак избранное" data-title="Установить признак избранное" data-callback="change_marketing" data-audio="'.$record['id_audio'].'" data-marketing="audio_favorite" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-star-o"></i></a>';
					}
					
					$audio_banner = '<a href="#" data-message="Вы уверены что хотите снять признак показать в слайдере на главной?" title="Снять признак показать в слайдере" data-title="Снять признак показать в слайдере" data-callback="change_marketing" data-audio="'.$record['id_audio'].'" data-marketing="audio_banner" class="btn btn-primary btn-xs confirm-dialog"><i class="fa fa-picture-o"></i></a>';
					if($record['audio_banner'] == 0){
						$audio_banner = '<a href="#" data-message="Вы уверены что хотите установить признак показать в слайдере на главной?" title="Установить признак показать в слайдере" data-title="Установить признак показать в слайдере" data-callback="change_marketing" data-audio="'.$record['id_audio'].'" data-marketing="audio_banner" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-picture-o"></i></a>';
					}
					
					$audio_default_in_player = "";
					if(!empty($record['audio_file'])){
						$audio_default_in_player = '<a href="#" data-message="Вы уверены что хотите снять признак в плеере?" title="Снять признак в плеере" data-title="Снять признак в плеере" data-callback="change_marketing" data-audio="'.$record['id_audio'].'" data-marketing="audio_default_in_player" class="btn btn-info btn-xs confirm-dialog"><i class="fa fa-play-circle"></i></a>';
						if($record['audio_default_in_player'] == 0){
							$audio_default_in_player = '<a href="#" data-message="Вы уверены что хотите установить признак в плеере?" title="Установить признак в плеере" data-title="Установить признак в плеере" data-callback="change_marketing" data-audio="'.$record['id_audio'].'" data-marketing="audio_default_in_player" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-stop-circle"></i></a>';
						}
					}

					$output['aaData'][] = array(
						'dt_photo'		=>  '<a href="'.base_url(getImage('files/audio/poster/'.$record['audio_image'])).'" data-fancybox="group_'.$record['id_audio'].'">
												<img src="'.base_url(getImage('files/audio/poster/'.$record['audio_image'])).'" class="img-thumbnail mw-50 mh-50"/>
											</a>',
						'dt_name'		=>  $record['audio_title_ru'],
						'dt_date'		=>  formatDate($record['audio_date']),
						'dt_actions'	=>  '<div class="btn-group">
												'.$audio_default_in_player.'
												'.$audio_banner.'
												'.$audio_favorite.'
												'.$status.'
												<a href="#" data-href="'.base_url('admin/audio/popup_forms/edit/'.$record['id_audio']).'" title="Редактировать" class="btn btn-primary btn-xs call-popup" data-popup="#general_popup_form"><i class="fa fa-pencil"></i></a>
												<a href="#" class="btn btn-danger btn-xs confirm-dialog" title="Удалить" data-message="Вы уверены что хотите удалить аудио?" data-callback="delete_action" data-audio="'.$record['id_audio'].'"><i class="fa fa-remove"></i></a>
											</div>'

					);
				}
				jsonResponse('', 'success', $output);
			break;
			// DONE
			case 'delete':
				$this->form_validation->set_rules('audio', 'Аудио', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_audio = (int)$this->input->post('audio');
				$audio = $this->audio->handler_get($id_audio);
				if(empty($audio)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$this->audio->handler_update($id_audio, array('audio_deleted' => 1));
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			// DONE
			case 'change_status':
				$this->form_validation->set_rules('audio', 'Аудио', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_audio = (int)$this->input->post('audio');
				$audio = $this->audio->handler_get($id_audio);
				if(empty($audio)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				if($audio['audio_active']){
					$status = 0;
				} else{
					$status = 1;
				}
				$this->audio->handler_update($id_audio, array('audio_active' => $status));
				jsonResponse('Операция прошла успешно.', 'success', array('status' => $status));
			break;
			// DONE
			case 'change_marketing':
				$this->form_validation->set_rules('audio', 'Аудио', 'required|xss_clean');
				$this->form_validation->set_rules('marketing', 'Маркетинг', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_audio = (int)$this->input->post('audio');
				$audio = $this->audio->handler_get($id_audio);
				if(empty($audio)){
					jsonResponse('Ошибка: Данные не верны.');
				}
				
				$marketing = $this->input->post('marketing', true);
				if(!isset($audio[$marketing])){
					jsonResponse('Ошибка: Данные не верны.');
				}
				
				if($audio[$marketing] == 1){
					$status = 0;
				} else{
					$status = 1;
				}
				$this->audio->handler_update($id_audio, array($marketing => $status));
				jsonResponse('Операция прошла успешно.', 'success', array('status' => $status));
			break;
			// DONE
			case 'add_property':
				$this->form_validation->set_rules('title_ru', 'Название RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'required|xss_clean|max_length[250]');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$insert = array(
					'property_title_ru' => $this->input->post('title_ru'),
					'property_title_en' => $this->input->post('title_en')
				);
				
				$values_ru = $this->input->post('values_ru');
				$values_en = $this->input->post('values_en');
				$values_weight = $this->input->post('values');
				if(!is_array($values_ru) || !is_array($values_en) || !is_array($values_weight)){
					jsonResponse('Ошибка: Добавьте значения для свойства.');
				}
				$values = array();
				foreach ($values_weight as $id_value) {
					if(!empty($values_ru[$id_value]) && !empty($values_en[$id_value])){
						$values[$id_value] = array(
							'title_ru' => $values_ru[$id_value],
							'title_en' => $values_en[$id_value]
						);
					}
				}

				if(empty($values)){
					jsonResponse('Ошибка: Добавьте значения для свойства.');
				}

				$insert['property_values'] = json_encode($values);

				$last_property = $this->audio->handler_get_property_weight();
				$property_weight = 1;
				if(!empty($last_property)){
					$property_weight = $last_property['property_weight'] + 1;
				}
				$insert['property_weight'] = $property_weight;

				$id_property = $this->audio->handler_insert_property($insert);
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'edit_property':
				$this->form_validation->set_rules('id_property', 'Свойство', 'required|xss_clean');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'required|xss_clean|max_length[250]');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
				}
				
				$id_property = (int) $this->input->post('id_property');
				$property = $this->audio->handler_get_property($id_property);
				if(empty($property)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$update = array(
					'property_title_ru' => $this->input->post('title_ru'),
					'property_title_en' => $this->input->post('title_en')
				);
				
				$values_ru = $this->input->post('values_ru');
				$values_en = $this->input->post('values_en');
				$values_weight = $this->input->post('values');

				if(!is_array($values_ru) || !is_array($values_en) || !is_array($values_weight)){
					jsonResponse('Ошибка: Добавьте значения для свойства.');
				}

				$values = array();
				foreach ($values_weight as $id_value) {
					if(!empty($values_ru[$id_value]) && !empty($values_en[$id_value])){
						$values[$id_value] = array(
							'title_ru' => $values_ru[$id_value],
							'title_en' => $values_en[$id_value]
						);
					}
				}

				if(empty($values)){
					jsonResponse('Ошибка: Добавьте значения для свойства.');
				}

				$update['property_values'] = json_encode($values);
				$this->audio->handler_update_property($id_property, $update);
				$this->audio->handler_delete_audio_property_values($id_property, array('not_id_value' => array_keys($values)));
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'properties_list_dt':
				$params = array(
					'limit' => intVal($_POST['iDisplayLength']),
					'start' => intVal($_POST['iDisplayStart'])
				);

				if ($_POST['iSortingCols'] > 0) {
					for ($i = 0; $i < $_POST['iSortingCols']; $i++) {
						switch ($_POST["mDataProp_" . intval($_POST['iSortCol_' . $i])]) {
							case 'dt_name_ru': $params['sort_by'][] = 'property_title_ru-' . $_POST['sSortDir_' . $i];
							break;
							case 'dt_name_en': $params['sort_by'][] = 'property_title_en-' . $_POST['sSortDir_' . $i];
							break;
						}
					}
				}

				$records = $this->audio->handler_get_all_properties($params);
				$records_total = $this->audio->handler_get_count_properties($params);

				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);

				foreach ($records as $record) {
					$status = '<a href="#" data-message="Вы уверены что хотите сделать свойство активной?" title="Сделать активной" data-title="Изменение статуса" data-callback="change_status" data-property="'.$record['id_property'].'" class="btn btn-success btn-xs confirm-dialog"><i class="fa fa-eye"></i></a>';
					if($record['property_active'] == 0){
						$status = '<a href="#" data-message="Вы уверены что хотите сделать свойство неактивной?" title="Сделать неактивной" data-title="Изменение статуса" data-callback="change_status" data-property="'.$record['id_property'].'" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-eye-slash"></i></a>';
					}

					$output['aaData'][] = array(
						'dt_name_ru'	=>  $record['property_title_ru'],
						'dt_name_en'	=>  $record['property_title_en'],
						'dt_actions'	=>  '<div class="btn-group">
												<span class="btn btn-default btn-xs call-function" title="Поднять на уровень выше" data-callback="change_weight" data-property="'.$record['id_property'].'" data-direction="up"><i class="fa fa-arrow-up"></i></span>
												<span class="btn btn-default btn-xs call-function" title="Отпустить на уровень ниже" data-callback="change_weight" data-property="'.$record['id_property'].'" data-direction="down"><i class="fa fa-arrow-down"></i></span>
												'.$status.'
												<a href="#" data-href="'.base_url('admin/audio/popup_forms/edit_property/'.$record['id_property']).'" title="Редактировать" class="btn btn-primary btn-xs call-popup" data-popup="#general_popup_form"><i class="fa fa-pencil"></i></a>
												<a href="#" class="btn btn-danger btn-xs confirm-dialog" title="Удалить" data-message="Вы уверены что хотите удалить свойство?" data-callback="delete_action" data-property="'.$record['id_property'].'"><i class="fa fa-remove"></i></a>
											</div>'
					);
				}
				jsonResponse('', 'success', $output);
			break;
			// DONE
			case 'delete_property':
                $this->form_validation->set_rules('property', 'Свойство', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_property = (int)$this->input->post('property');
				$property = $this->audio->handler_get_property($id_property);
				if(empty($property)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$this->audio->handler_delete_property($id_property);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			// DONE
			case 'change_status_property':
				$this->form_validation->set_rules('property', 'Свойство', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_property = (int)$this->input->post('property');
				$property = $this->audio->handler_get_property($id_property);
				if(empty($property)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				if($property['property_active']){
					$status = 0;
				} else{
					$status = 1;
				}
				$this->audio->handler_update_property($id_property, array('property_active' => $status));
				jsonResponse('Операция прошла успешно.', 'success', array('status' => $status));
			break;
			// DONE
			case 'change_weight_property':
				$this->form_validation->set_rules('property', 'Свойство', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_property = (int) $this->input->post('property');
				$property = $this->audio->handler_get_property($id_property);
				if(empty($property)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$params = array(
					'property_weight' => $property['property_weight']
				);

				if($this->input->post('direction') != 'down'){
					$params['direction'] = 'up';
				}
				$last_property = $this->audio->handler_get_property_weight($params);
				if(!empty($last_property)){
					$property_weight = $last_property['property_weight'];
					$update = array(
						array(
							'id_property' => $id_property,
							'property_weight' => $last_property['property_weight']
						),
						array(
							'id_property' => $last_property['id_property'],
							'property_weight' => $property['property_weight']
						)
					);
					$this->audio->handler_update_properties($update);
				}

				jsonResponse('Сохранено.','success');
			break;
			// DONE
			case 'upload_photo':
				$path = 'files/audio/poster';
				create_dir($path);

				$config['upload_path'] = FCPATH . $path;
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['file_name'] = uniqid();
				$config['min_width']	= '600';
				$config['min_height']	= '400';
				$config['max_size']	= '4000';

				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload()){
					jsonResponse($this->upload->display_errors('',''),'error');
				}

				$data = $this->upload->data();
				$config = array(
					'source_image'      => $data['full_path'],
					'create_thumb'      => false,
					'new_image'         => FCPATH . $path,
					'maintain_ratio'    => true,
					'width'             => 600
				);

				$this->load->library('image_lib');
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
				$info = new StdClass;
				$info->filename = $data['file_name'];
				$files[] = $info;
				jsonResponse('', 'success', array("files" => $files));
			break;
			// DONE
			case 'upload_file':
				$path = 'files/audio';
				create_dir($path);

				$config['upload_path'] = FCPATH . $path;
				$config['allowed_types'] = 'mp3';
				$config['file_name'] = uniqid();
				$config['max_size']	= '20000';

				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload('audiofile')){
					jsonResponse($this->upload->display_errors('',''),'error');
				}

				$data = $this->upload->data();
				$info = new StdClass;
				$info->client_name = str_replace($data['file_ext'],'',$data['client_name']);
				$info->name = $data['file_name'];
				jsonResponse('', 'success', array("file" => $info, 'data' => $data));
			break;
		}
	}
}
?>