<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Audio_model extends CI_Model{
	var $audio = "audio";
	var $audio_properties = "audio_properties";
	var $audio_properties_values = "audio_properties_values";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->audio, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_audio = 0, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_audio', $id_audio);
		$this->db->update($this->audio, $data);
	}

	function handler_delete($id_audio = 0){
		$this->db->where('id_audio', $id_audio);
		$this->db->delete($this->audio);
	}

	function handler_get($id_audio = 0){
		$this->db->where('id_audio', $id_audio);
		return $this->db->get($this->audio)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " audio_date DESC ";
		$audio_deleted = 0;

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

		$this->db->select("{$this->audio}.*");
		$this->db->from("{$this->audio}");

        if(isset($id_audio)){
			$this->db->where_in("{$this->audio}.id_audio", $id_audio);
        }

        if(isset($audio_active)){
			$this->db->where("{$this->audio}.audio_active", $audio_active);
        }

        if(isset($audio_deleted)){
			$this->db->where("{$this->audio}.audio_deleted", $audio_deleted);
        }

        if(isset($audio_favorite)){
			$this->db->where("{$this->audio}.audio_favorite", $audio_favorite);
        }

		if(isset($audio_banner)){
			$this->db->where("{$this->audio}.audio_banner", $audio_banner);
		}

		if(isset($audio_default_in_player)){
			$this->db->where("{$this->audio}.audio_default_in_player", $audio_default_in_player);
		}

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get()->result_array();
	}

	function handler_get_count($conditions = array()){
		$audio_deleted = 0;

        extract($conditions);

		$this->db->select("{$this->audio}.*");
		$this->db->from("{$this->audio}");

        if(isset($id_audio)){
			$this->db->where_in("{$this->audio}.id_audio", $id_audio);
        }

        if(isset($audio_active)){
			$this->db->where("{$this->audio}.audio_active", $audio_active);
        }

        if(isset($audio_deleted)){
			$this->db->where("{$this->audio}.audio_deleted", $audio_deleted);
        }

        if(isset($audio_favorite)){
			$this->db->where("{$this->audio}.audio_favorite", $audio_favorite);
        }

        if(isset($audio_banner)){
			$this->db->where("{$this->audio}.audio_banner", $audio_banner);
        }
		
		if(isset($audio_default_in_player)){
			$this->db->where("{$this->audio}.audio_default_in_player", $audio_default_in_player);
		}

		return $this->db->count_all_results();
	}

	// PROPERTIES
	function handler_insert_property($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->audio_properties, $data);
		return $this->db->insert_id();
	}

	function handler_update_property($id_property = 0, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_property', $id_property);
		$this->db->update($this->audio_properties, $data);
	}

	function handler_delete_property($id_property = 0){
		$this->handler_delete_audio_property_values($id_property);

		$this->db->where('id_property', $id_property);
		$this->db->delete($this->audio_properties);
	}
	
	function handler_get_property($id_property = 0){
		$this->db->where('id_property', $id_property);
		return $this->db->get($this->audio_properties)->row_array();
	}
	
	function handler_get_property_weight($conditions = array()){
		$direction = 'down';
		extract($conditions);

		if(isset($property_weight)){
			if($direction == 'down'){
				$this->db->where('property_weight', $property_weight + 1);
			} else{
				$this->db->where('property_weight', $property_weight - 1);
			}
		} else{
			$this->db->order_by('property_weight', 'DESC');
		}

		$this->db->limit(1);
		$query = $this->db->get($this->audio_properties);
		
		return $query->row_array();            
	}
	
	function handler_update_properties($data = array(), $column = 'id_property'){
		if(empty($data)){
			return false;
		}

		return $this->db->update_batch($this->audio_properties, $data, $column);
	}
	
	function handler_get_all_properties($conditions = array()){
		$order_by = " property_weight ASC ";

		extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

		if(isset($id_property)){
			$this->db->where_in('id_property', $id_property);
		}

		if(isset($property_active)){
			$this->db->where('property_active', $property_active);
		}

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->audio_properties)->result_array();
	}

	function handler_get_count_properties($conditions = array()){
		extract($conditions);
		
		if(isset($id_property)){
			$this->db->where_in('id_property', $id_property);
		}

		if(isset($property_active)){
			$this->db->where('property_active', $property_active);
		}

		return $this->db->count_all_results($this->audio_properties);
	}

	// AUDIO PROPERTIES VALUES
	function handler_insert_audio_properties_values($data = array()){
		if(empty($data)){
			return false;
		}

		$this->db->insert_batch($this->audio_properties_values, $data);
	}

	function handler_get_audio_properties_values($conditions = array()){
		extract($conditions);

		if(isset($id_audio)){
			$this->db->where('id_audio', $id_audio);
		}

		return $this->db->get($this->audio_properties_values)->result_array();
	}
	
	function handler_get_audios_by_properties_values($conditions = array()){
		$where = array();		
		extract($conditions);

		if(isset($selected_filters)){
			foreach($selected_filters as $selected_filter){
				$where[] = "(
								SELECT apv.id_property_id_value
								FROM $this->audio_properties_values apv 
								INNER JOIN {$this->audio_properties} ap ON apv.id_property = ap.id_property
								WHERE a.id_audio = apv.id_audio AND apv.id_property_id_value = '{$selected_filter}' AND ap.property_active = 1 AND ap.property_deleted = 0
								GROUP BY apv.id_audio
							)";
			}			
		}

		$sql = "SELECT a.id_audio
				FROM $this->audio a";
		
		if(!empty($where)){
			$sql .= " WHERE " . implode(" AND ", $where);
		}
		
		$records = $this->db->query($sql)->result_array();
		$id_audio = array(0);
		if(!empty($records)){
			foreach ($records as $record) {
				$id_audio[$record['id_audio']] = $record['id_audio'];
			}
		}

		return $id_audio;
	}
	
	function handler_get_audio_type_of_works($conditions = array()){
		$sql = "SELECT 
					ap.*,
					apv.id_audio, 
					GROUP_CONCAT(apv.id_property_id_value separator ',') as audio_values
				FROM {$this->audio_properties_values} apv
				INNER JOIN {$this->audio_properties} ap ON apv.id_property = ap.id_property AND ap.property_on_audio = 1
				GROUP BY apv.id_audio";		
		return $this->db->query($sql)->result_array();
	}

	function handler_delete_audio_property_values($id_property = 0, $conditions = array()){
		$this->db->where('id_property', $id_property);
		
		extract($conditions);

		if(isset($not_id_value)){
			$this->db->where_not_in('id_value', $not_id_value);
		}

		$this->db->delete($this->audio_properties_values);
	}

	function handler_delete_audio_properties_values($conditions = array()){
		extract($conditions);

		if(isset($id_audio)){
			$this->db->where_in('id_audio', $id_audio);
		}

		if(isset($id_property)){
			$this->db->where_in('id_property', $id_property);
		}

		if(isset($id_value)){
			$this->db->where_in('id_value', $id_value);
		}

		$this->db->delete($this->audio_properties_values);
	}
}
?>
