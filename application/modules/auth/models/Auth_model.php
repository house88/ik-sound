<?php
/*
Author: Daniel Gutierrez
Date: 9/18/12
Version: 1.0
*/

class Auth_model extends CI_Model{
	
	var $admin_table = "admin";
	
	function __construct(){
		parent::__construct();
	}

	// ADMIN	
	function handler_admin_insert($data = array()){
		if(empty($data)){
			return false;
		}

		$this->db->insert($this->admin_table, $data);
		return $this->db->insert_id();
	}

	function handler_admin_update($id_user, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where("id_user", $id_user);
		return $this->db->update($this->admin_table, $data);
	}

	function handler_admin_get($conditions = array()){
		if(empty($conditions)){
			return false;
		}

		extract($conditions);

		if(isset($id_user)){
			$this->db->where('id_user', $id_user);
		}

		if(isset($email)){
			$this->db->where('user_email', $email);
		}

		return $this->db->get($this->admin_table)->row();
	}
}

?>