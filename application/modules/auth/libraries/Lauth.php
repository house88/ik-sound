<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lauth
{
	public function __construct(){
		$this->ci =& get_instance();
	}    
	
	function logged_in(){
		return (bool) $this->ci->session->userdata('logged_in');
	}
	
	function logout(){
		$this->ci->session->sess_destroy();
	}

	function id_user(){
		if($this->logged_in()){
			return (int) $this->ci->session->userdata('id_user');
		}else{
			return 0;
		}
	}
	
	function is_admin(){
		if($this->ci->session->userdata('group_type') == 'admin'){
			return true;
		}else{
			return false;
		}
	}
}
