<?php
/*
Author: Daniel Gutierrez
Date: 9/18/12
Version: 1.0
*/

class Menu_model extends CI_Model{
    var $menu = "menu";

    function __construct(){
        parent::__construct();
    }
	
	function handler_insert($data = array()){
        if(empty($data)){
            return;
        }

        $this->db->insert($this->menu, $data);
        return $this->db->insert_id();
	}

	function handler_update($id_menu, $data = array()){
        if(empty($data)){
            return;
        }

        $this->db->where('id_menu', $id_menu);
        $this->db->update($this->menu, $data);
	}
	
	function handler_delete($id_menu){
        $this->db->where('id_menu', $id_menu);
        $this->db->delete($this->menu);
	}

	function handler_get($id_menu){
        $this->db->where('id_menu', $id_menu);
        return $this->db->get($this->menu)->row_array();
	}	

	function handler_get_by_title($menu_title){
        $this->db->where('menu_title', $menu_title);
        return $this->db->get($this->menu)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " id_menu ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

        if(isset($active)){
			$this->db->where('menu_visible', $active);
        }

		$this->db->order_by($order_by);
		return $this->db->get($this->menu)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($active)){
			$this->db->where('menu_visible', $active);
        }

		return $this->db->count_all_results($this->menu);
	}
}

?>
