<h1 class="page-header">
    Меню
    <a href="<?php echo base_url('admin/menu/add');?>" class="btn btn-success btn-xs pull-right">
        <i class="fa fa-plus"></i>
        Добавить меню
    </a>
</h1>
<div class="panel panel-default">
    <div class="panel-heading">
        Добавить меню
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <form role="form" id="add_form">
                    <div class="form-group">
						<label>Название</label>
						<input class="form-control" placeholder="Название" name="title">
						<p class="help-block">Название не должно содержать более 100 символов.</p>
					</div>
					<div class="form-group">
						<label>Елементы меню</label>
						<div id="property_values">
							<table class="table table-striped table-bordered">
								<thead>
									<tr>
										<th class="text-center">Название</th>
										<th class="text-center">Ссылка</th>
										<th class="text-center w-50"></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class=""><input class="form-control" placeholder="Название" name="menu_elements[0][title]" value=""></td>
										<td class=""><input class="form-control" placeholder="Ссылка" name="menu_elements[0][link]" value=""></td>
										<td class=""><span class="btn btn-danger remove_row"><i class="ca-icon ca-icon_remove"></i></span></td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="2">
											<span class="btn btn-default add_row_value">Добавить елемент</span>
										</td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
                    <button type="submit" class="btn btn-success">Сохранить</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
	var menu_index = 1;
    $(function(){
		$('body').on('click','.add_row_value', function(){
			var $table = $(this).closest('table');
			var $tbody = $table.children('tbody');
			var new_row = '<tr>';
			new_row += '<td>';
			new_row += '<input class="form-control" placeholder="Название" name="menu_elements['+menu_index+'][title]">';
			new_row += '</td>';
			new_row += '<td>';
			new_row += '<input class="form-control" placeholder="Ссылка" name="menu_elements['+menu_index+'][link]">';
			new_row += '</td>';
			new_row += '<td>';
			new_row += '<span class="btn btn-danger remove_row"><i class="ca-icon ca-icon_remove"></i></span>';
			new_row += '</td>';
			new_row += '</tr>';
			$tbody.append(new_row);
			menu_index++;
		});

		$('body').on('click','.remove_row', function(){
			$(this).closest('tr').remove();
		});

        var add_form = $('#add_form');
        add_form.submit(function () {
            var fdata = add_form.serialize();
            $.ajax({
                type: 'POST',
                url: base_url+'admin/menu/ajax_operations/add',
                data: fdata,
                dataType: 'JSON',
                success: function(resp){
                    if(resp.mess_type == 'success'){
                        add_form.replaceWith('<div class="alert alert-success mb-0 ml-5 mr-5">'+resp.message+'</div>');
                    } else{
                        systemMessages(resp.message, resp.mess_type);
                    }
                }
            });
            return false;
        });
    });
</script>
