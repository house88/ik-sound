<h1 class="page-header">
    Меню
    <a href="<?php echo base_url('admin/menu/add');?>" class="btn btn-success btn-xs pull-right">
        <i class="fa fa-plus"></i>
        Добавить раздел
    </a>
</h1>
<div class="panel panel-default">
    <div class="panel-heading">
        Все разделы
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th>Название</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php if(!empty($left_menu_items)){?>
                    <?php foreach($left_menu_items as $left_menu_item){?>
                        <tr>
                            <td class="w-40 text-center"><?php echo $left_menu_item['id_menu'];?></td>
                            <td><?php echo $left_menu_item['menu_title'];?></td>
                            <td class="w-120 text-center">
                                <?php if($left_menu_item['menu_visible']){?>
                                    <a href="#" data-message="Вы уверены что хотите сделать раздел невидимый?" data-title="Изменение статуса раздела" data-callback="change_status" data-menu="<?php echo $left_menu_item['id_menu'];?>" class="btn btn-success btn-xs confirm-dialog">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                <?php } else{?>
                                    <a href="#" data-message="Вы уверены что хотите сделать раздел видимый?" data-title="Изменение статуса раздела" data-callback="change_status" data-menu="<?php echo $left_menu_item['id_menu'];?>" class="btn btn-default btn-xs confirm-dialog">
                                        <i class="fa fa-eye-slash"></i>
                                    </a>
                                <?php }?>
                                <a href="<?php echo base_url('admin/menu/edit/'.$left_menu_item['id_menu']);?>" class="btn btn-primary btn-xs">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a href="#"
                                   data-message="Вы уверены что хотите удалить раздел <?php echo $left_menu_item['menu_title'];?>?"
                                   data-title="Удаление раздела меню"
                                   data-callback="delete_menu"
                                   data-menu="<?php echo $left_menu_item['id_menu'];?>"
                                   class="btn btn-danger btn-xs confirm-dialog">
                                    <i class="ca-icon ca-icon_remove"></i>
                                </a>
                            </td>
                        </tr>
                    <?php }?>
                <?php } else{?>
                    <tr>
                        <td colspan="5">
                            <div class="alert alert-info mb-0 ml-5 mr-5">
                                Нет разделов меню.
                            </div>
                        </td>
                    </tr>
                <?php }?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    var delete_menu = function(btn){
        var $this = $(btn);
        var menu = $this.data('menu');
        $.ajax({
            type: 'POST',
            url: base_url+'admin/menu/ajax_operations/delete',
            data: {menu:menu},
            dataType: 'JSON',
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                if(resp.mess_type == 'success'){
                    var $tbody = $this.closest('tbody');
                    $this.closest('tr').remove();
                    var total = $tbody.children('tr').length;
                    if(total == 0){
                        $tbody.html('<tr><td colspan="4"><div class="alert alert-info mb-0 ml-5 mr-5">Нет разделов меню.</div></td></tr>');
                    }
                }
            }
        });
        return false;
    }
    var change_status = function(btn){
		var $this = $(btn);
		var menu = $this.data('menu');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/menu/ajax_operations/change_status',
			data: {menu:menu},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					if(resp.status == 0){
						$this.replaceWith('<a href="#" data-message="Вы уверены что хотите сделать раздел невидимый??" data-title="Изменение статуса раздела" data-callback="change_status" data-menu="'+menu+'" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-eye-slash"></i></a>');
					} else{
						$this.replaceWith('<a href="#" data-message="Вы уверены что хотите сделать раздел видимый??" data-title="Изменение статуса раздела" data-callback="change_status" data-menu="'+menu+'" class="btn btn-success btn-xs confirm-dialog"><i class="fa fa-eye"></i></a>');
					}
				}
			}
		});
		return false;
	}
</script>
