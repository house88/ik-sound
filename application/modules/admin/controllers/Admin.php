<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}
	
	function index(){
		if(!$this->lauth->logged_in()){
			redirect('admin/signin');
		}
		
		if(!$this->lauth->is_admin()){
			redirect();
		}

		$this->settings();
	}
	
	function settings(){
		if(!$this->lauth->logged_in()){
			redirect('admin/signin');
		}
		
		if(!$this->lauth->is_admin()){
			redirect();
		}

		$this->data['main_title'] = 'Настройки';
		$this->data['main_content'] = 'admin/modules/settings/settings_view';
		$this->load->view('admin/page', $this->data);
	}
	
	function translations(){
		if(!$this->lauth->logged_in()){
			redirect('admin/signin');
		}
		
		if(!$this->lauth->is_admin()){
			redirect();
		}

		$this->load->model('translations/Translations_model', 'translations');
		$this->data['translations'] = $this->translations->get_translations();
		$this->data['main_title'] = 'Переводы';
		$this->data['main_content'] = 'admin/modules/translations/translations_view';
		$this->load->view('admin/page', $this->data);
	}
	
	function contacts(){
		if(!$this->lauth->logged_in()){
			redirect('admin/signin');
		}
		
		if(!$this->lauth->is_admin()){
			redirect();
		}

		$this->data['contacts'] = $this->settings->handler_get_contacts();
		$this->data['main_title'] = 'Контакты';
		$this->data['main_content'] = 'admin/modules/page/contacts_view';
		$this->load->view('admin/page', $this->data);
	}
	
	function signin(){
		if($this->lauth->logged_in()){
            redirect('admin');
        }
        
        $this->data['main_title'] = 'Авторизация';
        $this->load->view('admin/modules/auth/signin_view', $this->data);
	}
	
	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
		}

		$option = $this->uri->segment(3);
		switch($option){
			case 'save_settings':
				if(!$this->lauth->logged_in()){
					jsonResponse(lang_line('error_message_not_privileged', false));
				}
				
				if(!$this->lauth->is_admin()){
					jsonResponse(lang_line('error_message_not_privileged', false));
				}
				
				$settings = $this->input->post('settings');
				$update = array();
				foreach($settings as $key => $setting){
					$update[] = array(
						'setting_alias' => $key,
						'setting_value_ru' => $setting['ru'],
						'setting_value_en' => $setting['en']
					);
				}
				
				$this->settings->update_settings($update);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'save_translations':
				if(!$this->lauth->logged_in()){
					jsonResponse(lang_line('error_message_not_privileged', false));
				}
				
				if(!$this->lauth->is_admin()){
					jsonResponse(lang_line('error_message_not_privileged', false));
				}
				
				$translations = $this->input->post('translations');
				$update = array();
				foreach($translations as $key => $translation){
					$update[] = array(
						'translation_key' => $key,
						'translation_text_ru' => $translation['ru'],
						'translation_text_en' => $translation['en']
					);
				}
				
				$this->load->model('translations/Translations_model', 'translations');
				$this->translations->update_translations($update);
				
				$lang_file = APPPATH . 'language/english/site_lang.php';
				$f = fopen($lang_file, "w");
				fwrite($f, '<?php '."\r\n");
				foreach($update as $lang_record){
					fwrite($f, '$lang[\''.$lang_record['translation_key'].'\'] = \''.$lang_record['translation_text_en'].'\';'."\r\n");
				}
				fclose($f);

				$lang_file = APPPATH . 'language/russian/site_lang.php';
				$f = fopen($lang_file, "w");
				fwrite($f, '<?php '."\r\n");
				foreach($update as $lang_record){
					fwrite($f, '$lang[\''.$lang_record['translation_key'].'\'] = \''.$lang_record['translation_text_ru'].'\';'."\r\n");
				}
				fclose($f);
				
				jsonResponse('Сохранено.', 'success');
			break;
			case 'save_contacts':
				if(!$this->lauth->logged_in()){
					jsonResponse(lang_line('error_message_not_privileged', false));
				}
				
				if(!$this->lauth->is_admin()){
					jsonResponse(lang_line('error_message_not_privileged', false));
				}

				$addresses = array();
				$post_addresses = $this->input->post('address');
				if(!empty($post_addresses)){
					foreach($post_addresses as $key => $address){
						$addresses[$key] = array(
							'icon' => $address['icon'],
							'name' => $address['name'],
							'value' => $address['value'],
							'link' => $address['link'],
							'is_visible' => !empty($address['is_visible']) ? true : false,
							'in_footer' => !empty($address['in_footer']) ? true : false,
						);
					}
				}

				$socials = array();
				$post_socials = $this->input->post('socials');
				if(!empty($post_socials)){
					foreach($post_socials as $key => $social){
						$socials[$key] = array(
							'icon' => $social['icon'],
							'name' => $social['name'],
							'link' => $social['link'],
							'is_visible' => !empty($social['is_visible']) ? true : false,
						);
					}
				}

				$update = array(
					'contacts_text_ru' => $this->input->post('text_ru'),
					'contacts_text_en' => $this->input->post('text_en'),
					'contacts_addresses' => !empty($addresses) ? json_encode($addresses) : null,
					'contacts_socials' => !empty($socials) ? json_encode($socials) : null,
					'mk_ru' => $this->input->post('mk_ru'),
					'mk_en' => $this->input->post('mk_en'),
					'md_ru' => $this->input->post('md_ru'),
					'md_en' => $this->input->post('md_en')
				);
				$id_page = 1;
				$this->settings->update_contacts($id_page, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'signin':
				if($this->lauth->logged_in()){
					jsonResponse('Вы уже авторизированы.', 'info');
				}
				
				$config = array(
					array(
						'field' => 'user_email',
						'label' => 'E-mail',
						'rules' => 'required|xss_clean|valid_email',
					),
					array(
						'field' => 'password',
						'label' => 'Password',
						'rules' => 'required|xss_clean',
					)
				);

				$this->form_validation->set_rules($config);

				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				$email = $this->input->post('user_email', true);
				$password 	= $this->input->post('password', true);
				$user_data 	= $this->mauth->handler_admin_get(array('email' => $email));
				if(!$user_data){
					jsonResponse('Данные для входа не верны.');
				}

				if(password_verify($password, $user_data->user_password)){	
					$session_data['id_user'] = $user_data->id_user;
					$session_data['group_type'] = 'admin';
					$session_data['logged_in'] = true;
					$this->session->set_userdata($session_data);
				} else{
					jsonResponse('Неверный логин или пароль, попробуйте ещё раз.');
				}

				jsonResponse('', 'success', array('redirect' => 'admin'));
			break;			
			case 'edit':
				if(!$this->lauth->logged_in()){
					jsonResponse('Ошибка: Пройдите авторизацию.');
				}

				if(!$this->lauth->is_admin()){
					jsonResponse('Ошибка: Нет прав.');
				}

				$this->form_validation->set_rules('user_email', 'Email', 'required|xss_clean|valid_email');
				$this->form_validation->set_rules('user_password', 'Новый пароль', 'min_length[6]|max_length[50]');
				$password = $this->input->post('user_password');
				if(!empty($password)){
					$this->form_validation->set_rules('confirm_password', 'Повторите новый пароль', 'required|matches[user_password]');
				}

				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$manager = $this->mauth->handler_admin_get(array('id_user' => $this->lauth->id_user()));
				if(empty($manager)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$update = array(
					'user_email' => $this->input->post('user_email')
				);

				$password = $this->input->post('user_password');
				if(!empty($password)){
					$update['user_password'] = password_hash($password, PASSWORD_BCRYPT);
				}

				$this->mauth->handler_admin_update($this->lauth->id_user(), $update);
				jsonResponse('Сохранено.','success');
			break;
		}
	}

	function popup_forms(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$action = $this->uri->segment(3);
		switch ($action) {
			case 'edit':
				if(!$this->lauth->logged_in()){
					jsonResponse('Ошибка: Пройдите авторизацию.');
				}

				if(!$this->lauth->is_admin()){
					jsonResponse('Ошибка: Нет прав.');
				}

				$this->data['manager'] = $this->mauth->handler_admin_get(array('id_user' => $this->lauth->id_user()));
				if(empty($this->data['manager'])){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$content = $this->load->view('admin/modules/admin/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function signout(){
        $this->lauth->logout();
		redirect('');
	}
}

?>
