<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Video extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->data['site_theme'] = $this->site_theme = $this->config->item('site_theme');
		$this->data['lang'] = $this->ulang = $this->lang->lang();
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title'][lang_column('setting_value')];
		$this->data['skeywords'] = $this->data['settings']['mk'][lang_column('setting_value')];
		$this->data['sdescription'] = $this->data['settings']['md'][lang_column('setting_value')];
		
		$this->load->model('Video_model', 'video');
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$uri = $this->uri->uri_to_assoc(3);
		$page = 1;
		if(isset($uri['page']) && (int)$uri['page'] > 0){
			$page = (int)$uri['page'];
		} elseif($this->input->post('page')){
			$page = (int) $this->input->post('page');
		}

		if($page < 1){
			$page = 1;
		}
		$this->data['page'] = $page;
		
		$limit = (int) $this->data['settings']['video_limit'][lang_column('setting_value')];
		$start = ($page <= 1) ? 0 : (($page * $limit) - $limit);
		
		$params = array(
			'limit' => $limit,
			'start' => $start,
			'video_active' => 1,
			'category_active' => 1,
			'sort_by' => array(
				'video_favorite-DESC',
				'video_date-DESC'
			)
		);
		
		$id_category = 0;
		if(isset($uri['category'])){
			$id_category = (int) $uri['category'];
		} elseif($this->input->post('category')){			
			$id_category = (int) $this->input->post('category');
		}

		if(isset($id_category) && $id_category > 0){
			$params['id_category'] = $id_category;
		}

		$this->data['records_total'] = $records_total = $this->video->handler_get_count($params);
		$this->data['videos'] = $this->video->handler_get_all($params);
		$this->data['categories'] = $this->video->handler_get_all_categories(array('category_active' => 1));
		$this->data['limit'] = $limit;
		$this->data['selected_category'] = $id_category;

		if (!$this->input->is_ajax_request()) {
			$this->data['breadcrumbs'] = $this->breadcrumbs;
			$this->data['main_content'] = 'modules/video/video_view';
			$this->data['og_meta'] = $this->page_meta->handler_get('video');
			$this->load->view(get_theme_view('page_template'), $this->data);
        } else{
			if($this->input->post('full_render')){
				$content = $this->load->view(get_theme_view('modules/video/video_view'), $this->data, true);
			} else{
				$content = $this->load->view(get_theme_view('modules/video/video_list_view'), $this->data, true);
			}
			jsonResponse('', 'success', array('content' => $content, 'records_total' => $records_total, 'stitle' => $this->data['stitle']));
		}
	}
	
	function popup_forms(){
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$action = $this->uri->segment(4);
		switch ($action) {
			case 'preview':
				$id_video = (int) $this->uri->segment(5);
				$this->data['video'] = $this->video->handler_get($id_video);
				if(empty($this->data['video'])){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$content = $this->load->view(get_theme_view('modules/video/preview_view'), $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}
	
	function _get_home_videos(){
		$params = array(
			'avideo_active' => 1,
			'video_favorite' => 1
		);

		$video_files = $this->video->handler_get_all($params);
		$videos = array();
		foreach ($video_files as $video_file) {
			$videos[] = array(
				'image' => site_url('files/video/'.$video_file['video_image']),
				'title' => $video_file[lang_column('video_title')],
				'type' => $video_file['video_type'],
				'code' => $video_file['video_code']
			);
		}

		return $videos;
	}
}
?>