<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse(lang_line('error_message_not_privileged', false));
			}
		}

		$this->data = array();
		$this->data['main_title'] = 'Видео';

		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Video_model", "video");

        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$this->data['main_content'] = 'admin/modules/video/list';
		$this->load->view('admin/page', $this->data);
	}

	function categories(){
		$this->data['main_title'] = 'Категории видео';
		$this->data['main_content'] = 'admin/modules/video/categories/list';
		$this->load->view('admin/page', $this->data);
	}
	
	function popup_forms(){
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$action = $this->uri->segment(4);
		switch ($action) {
			case 'add':
				$params = array(
					'category_active' => 1
				);
				$this->data['categories'] = $this->video->handler_get_all_categories($params);
				$content = $this->load->view('admin/modules/video/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			case 'edit':
				$id_video = (int) $this->uri->segment(5);
				$this->data['video'] = $this->video->handler_get($id_video);
				if(empty($this->data['video'])){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$params = array(
					'category_active' => 1
				);
				$this->data['categories'] = $this->video->handler_get_all_categories($params);
				$content = $this->load->view('admin/modules/video/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			case 'add_category':
				$content = $this->load->view('admin/modules/video/categories/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			case 'edit_category':
				$id_category = (int) $this->uri->segment(5);
				$this->data['category'] = $this->video->handler_get_category($id_category);
				if(empty($this->data['category'])){
					jsonResponse('Ошибка: Данные не верны.');
				}
				
				$content = $this->load->view('admin/modules/video/categories/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			case 'preview':
				$id_video = (int) $this->uri->segment(5);
				$this->data['video'] = $this->video->handler_get($id_video);
				if(empty($this->data['video'])){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$content = $this->load->view('admin/modules/video/preview_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			// DONE
			case 'add':
				$this->form_validation->set_rules('category', 'Категория', 'required|xss_clean');
				$this->form_validation->set_rules('link', 'Ссылка', 'required|xss_clean|valid_url');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('description_ru', 'Описание RU', 'xss_clean|max_length[500]');
				$this->form_validation->set_rules('description_en', 'Описание En', 'xss_clean|max_length[500]');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
				}
				
				$path = 'files/video';
				create_dir($path);

				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink($path.'/'.$remove_photo);
					}
				}

				$id_category = $this->input->post('category');
				$category = $this->video->handler_get_category($id_category);
				if(empty($category)){
					jsonResponse('Ошибка: Данные не верны.');
				}
				
				$this->load->library('Videothumb', 'videothumb');
				$video_info = $this->videothumb->process($this->input->post('link'));
				
				if(!empty($video_info['error'])) {
					jsonResponse($video_info['error']);
				}
				
				$config = array(
					'source_image'      => $video_info['image'],
					'create_thumb'      => false,
					'new_image'         => FCPATH . $path,
					'maintain_ratio'    => true,
					'width'             => 600
				);
				$this->load->library('image_lib');
				$this->image_lib->initialize($config);
				$this->image_lib->resize();

				if($this->input->post('video_image')){
					$image_name = $this->input->post('video_image');
				} else{
					$image_name = explode('/', $video_info['image']);
					$image_name = end($image_name);
				}
				$insert = array(
					'video_category' => $id_category,
					'video_title_ru' => $this->input->post('title_ru'),
					'video_title_en' => $this->input->post('title_en'),
					'video_description_ru' => $this->input->post('description_ru'),
					'video_description_en' => $this->input->post('description_en'),
					'video_link' => $this->input->post('link', true),
					'video_code' => $video_info['v_id'],
					'video_image' => $image_name,
					'video_type' => $video_info['type']
				);

				$id_video = $this->video->handler_insert($insert);
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'edit':
                $this->form_validation->set_rules('id_video', 'Видео', 'required|xss_clean');
				$this->form_validation->set_rules('category', 'Категория', 'required|xss_clean');
				$this->form_validation->set_rules('link', 'Ссылка', 'required|xss_clean|valid_url');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('description_ru', 'Описание RU', 'xss_clean|max_length[500]');
				$this->form_validation->set_rules('description_en', 'Описание En', 'xss_clean|max_length[500]');
				$this->form_validation->set_rules('video_image', 'Постер', 'required|xss_clean');
				$this->form_validation->set_rules('video_date', 'Дата', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
				}
				
				$path = 'files/video';
				create_dir($path);

				$id_video = (int)$this->input->post('id_video');
				$video = $this->video->handler_get($id_video);
				if(empty($video)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/video/'.$remove_photo);
					}
				}
				
				$id_category = $this->input->post('category');
				$category = $this->video->handler_get_category($id_category);
				if(empty($category)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$update = array(
					'video_category' => $id_category,
					'video_title_ru' => $this->input->post('title_ru'),
					'video_title_en' => $this->input->post('title_en'),
					'video_description_ru' => $this->input->post('description_ru'),
					'video_description_en' => $this->input->post('description_en'),
					'video_image' => $this->input->post('video_image'),
					'video_date' => formatDate($this->input->post('video_date'), 'Y-m-d H:i:s')
				);

				$video_link = $this->input->post('link');
				if($video['video_link'] != $video_link){
					$this->load->library('Videothumb', 'videothumb');
					$video_info = $this->videothumb->process($video_link);
					
					if(!empty($video_info['error'])) {
						jsonResponse($video_info['error']);
					}
					
					$config = array(
						'source_image'      => $video_info['image'],
						'create_thumb'      => false,
						'new_image'         => FCPATH . $path,
						'maintain_ratio'    => true,
						'width'             => 600
					);
					$this->load->library('image_lib');
					$this->image_lib->initialize($config);
					$this->image_lib->resize();
	
					if($this->input->post('video_image')){
						$image_name = $this->input->post('video_image');
					} else{
						$image_name = explode('/', $video_info['image']);
						$image_name = end($image_name);
					}

					$update['video_link'] = $video_link;
					$update['video_code'] = $video_info['v_id'];
					$update['video_image'] = $image_name;
					$update['video_type'] = $video_info['type'];
				}

				$this->video->handler_update($id_video, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'list_dt':
				$params = array(
					'limit' => intVal($_POST['iDisplayLength']),
					'start' => intVal($_POST['iDisplayStart'])
				);

				if ($_POST['iSortingCols'] > 0) {
					for ($i = 0; $i < $_POST['iSortingCols']; $i++) {
						switch ($_POST["mDataProp_" . intval($_POST['iSortCol_' . $i])]) {
							case 'dt_name': $params['sort_by'][] = "{$this->video->video}.video_title_ru-" . $_POST['sSortDir_' . $i];
							break;
							case 'dt_cname': $params['sort_by'][] = "{$this->video->video_categories}.category_title_ru-" . $_POST['sSortDir_' . $i];
							break;
							case 'dt_type': $params['sort_by'][] = "{$this->video->video}.video_type-" . $_POST['sSortDir_' . $i];
							break;
							case 'dt_date': $params['sort_by'][] = "{$this->video->video}.video_date-" . $_POST['sSortDir_' . $i];
							break;
						}
					}
				}

				$records = $this->video->handler_get_all($params);
				$records_total = $this->video->handler_get_count($params);

				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);

				foreach ($records as $record) {
					$status = '<a href="#" data-message="Вы уверены что хотите сделать видео неактивным?" title="Сделать неактивным" data-title="Изменение статуса" data-callback="change_status" data-video="'.$record['id_video'].'" class="btn btn-success btn-xs confirm-dialog"><i class="fa fa-eye"></i></a>';
					if($record['video_active'] == 0){
						$status = '<a href="#" data-message="Вы уверены что хотите сделать видео активным?" title="Сделать активным" data-title="Изменение статуса" data-callback="change_status" data-video="'.$record['id_video'].'" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-eye-slash"></i></a>';
					}
					
					$video_favorite = '<a href="#" data-message="Вы уверены что хотите снять признак избранное?" title="Снять признак избранное" data-title="Снять признак избранное" data-callback="change_marketing" data-video="'.$record['id_video'].'" data-marketing="video_favorite" class="btn btn-warning btn-xs confirm-dialog"><i class="fa fa-star"></i></a>';
					if($record['video_favorite'] == 0){
						$video_favorite = '<a href="#" data-message="Вы уверены что хотите установить признак избранное?" title="Установить признак избранное" data-title="Установить признак избранное" data-callback="change_marketing" data-video="'.$record['id_video'].'" data-marketing="video_favorite" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-star-o"></i></a>';
					}

					$output['aaData'][] = array(
						'dt_photo'		=>  '<a href="'.base_url(getImage('files/video/'.$record['video_image'])).'" data-fancybox="group_'.$record['id_video'].'">
												<img src="'.base_url(getImage('files/video/'.$record['video_image'])).'" class="img-thumbnail mw-50 mh-50"/>
											</a>',
						'dt_name'		=>  $record['video_title_ru'],
						'dt_cname'		=>  $record['category_title_ru'],
						'dt_type'		=>  $record['video_type'],
						'dt_date'		=>  formatDate($record['video_date']),
						'dt_actions'	=>  '<div class="btn-group">
												<a href="#" data-href="'.base_url('admin/video/popup_forms/preview/'.$record['id_video']).'" title="Просмотр" class="btn btn-primary btn-xs call-popup" data-popup="#video_popup_form"><i class="fa fa-video-camera"></i></a>
												'.$video_favorite.'
												'.$status.'
												<a href="#" data-href="'.base_url('admin/video/popup_forms/edit/'.$record['id_video']).'" title="Редактировать" class="btn btn-primary btn-xs call-popup" data-popup="#general_popup_form"><i class="fa fa-pencil"></i></a>
												<a href="#" class="btn btn-danger btn-xs confirm-dialog" title="Удалить" data-message="Вы уверены что хотите удалить видео?" data-callback="delete_action" data-video="'.$record['id_video'].'"><i class="fa fa-remove"></i></a>
											</div>'

					);
				}
				jsonResponse('', 'success', $output);
			break;
			// DONE
			case 'delete':
				$this->form_validation->set_rules('video', 'Видео', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_video = (int)$this->input->post('video');
				$video = $this->video->handler_get($id_video);
				if(empty($video)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$this->video->handler_update($id_video, array('video_deleted' => 1));
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			// DONE
			case 'change_status':
				$this->form_validation->set_rules('video', 'Видео', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_video = (int)$this->input->post('video');
				$video = $this->video->handler_get($id_video);
				if(empty($video)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				if($video['video_active']){
					$status = 0;
				} else{
					$status = 1;
				}
				$this->video->handler_update($id_video, array('video_active' => $status));
				jsonResponse('Операция прошла успешно.', 'success', array('status' => $status));
			break;
			// DONE
			case 'change_marketing':
				$this->form_validation->set_rules('video', 'Видео', 'required|xss_clean');
				$this->form_validation->set_rules('marketing', 'Маркетинг', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_video = (int)$this->input->post('video');
				$video = $this->video->handler_get($id_video);
				if(empty($video)){
					jsonResponse('Ошибка: Данные не верны.');
				}
				
				$marketing = $this->input->post('marketing', true);
				if(!isset($video[$marketing])){
					jsonResponse('Ошибка: Данные не верны.');
				}
				
				if($video[$marketing] == 1){
					$status = 0;
				} else{
					$status = 1;
				}
				$this->video->handler_update($id_video, array($marketing => $status));
				jsonResponse('Операция прошла успешно.', 'success', array('status' => $status));
			break;
			// DONE
			case 'add_category':
				$this->form_validation->set_rules('title_ru', 'Название RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ru', 'Meta keywords RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_en', 'Meta keywords En', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ru', 'Meta description RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_en', 'Meta description En', 'required|xss_clean|max_length[250]');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$insert = array(
					'category_title_ru' => $this->input->post('title_ru'),
					'category_title_en' => $this->input->post('title_en'),
					'mk_ru' => $this->input->post('mk_ru'),
					'mk_en' => $this->input->post('mk_en'),
					'md_ru' => $this->input->post('md_ru'),
					'md_en' => $this->input->post('md_en'),
					'category_active' => ($this->input->post('category_active'))?1:0
				);

				$id_category = $this->video->handler_insert_category($insert);

				$config = array(
					'table' => 'video_categories',
					'id' => 'id_category',
					'field' => 'url_ru',
					'title' => 'category_title_ru',
					'replacement' => 'dash'
				);
				$this->load->library('slug', $config);
				$update = array(
					'url_ru' => $this->slug->create_slug(cut_str($insert['category_title_ru'], 200).'-'.$id_category),
					'url_en' => $this->slug->create_slug(cut_str($insert['category_title_en'], 200).'-'.$id_category)
				);
				$this->video->handler_update_category($id_category, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'edit_category':
                $this->form_validation->set_rules('id_category', 'Категория', 'required|xss_clean');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_ru', 'Meta keywords RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_en', 'Meta keywords En', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ru', 'Meta description RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_en', 'Meta description En', 'required|xss_clean|max_length[250]');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_category = (int)$this->input->post('id_category');
				$config = array(
					'table' => 'video_categories',
					'id' => 'id_category',
					'field' => 'url_ru',
					'title' => 'category_title_ru',
					'replacement' => 'dash'
				);
				$this->load->library('slug', $config);
				$update = array(
					'category_title_ru' => $this->input->post('title_ru'),
					'category_title_en' => $this->input->post('title_en'),
					'mk_ru' => $this->input->post('mk_ru'),
					'mk_en' => $this->input->post('mk_en'),
					'md_ru' => $this->input->post('md_ru'),
					'md_en' => $this->input->post('md_en'),
					'category_active' => ($this->input->post('category_active'))?1:0,
					'url_ru' => $this->slug->create_slug(cut_str($this->input->post('title_ru'), 200).'-'.$id_category),
					'url_en' => $this->slug->create_slug(cut_str($this->input->post('title_en'), 200).'-'.$id_category)
				);

				$this->video->handler_update_category($id_category, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'categories_list_dt':
				$params = array(
					'limit' => intVal($_POST['iDisplayLength']),
					'start' => intVal($_POST['iDisplayStart'])
				);

				if ($_POST['iSortingCols'] > 0) {
					for ($i = 0; $i < $_POST['iSortingCols']; $i++) {
						switch ($_POST["mDataProp_" . intval($_POST['iSortCol_' . $i])]) {
							case 'dt_name_ru': $params['sort_by'][] = 'category_title_ru-' . $_POST['sSortDir_' . $i];
							break;
							case 'dt_name_en': $params['sort_by'][] = 'category_title_en-' . $_POST['sSortDir_' . $i];
							break;
						}
					}
				}

				$records = $this->video->handler_get_all_categories($params);
				$records_total = $this->video->handler_get_count_categories($params);

				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);

				foreach ($records as $record) {
					$status = '<a href="#" data-message="Вы уверены что хотите сделать категорию активной?" data-dtype="warning" title="Сделать активной" data-title="Изменение статуса" data-callback="change_status" data-category="'.$record['id_category'].'" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-eye-slash"></i></a>';
					if($record['category_active'] == 1){
						$status = '<a href="#" data-message="Вы уверены что хотите сделать категорию неактивной?" data-dtype="warning" title="Сделать неактивной" data-title="Изменение статуса" data-callback="change_status" data-category="'.$record['id_category'].'" class="btn btn-success btn-xs confirm-dialog"><i class="fa fa-eye"></i></a>';
					}

					$output['aaData'][] = array(
						'dt_name_ru'	=>  $record['category_title_ru'],
						'dt_name_en'	=>  $record['category_title_en'],
						'dt_actions'	=>  $status
											.' <a href="#" data-href="'.base_url('admin/video/popup_forms/edit_category/'.$record['id_category']).'" title="Редактировать" class="btn btn-primary btn-xs call-popup" data-popup="#general_popup_form"><i class="fa fa-pencil"></i></a>'
											.' <a href="#" class="btn btn-danger btn-xs confirm-dialog" title="Удалить" data-message="Вы уверены что хотите удалить категорию?" data-callback="delete_action" data-category="'.$record['id_category'].'"><i class="fa fa-remove"></i></a>'
					);
				}
				jsonResponse('', 'success', $output);
			break;
			// DONE
			case 'delete_category':
                $this->form_validation->set_rules('category', 'Категория', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_category = (int)$this->input->post('category');
				$category = $this->video->handler_get_category($id_category);
				if(empty($category)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$this->video->handler_update_category($id_category, array('category_deleted' => 1));
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			// DONE
			case 'change_status_category':
				$this->form_validation->set_rules('category', 'Категория', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_category = (int)$this->input->post('category');
				$category = $this->video->handler_get_category($id_category);
				if(empty($category)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				if($category['category_active']){
					$status = 0;
				} else{
					$status = 1;
				}
				$this->video->handler_update_category($id_category, array('category_active' => $status));
				jsonResponse('Операция прошла успешно.', 'success', array('status' => $status));
			break;
			// DONE
			case 'upload_photo':
				$path = 'files/video';
				create_dir($path);

				$config['upload_path'] = FCPATH . $path;
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['file_name'] = uniqid();
				$config['min_width']	= '600';
				$config['min_height']	= '400';
				$config['max_size']	= '5000';

				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload()){
					jsonResponse($this->upload->display_errors('',''),'error');
				}

				$data = $this->upload->data();
				$config = array(
					'source_image'      => $data['full_path'],
					'create_thumb'      => false,
					'new_image'         => FCPATH . $path,
					'maintain_ratio'    => true,
					'width'             => 600
				);

				$this->load->library('image_lib');
				$this->image_lib->initialize($config);
				$this->image_lib->resize();

				$info = new StdClass;
				$info->filename = $data['file_name'];
				$files[] = $info;
				jsonResponse('', 'success', array("files" => $files));
			break;
		}
	}
}
?>