<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Video_model extends CI_Model{
	var $video = "video";
	var $video_categories = "video_categories";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->video, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_video = 0, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_video', $id_video);
		$this->db->update($this->video, $data);
	}

	function handler_delete($id_video = 0){
		$this->db->where('id_video', $id_video);
		$this->db->delete($this->video);
	}

	function handler_get($id_video = 0){
		$this->db->where('id_video', $id_video);
		return $this->db->get($this->video)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " video_date DESC ";
		$video_deleted = 0;
		$category_deleted = 0;

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

		$this->db->select("{$this->video}.*");
		$this->db->select("{$this->video_categories}.*");
		$this->db->from("{$this->video}");
		$this->db->join("{$this->video_categories}", "{$this->video}.video_category = {$this->video_categories}.id_category", "inner");

        if(isset($id_video)){
			$this->db->where_in("{$this->video}.id_video", $id_video);
        }

        if(isset($video_active)){
			$this->db->where("{$this->video}.video_active", $video_active);
        }

        if(isset($video_deleted)){
			$this->db->where("{$this->video}.video_deleted", $video_deleted);
        }

        if(isset($video_favorite)){
			$this->db->where("{$this->video}.video_favorite", $video_favorite);
        }
		
		if(isset($id_category)){
			$this->db->where("{$this->video}.video_category", $id_category);
		}

        if(isset($category_deleted)){
			$this->db->where("{$this->video_categories}.category_deleted", $category_deleted);
        }

        if(isset($category_active)){
			$this->db->where("{$this->video_categories}.category_active", $category_active);
        }

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get()->result_array();
	}

	function handler_get_count($conditions = array()){
		$video_deleted = 0;
		$category_deleted = 0;

        extract($conditions);

		$this->db->select("{$this->video}.*");
		$this->db->from("{$this->video}");
		$this->db->join("{$this->video_categories}", "{$this->video}.video_category = {$this->video_categories}.id_category", "inner");

        if(isset($id_video)){
			$this->db->where_in("{$this->video}.id_video", $id_video);
        }

        if(isset($video_active)){
			$this->db->where("{$this->video}.video_active", $video_active);
        }

        if(isset($video_deleted)){
			$this->db->where("{$this->video}.video_deleted", $video_deleted);
        }
		
		if(isset($video_favorite)){
			$this->db->where("{$this->video}.video_favorite", $video_favorite);
		}
		
		if(isset($id_category)){
			$this->db->where("{$this->video}.video_category", $id_category);
		}

        if(isset($category_deleted)){
			$this->db->where("{$this->video_categories}.category_deleted", $category_deleted);
        }

        if(isset($category_active)){
			$this->db->where("{$this->video_categories}.category_active", $category_active);
        }

		return $this->db->count_all_results();
	}

	// CATEGORIES
	function handler_insert_category($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->video_categories, $data);
		return $this->db->insert_id();
	}

	function handler_update_category($id_category = 0, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_category', $id_category);
		$this->db->update($this->video_categories, $data);
	}
	
	function handler_get_category($id_category = 0){
		$this->db->where('id_category', $id_category);
		return $this->db->get($this->video_categories)->row_array();
	}
	
	function handler_get_all_categories($conditions = array()){
		$order_by = " id_category ASC ";
		$category_deleted = 0;

		extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

		if(isset($id_category)){
			$this->db->where_in('id_category', $id_category);
		}

		if(isset($category_active)){
			$this->db->where('category_active', $category_active);
		}

		if(isset($category_deleted)){
			$this->db->where('category_deleted', $category_deleted);
		}

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->video_categories)->result_array();
	}

	function handler_get_count_categories($conditions = array()){
		$category_deleted = 0;

		extract($conditions);
		
		if(isset($id_category)){
			$this->db->where_in('id_category', $id_category);
		}

		if(isset($category_active)){
			$this->db->where('category_active', $category_active);
		}

		if(isset($category_deleted)){
			$this->db->where('category_deleted', $category_deleted);
		}

		return $this->db->count_all_results($this->video_categories);
	}
}
?>
