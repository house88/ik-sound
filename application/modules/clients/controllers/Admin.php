<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse(lang_line('error_message_not_privileged', false));
			}
		}

		$this->data = array();
		$this->data['main_title'] = 'Клиенты';

		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Clients_model", "clients");

        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$this->data['main_content'] = 'admin/modules/clients/list';
		$this->load->view('admin/page', $this->data);
	}
	
	function popup_forms(){
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$action = $this->uri->segment(4);
		switch ($action) {
			// DONE
			case 'add':
				$content = $this->load->view('admin/modules/clients/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			// DONE
			case 'edit':
				$id_client = (int) $this->uri->segment(5);
				$this->data['client'] = $this->clients->handler_get($id_client);
				if(empty($this->data['client'])){
					jsonResponse('Ошибка: Данные не верны.');
				}
				
				$content = $this->load->view('admin/modules/clients/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			// DONE
			case 'add':
				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('link', 'Ссылка на веб-сайт', 'xss_clean|max_length[500]');
				$this->form_validation->set_rules('client_photo', 'Фото', 'required');
				$this->form_validation->set_rules('client_published_date', 'Дата публикации', 'required');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/clients/'.$remove_photo);
					}
				}

				$published_on = date('Y-m-d H:i:s');
				if(validateDate($this->input->post('client_published_date'), 'd.m.Y H:i:s')){
					$published_on = getDateFormat($this->input->post('client_published_date'), 'd.m.Y H:i:s', 'Y-m-d H:i:s');
				}

				$insert = array(
					'client_title' => $this->input->post('title'),
					'client_link' => $this->input->post('link'),
					'client_photo' => $this->input->post('client_photo', true),
					'client_published_date' => $published_on
				);

				$id_client = $this->clients->handler_insert($insert);
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'edit':
                $this->form_validation->set_rules('id_client', 'Клиент', 'required|xss_clean');
				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('link', 'Ссылка на веб-сайт', 'xss_clean|max_length[500]');
				$this->form_validation->set_rules('client_photo', 'Фото', 'required');
				$this->form_validation->set_rules('client_published_date', 'Дата публикации', 'required');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_client = (int)$this->input->post('id_client');
				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/clients/'.$remove_photo);
					}
				}

				$published_on = date('Y-m-d H:i:s');
				if(validateDate($this->input->post('client_published_date'), 'd.m.Y H:i:s')){
					$published_on = getDateFormat($this->input->post('client_published_date'), 'd.m.Y H:i:s', 'Y-m-d H:i:s');
				}

				$update = array(
					'client_title' => $this->input->post('title'),
					'client_link' => $this->input->post('link'),
					'client_photo' => $this->input->post('client_photo', true),
					'client_published_date' => $published_on
				);

				$this->clients->handler_update($id_client, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'list_dt':
				$params = array(
					'limit' => intVal($_POST['iDisplayLength']),
					'start' => intVal($_POST['iDisplayStart'])
				);

				if ($_POST['iSortingCols'] > 0) {
					for ($i = 0; $i < $_POST['iSortingCols']; $i++) {
						switch ($_POST["mDataProp_" . intval($_POST['iSortCol_' . $i])]) {
							case 'dt_id': $params['sort_by'][] = 'id_client-' . $_POST['sSortDir_' . $i];
							break;
							case 'dt_name': $params['sort_by'][] = 'client_title-' . $_POST['sSortDir_' . $i];
							break;
						}
					}
				}

				$records = $this->clients->handler_get_all($params);
				$records_total = $this->clients->handler_get_count($params);

				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);

				foreach ($records as $record) {
					$status = '<a href="#" data-message="Вы уверены что хотите сделать клиента неактивным?" title="Сделать неактивным" data-title="Изменение статуса" data-callback="change_status" data-client="'.$record['id_client'].'" class="btn btn-success btn-xs confirm-dialog"><i class="fa fa-eye"></i></a>';
					if($record['client_active'] == 0){
						$status = '<a href="#" data-message="Вы уверены что хотите сделать клиента активным?" title="Сделать активным" data-title="Изменение статуса" data-callback="change_status" data-client="'.$record['id_client'].'" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-eye-slash"></i></a>';
					}

					$output['aaData'][] = array(
						'dt_id'			=>  $record['id_client'],
						'dt_photo'		=>  '<img src="'.base_url(getImage('files/clients/'.$record['client_photo'])).'" class="img-thumbnail mw-100pr mh-100pr">',
						'dt_name'		=>  $record['client_title'],
						'dt_published'	=>  getDateFormat($record['client_published_date'], 'Y-m-d H:i:s', 'd.m.Y H:i:s'),
						'dt_actions'	=>  '<div class="btn-group">
												'.$status.'
												<a href="#" data-href="'.base_url('admin/clients/popup_forms/edit/'.$record['id_client']).'" title="Редактировать" class="btn btn-primary btn-xs call-popup" data-popup="#general_popup_form"><i class="fa fa-pencil"></i></a>
												<a href="#" class="btn btn-danger btn-xs confirm-dialog" title="Удалить" data-message="Вы уверены что хотите удалить клиента?" data-callback="delete_action" data-client="'.$record['id_client'].'"><i class="fa fa-remove"></i></a>
											</div>'
					);
				}
				jsonResponse('', 'success', $output);
			break;
			// DONE
			case 'upload_photo':
				$path = 'files/clients';
				create_dir($path);

				$config['upload_path'] = FCPATH . $path;
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['file_name'] = uniqid();
				$config['min_width']	= '100';
				$config['max_size']	= '1000';

				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload()){
					jsonResponse($this->upload->display_errors('',''),'error');
				}

				$data = $this->upload->data();
				$config = array(
					'source_image'      => $data['full_path'],
					'create_thumb'      => false,
					'new_image'         => FCPATH . $path,
					'maintain_ratio'    => true,
					'width'             => 100
				);

				$this->load->library('image_lib');
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
				$info = new StdClass;
				$info->filename = $data['file_name'];
				$files[] = $info;
				jsonResponse('', 'success', array("files" => $files));
			break;
			// DONE
			case 'delete':
				$this->form_validation->set_rules('client', 'Клиент', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_client = (int)$this->input->post('client');
				$client = $this->clients->handler_get($id_client);
				if(empty($client)){
					jsonResponse('Клиент не существует.');
				}

				if(!empty($client['client_photo'])){
					@unlink('files/clients/'.$client['client_photo']);
				}

				$this->clients->handler_delete($id_client);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			// DONE
			case 'change_status':
				$this->form_validation->set_rules('client', 'Клиент', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_client = (int)$this->input->post('client');
				$client = $this->clients->handler_get($id_client);
				if(empty($client)){
					jsonResponse('Клиент не существует.');
				}
				if($client['client_active']){
					$status = 0;
				} else{
					$status = 1;
				}
				$this->clients->handler_update($id_client, array('client_active' => $status));
				jsonResponse('Операция прошла успешно.', 'success', array('status' => $status));
			break;
		}
	}
}
?>