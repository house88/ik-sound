<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Clients_model extends CI_Model{
	var $clients = "clients";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->clients, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_client, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_client', $id_client);
		$this->db->update($this->clients, $data);
	}

	function handler_delete($id_client){
		$this->db->where('id_client', $id_client);
		$this->db->delete($this->clients);
	}

	function handler_get($id_client){
		$this->db->where('id_client', $id_client);
		return $this->db->get($this->clients)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " client_published_date DESC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

        if(isset($id_client)){
			$this->db->where_in('id_client', $id_client);
        }

        if(isset($client_active)){
			$this->db->where('client_active', $client_active);
        }

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->clients)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($id_client)){
			$this->db->where_in('id_client', $id_client);
        }

        if(isset($client_active)){
			$this->db->where('client_active', $client_active);
        }

		return $this->db->count_all_results($this->clients);
	}
}
?>
