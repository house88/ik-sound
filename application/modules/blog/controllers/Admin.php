<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse(lang_line('error_message_not_privileged', false));
			}
		}

		$this->data = array();
		$this->data['main_title'] = 'Блог';

		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Blog_model", "blog");

        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$this->data['main_content'] = 'admin/modules/blog/list';
		$this->load->view('admin/page', $this->data);
	}

	function add(){
		$this->data['main_content'] = 'admin/modules/blog/add';
		$this->load->view('admin/page', $this->data);
	}

	function edit(){
		$id_blog = (int)$this->uri->segment(4);
		$this->data['blog'] = $this->blog->handler_get($id_blog);
		$this->data['main_content'] = 'admin/modules/blog/edit';
		$this->load->view('admin/page', $this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				$this->form_validation->set_rules('title_ru', 'Название RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('description_ru', 'Текст статьи RU', 'required');
				$this->form_validation->set_rules('description_en', 'Текст статьи En', 'required');
				$this->form_validation->set_rules('stext_ru', 'Краткое описание RU', 'required|xss_clean|max_length[500]');
				$this->form_validation->set_rules('stext_en', 'Краткое описание En', 'required|xss_clean|max_length[500]');
				$this->form_validation->set_rules('mk_ru', 'Meta keywords RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_en', 'Meta keywords En', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ru', 'Meta description RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_en', 'Meta description En', 'required|xss_clean|max_length[250]');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/blog/'.$remove_photo);
					}
				}

				$remove_files = $this->input->post('remove_files');
				if(!empty($remove_files)){
					foreach($remove_files as $remove_file){
						@unlink('files/blog/audio/'.$remove_file);
					}
				}
				
				$audio_files = (is_array($this->input->post('audio_files')))?array_filter($this->input->post('audio_files')):array();
				$audio_client_names = (is_array($this->input->post('audio_client_names')))?array_filter($this->input->post('audio_client_names')):array();
				$filtered_audio_files = array();
				if(!empty($audio_files)){
					foreach ($audio_files as $file_key => $audio_file) {
						if(empty($audio_client_names[$file_key])){
							continue;
						}

						$filtered_audio_files[] = array(
							'name' => $audio_file,
							'title' => $audio_client_names[$file_key]
						);
					}
				}

				$insert = array(
					'blog_title_ru' => $this->input->post('title_ru'),
					'blog_title_en' => $this->input->post('title_en'),
					'blog_description_ru' => $this->input->post('description_ru'),
					'blog_description_en' => $this->input->post('description_en'),
					'blog_small_description_ru' => $this->input->post('stext_ru'),
					'blog_small_description_en' => $this->input->post('stext_en'),
					'mk_ru' => $this->input->post('mk_ru'),
					'mk_en' => $this->input->post('mk_en'),
					'md_ru' => $this->input->post('md_ru'),
					'md_en' => $this->input->post('md_en'),
					'blog_photo' => ($this->input->post('blog_photo', true))?$this->input->post('blog_photo'):'',
					'blog_audio' => json_encode($filtered_audio_files)
				);

				$id_blog = $this->blog->handler_insert($insert);

				$config = array(
					'table' => 'blog',
					'id' => 'id_blog',
					'field' => 'url',
					'title' => 'blog_title_ru',
					'replacement' => 'dash'
				);
				$this->load->library('slug', $config);
				$update = array(
					'url_ru' => $this->slug->create_slug(cut_str($insert['blog_title_ru'], 200).'-'.$id_blog),
					'url_en' => $this->slug->create_slug(cut_str($insert['blog_title_en'], 200).'-'.$id_blog)
				);
				$this->blog->handler_update($id_blog, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'edit':
                $this->form_validation->set_rules('blog', 'Блог', 'required|xss_clean');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('description_ru', 'Текст статьи RU', 'required');
				$this->form_validation->set_rules('description_en', 'Текст статьи En', 'required');
				$this->form_validation->set_rules('stext_ru', 'Краткое описание RU', 'required|xss_clean|max_length[500]');
				$this->form_validation->set_rules('stext_en', 'Краткое описание En', 'required|xss_clean|max_length[500]');
				$this->form_validation->set_rules('mk_ru', 'Meta keywords RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('mk_en', 'Meta keywords En', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_ru', 'Meta description RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('md_en', 'Meta description En', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('blog_date', 'Дата', 'required|xss_clean');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_blog = (int)$this->input->post('blog');
				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/blog/'.$remove_photo);
					}
				}
				
				$audio_files = (is_array($this->input->post('audio_files')))?array_filter($this->input->post('audio_files')):array();
				$audio_client_names = (is_array($this->input->post('audio_client_names')))?array_filter($this->input->post('audio_client_names')):array();
				$filtered_audio_files = array();
				if(!empty($audio_files)){
					foreach ($audio_files as $file_key => $audio_file) {
						if(empty($audio_client_names[$file_key])){
							continue;
						}

						$filtered_audio_files[] = array(
							'name' => $audio_file,
							'title' => $audio_client_names[$file_key]
						);
					}
				}

				$config = array(
					'table' => 'blog',
					'id' => 'id_blog',
					'field' => 'url',
					'title' => 'blog_title_ru',
					'replacement' => 'dash'
				);
				$this->load->library('slug', $config);
				$update = array(
					'blog_title_ru' => $this->input->post('title_ru'),
					'blog_title_en' => $this->input->post('title_en'),
					'blog_description_ru' => $this->input->post('description_ru'),
					'blog_description_en' => $this->input->post('description_en'),
					'blog_small_description_ru' => $this->input->post('stext_ru'),
					'blog_small_description_en' => $this->input->post('stext_en'),
					'mk_ru' => $this->input->post('mk_ru'),
					'mk_en' => $this->input->post('mk_en'),
					'md_ru' => $this->input->post('md_ru'),
					'md_en' => $this->input->post('md_en'),
					'url_ru' => $this->slug->create_slug(cut_str($this->input->post('title_ru'), 200).'-'.$id_blog),
					'url_en' => $this->slug->create_slug(cut_str($this->input->post('title_en'), 200).'-'.$id_blog),
					'blog_photo' => ($this->input->post('blog_photo', true))?$this->input->post('blog_photo'):'',
					'blog_audio' => json_encode($filtered_audio_files),
					'blog_date' => formatDate($this->input->post('blog_date'), 'Y-m-d H:i:s')
				);

				$this->blog->handler_update($id_blog, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'list_dt':
				$params = array(
					'limit' => intVal($_POST['iDisplayLength']),
					'start' => intVal($_POST['iDisplayStart'])
				);

				if ($_POST['iSortingCols'] > 0) {
					for ($i = 0; $i < $_POST['iSortingCols']; $i++) {
						switch ($_POST["mDataProp_" . intval($_POST['iSortCol_' . $i])]) {
							case 'dt_id': $params['sort_by'][] = 'id_blog-' . $_POST['sSortDir_' . $i];
							break;
							case 'dt_date': $params['sort_by'][] = 'blog_date-' . $_POST['sSortDir_' . $i];
							break;
							case 'dt_name': $params['sort_by'][] = 'blog_title_ru-' . $_POST['sSortDir_' . $i];
							break;
						}
					}
				}

				$records = $this->blog->handler_get_all($params);
				$records_total = $this->blog->handler_get_count($params);

				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);

				foreach ($records as $record) {
					$status = '<a href="#" data-message="Вы уверены что хотите сделать блог неактивным?" title="Сделать неактивным" data-title="Изменение статуса" data-callback="change_status" data-blog="'.$record['id_blog'].'" class="btn btn-success btn-xs confirm-dialog"><i class="fa fa-eye"></i></a>';
					if($record['blog_active'] == 0){
						$status = '<a href="#" data-message="Вы уверены что хотите сделать блог активным?" title="Сделать активным" data-title="Изменение статуса" data-callback="change_status" data-blog="'.$record['id_blog'].'" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-eye-slash"></i></a>';
					}

					$output['aaData'][] = array(
						'dt_id'			=>  $record['id_blog'],
						'dt_photo'		=>  '<img src="'.base_url(getImage('files/blog/'.$record['blog_photo'])).'" class="img-thumbnail mw-100pr mh-100pr">',
						'dt_name'		=>  '<a href="'.base_url('blog/post/'.$record['url_ru']).'" target="_blank">'.$record['blog_title_ru'].'</a>',
						'dt_date'		=>  formatDate($record['blog_date'], 'd.m.Y H:i:s'),
						'dt_actions'	=>  $status
											.' <a href="'.base_url('admin/blog/edit/'.$record['id_blog']).'" title="Редактировать" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>'
											.' <a href="#" class="btn btn-danger btn-xs confirm-dialog" title="Удалить" data-message="Вы уверены что хотите удалить блог?" data-callback="delete_action" data-blog="'.$record['id_blog'].'"><i class="fa fa-remove"></i></a>'
					);
				}
				jsonResponse('', 'success', $output);
			break;
			case 'upload_photo':
				$path = 'files/blog';
				create_dir($path);

				$config['upload_path'] = FCPATH . $path;
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['file_name'] = uniqid();
				$config['min_width']	= '800';
				$config['min_height']	= '100';
				$config['max_size']	= '5000';

				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload()){
					jsonResponse($this->upload->display_errors('',''),'error');
				}

				$data = $this->upload->data();
				$config = array(
					'source_image'      => $data['full_path'],
					'create_thumb'      => false,
					'new_image'         => FCPATH . $path,
					'maintain_ratio'    => true,
					'width'             => 800
				);

				$this->load->library('image_lib');
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
				$info = new StdClass;
				$info->filename = $data['file_name'];
				$files[] = $info;
				jsonResponse('', 'success', array("files" => $files));
			break;
			case 'upload_audio':
				$path = 'files/blog/audio';
				create_dir($path);

				$config['upload_path'] = FCPATH . $path;
				$config['allowed_types'] = 'mp3';
				$config['file_name'] = uniqid();
				$config['max_size']	= '20000';

				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload('audiofile')){
					jsonResponse($this->upload->display_errors('',''),'error');
				}

				$data = $this->upload->data();
				$info = new StdClass;
				$info->client_name = str_replace($data['file_ext'],'',$data['client_name']);
				$info->name = $data['file_name'];
				jsonResponse('', 'success', array("file" => $info, 'data' => $data));
			break;
			case 'delete':
				$this->form_validation->set_rules('blog', 'Блог', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_blog = (int)$this->input->post('blog');
				$blog = $this->blog->handler_get($id_blog);
				if(empty($blog)){
					jsonResponse('Блог не существует.');
				}

				if(!empty($blog['blog_photo'])){
					@unlink('files/blog/'.$blog['blog_photo']);
				}

				$this->blog->handler_delete($id_blog);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'change_status':
				$this->form_validation->set_rules('blog', 'Блог', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_blog = (int)$this->input->post('blog');
				$blog = $this->blog->handler_get($id_blog);
				if(empty($blog)){
					jsonResponse('Блог не существует.');
				}
				if($blog['blog_active']){
					$status = 0;
				} else{
					$status = 1;
				}
				$this->blog->handler_update($id_blog, array('blog_active' => $status));
				jsonResponse('Операция прошла успешно.', 'success', array('status' => $status));
			break;
		}
	}
}
?>