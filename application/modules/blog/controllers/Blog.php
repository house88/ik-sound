<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->data['site_theme'] = $this->site_theme = $this->config->item('site_theme');
		$this->data['lang'] = $this->ulang = $this->lang->lang();
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title']['setting_value_'.$this->ulang];
		$this->data['skeywords'] = $this->data['settings']['mk']['setting_value_'.$this->ulang];
		$this->data['sdescription'] = $this->data['settings']['md']['setting_value_'.$this->ulang];
		
		$this->load->model('Blog_model', 'blog');
		$this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
		
		if($this->data['settings']['blog_is_active'][lang_column('setting_value')] != 1){
			return_404($this->data);
		}
	}

	function index(){
		$uri = $this->uri->uri_to_assoc(3);
		$page = 1;
		if(isset($uri['page']) && (int)$uri['page'] > 0){
			$page = (int)$uri['page'];
		} elseif($this->input->post('page')){
			$page = (int) $this->input->post('page');
			
			if($page > 1){
				$uri['page'] = $page;
			}
		}

		if($page < 1){
			$page = 1;
		}

		$this->data['page'] = $page;		
		$this->data['limit'] = $limit = (int) $this->data['settings']['blog_limit'][lang_column('setting_value')];
		$start = ($page <= 1) ? 0 : (($page * $limit) - $limit);
		
		$params = array(
			'limit' => $limit,
			'start' => $start,
			'blog_active' => 1
		);

		$this->data['records_total'] = $records_total = $this->blog->handler_get_count($params);
		$this->data['blogs'] = $this->blog->handler_get_all($params);

		if($page == 1 && !empty($this->data['blogs'])){
			$first_blog = $this->data['blogs'][0];
			$this->data['first_blog'] = $first_blog;
			unset($this->data['blogs'][0]);
		}

		$this->data['uri_ru'] = 'ru/blog'.((!empty($uri))?'/'.$this->uri->assoc_to_uri($uri):'');
		$this->data['uri_en'] = 'en/blog'.((!empty($uri))?'/'.$this->uri->assoc_to_uri($uri):'');
		if (!$this->input->is_ajax_request()) {
			$this->data['breadcrumbs'] = $this->breadcrumbs;
			$this->data['main_content'] = 'modules/blog/blogs_view';
			$this->data['og_meta'] = $this->page_meta->handler_get('blog');
			$this->load->view(get_theme_view('page_template'), $this->data);
        } else{
			if($this->input->post('full_render')){
				$content = $this->load->view(get_theme_view('modules/blog/blogs_view'), $this->data, true);
			} else{
				$content = $this->load->view(get_theme_view('modules/blog/blogs_list_view'), $this->data, true);
			}
			jsonResponse('', 'success', array('content' => $content, 'records_total' => $records_total, 'stitle' => $this->data['stitle'], 'uri_ru' => $this->data['uri_ru'], 'uri_en' => $this->data['uri_en']));
		}
	}

	function post(){
		$uri = explode('/', $this->uri->uri_string());
		$blog_url = end($uri);
		$this->data['blog'] = $this->blog->handler_get_by_url($blog_url);
		if(empty($this->data['blog'])){
			return_404($this->data);
		}

        if($this->data['blog']['blog_active'] == 0){
            return_404($this->data);
        }

		if($this->data['blog'][lang_column('url')] != $blog_url){
			redirect($this->ulang.'/blog/post/'.$this->data['blog'][lang_column('url')]);
		}

		$this->breadcrumbs[] = array(
			'title' => $this->data['blog']['blog_title_'.$this->ulang],
			'link' => base_url($this->ulang.'/blog/post/'.$this->data['blog']['url_'.$this->ulang])
		);

		$this->data['uri_ru'] = 'ru/blog/post/'.$this->data['blog']['url_ru'];
		$this->data['uri_en'] = 'en/blog/post/'.$this->data['blog']['url_en'];
		$this->data['stitle'] = $this->data['blog']['blog_title_'.$this->ulang];
		$this->data['skeywords'] = $this->data['blog']['mk_'.$this->ulang];
		$this->data['sdescription'] = $this->data['blog']['md_'.$this->ulang];
		
		if (!$this->input->is_ajax_request()) {
			$this->data['breadcrumbs'] = $this->breadcrumbs;
			$this->data['main_content'] = 'modules/blog/blog_post_view';
			$this->data['og_meta'] = array(
				'page_title_ru' => $this->data['blog']['blog_title_ru'],
				'page_title_en' => $this->data['blog']['blog_title_en'],
				'page_mk_ru' => $this->data['blog']['mk_ru'],
				'page_mk_en' => $this->data['blog']['mk_en'],
				'page_md_ru' => $this->data['blog']['md_ru'],
				'page_md_en' => $this->data['blog']['md_en'],
				'page_image' => $this->data['blog']['blog_photo'],
				'page_image_src' => file_modification_time('files/blog/'.$this->data['blog']['blog_photo'])
			);
			$this->load->view(get_theme_view('page_template'), $this->data);
		} else{
			$content = $this->load->view(get_theme_view('modules/blog/blog_post_view'), $this->data, true);
			jsonResponse('', 'success', array('content' => $content, 'stitle' => $this->data['stitle'], 'uri_ru' => $this->data['uri_ru'], 'uri_en' => $this->data['uri_en']));
		}
	}
}
?>