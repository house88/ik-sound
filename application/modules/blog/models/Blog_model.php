<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Blog_model extends CI_Model{
	var $blog = "blog";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->blog, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_blog, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_blog', $id_blog);
		$this->db->update($this->blog, $data);
	}

	function handler_delete($id_blog){
		$this->db->where('id_blog', $id_blog);
		$this->db->delete($this->blog);
	}

	function handler_get($id_blog){
		$this->db->where('id_blog', $id_blog);
		return $this->db->get($this->blog)->row_array();
	}

	function handler_get_by_url($url){
		$this->db->where('url_ru', $url);
		$this->db->or_where('url_en', $url);
		return $this->db->get($this->blog)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " blog_date DESC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

        if(isset($id_blog)){
			$this->db->where_in('id_blog', $id_blog);
        }

        if(isset($blog_active)){
			$this->db->where('blog_active', $blog_active);
        }

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->blog)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($id_blog)){
			$this->db->where_in('id_blog', $id_blog);
        }

        if(isset($blog_active)){
			$this->db->where('blog_active', $blog_active);
        }

		return $this->db->count_all_results($this->blog);
	}
}
?>
