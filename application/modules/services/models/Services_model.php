<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Services_model extends CI_Model{
	var $services = "services";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->services, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_service, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_service', $id_service);
		$this->db->update($this->services, $data);
	}

	function handler_delete($id_service){
		$this->db->where('id_service', $id_service);
		$this->db->delete($this->services);
	}

	function handler_get($id_service){
		$this->db->where('id_service', $id_service);
		return $this->db->get($this->services)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " id_service ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

        if(isset($id_service)){
			$this->db->where_in('id_service', $id_service);
        }

        if(isset($service_active)){
			$this->db->where('service_active', $service_active);
        }

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->services)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($id_service)){
			$this->db->where_in('id_service', $id_service);
        }

        if(isset($service_active)){
			$this->db->where('service_active', $service_active);
        }

		return $this->db->count_all_results($this->services);
	}
}
?>
