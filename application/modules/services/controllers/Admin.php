<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->lauth->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse(lang_line('error_message_not_privileged', false));
			}
		}

		$this->data = array();
		$this->data['main_title'] = 'Услуги';

		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Services_model", "services");

        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$this->data['main_content'] = 'admin/modules/services/list';
		$this->load->view('admin/page', $this->data);
	}
	
	function popup_forms(){
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$action = $this->uri->segment(4);
		switch ($action) {
			// DONE
			case 'add':
				$content = $this->load->view('admin/modules/services/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			// DONE
			case 'edit':
				$id_service = (int) $this->uri->segment(5);
				$this->data['service'] = $this->services->handler_get($id_service);
				if(empty($this->data['service'])){
					jsonResponse('Ошибка: Данные не верны.');
				}
				
				$content = $this->load->view('admin/modules/services/form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			// DONE
			case 'add':
				$this->form_validation->set_rules('title_ru', 'Название RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('description_ru', 'Текст RU', 'required|xss_clean|max_length[1000]');
				$this->form_validation->set_rules('description_en', 'Текст En', 'required|xss_clean|max_length[1000]');
				$this->form_validation->set_rules('service_photo', 'Фото', 'required');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/services/'.$remove_photo);
					}
				}

				$insert = array(
					'service_title_ru' => $this->input->post('title_ru'),
					'service_title_en' => $this->input->post('title_en'),
					'service_description_ru' => $this->input->post('description_ru'),
					'service_description_en' => $this->input->post('description_en'),
					'service_photo' => $this->input->post('service_photo', true)
				);

				$id_service = $this->services->handler_insert($insert);
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'edit':
                $this->form_validation->set_rules('id_service', 'Услуга', 'required|xss_clean');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название En', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('description_ru', 'Текст RU', 'required|xss_clean|max_length[1000]');
				$this->form_validation->set_rules('description_en', 'Текст En', 'required|xss_clean|max_length[1000]');
				$this->form_validation->set_rules('service_photo', 'Фото', 'required');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_service = (int)$this->input->post('id_service');
				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/services/'.$remove_photo);
					}
				}

				$update = array(
					'service_title_ru' => $this->input->post('title_ru'),
					'service_title_en' => $this->input->post('title_en'),
					'service_description_ru' => $this->input->post('description_ru'),
					'service_description_en' => $this->input->post('description_en'),
					'service_photo' => $this->input->post('service_photo', true)
				);

				$this->services->handler_update($id_service, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			// DONE
			case 'list_dt':
				$params = array(
					'limit' => intVal($_POST['iDisplayLength']),
					'start' => intVal($_POST['iDisplayStart'])
				);

				if ($_POST['iSortingCols'] > 0) {
					for ($i = 0; $i < $_POST['iSortingCols']; $i++) {
						switch ($_POST["mDataProp_" . intval($_POST['iSortCol_' . $i])]) {
							case 'dt_id': $params['sort_by'][] = 'id_service-' . $_POST['sSortDir_' . $i];
							break;
							case 'dt_name': $params['sort_by'][] = 'service_title_ru-' . $_POST['sSortDir_' . $i];
							break;
						}
					}
				}

				$records = $this->services->handler_get_all($params);
				$records_total = $this->services->handler_get_count($params);

				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);

				foreach ($records as $record) {
					$status = '<a href="#" data-message="Вы уверены что хотите сделать услугу неактивной?" title="Сделать неактивной" data-title="Изменение статуса" data-callback="change_status" data-service="'.$record['id_service'].'" class="btn btn-success btn-xs confirm-dialog"><i class="fa fa-eye"></i></a>';
					if($record['service_active'] == 0){
						$status = '<a href="#" data-message="Вы уверены что хотите сделать услугу активной?" title="Сделать активной" data-title="Изменение статуса" data-callback="change_status" data-service="'.$record['id_service'].'" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-eye-slash"></i></a>';
					}

					$output['aaData'][] = array(
						'dt_id'			=>  $record['id_service'],
						'dt_photo'		=>  '<img src="'.base_url(getImage('files/services/'.$record['service_photo'])).'" class="img-thumbnail mw-100pr mh-100pr">',
						'dt_name'		=>  $record['service_title_ru'],
						'dt_actions'	=>  '<div class="btn-group">
												'.$status.'
												<a href="#" data-href="'.base_url('admin/services/popup_forms/edit/'.$record['id_service']).'" title="Редактировать" class="btn btn-primary btn-xs call-popup" data-popup="#general_popup_form"><i class="fa fa-pencil"></i></a>
												<a href="#" class="btn btn-danger btn-xs confirm-dialog" title="Удалить" data-message="Вы уверены что хотите удалить услугу?" data-callback="delete_action" data-service="'.$record['id_service'].'"><i class="fa fa-remove"></i></a>
											</div>'
					);
				}
				jsonResponse('', 'success', $output);
			break;
			// DONE
			case 'upload_photo':
				$path = 'files/services';
				create_dir($path);

				$config['upload_path'] = FCPATH . $path;
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['file_name'] = uniqid();
				$config['min_width']	= '140';
				$config['min_height']	= '140';
				$config['max_size']	= '1000';

				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload()){
					jsonResponse($this->upload->display_errors('',''),'error');
				}

				$data = $this->upload->data();
				$config = array(
					'source_image'      => $data['full_path'],
					'create_thumb'      => false,
					'new_image'         => FCPATH . $path,
					'maintain_ratio'    => true,
					'width'             => 140,
					'width'             => 140
				);

				$this->load->library('image_lib');
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
				$info = new StdClass;
				$info->filename = $data['file_name'];
				$files[] = $info;
				jsonResponse('', 'success', array("files" => $files));
			break;
			// DONE
			case 'delete':
				$this->form_validation->set_rules('service', 'Услуга', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_service = (int)$this->input->post('service');
				$service = $this->services->handler_get($id_service);
				if(empty($service)){
					jsonResponse('Услуга не существует.');
				}

				if(!empty($service['service_photo'])){
					@unlink('files/services/'.$service['service_photo']);
				}

				$this->services->handler_delete($id_service);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			// DONE
			case 'change_status':
				$this->form_validation->set_rules('service', 'Услуга', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_service = (int)$this->input->post('service');
				$service = $this->services->handler_get($id_service);
				if(empty($service)){
					jsonResponse('Услуга не существует.');
				}
				if($service['service_active']){
					$status = 0;
				} else{
					$status = 1;
				}
				$this->services->handler_update($id_service, array('service_active' => $status));
				jsonResponse('Операция прошла успешно.', 'success', array('status' => $status));
			break;
		}
	}
}
?>