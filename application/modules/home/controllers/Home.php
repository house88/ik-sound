<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->data['lang'] = $this->ulang = $this->lang->lang();
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title']['setting_value_'.$this->ulang];
		$this->data['skeywords'] = $this->data['settings']['mk']['setting_value_'.$this->ulang];
		$this->data['sdescription'] = $this->data['settings']['md']['setting_value_'.$this->ulang];
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		if (!$this->input->is_ajax_request()) {
			$this->data['breadcrumbs'] = $this->breadcrumbs;
			$this->data['main_content'] = 'modules/home/home_view';
			$this->data['og_meta'] = $this->page_meta->handler_get('home');
			$this->load->view(get_theme_view('page_template'), $this->data);
        } else{
			$content = $this->load->view(get_theme_view('modules/home/home_view'), $this->data, true);
			jsonResponse('', 'success', array('content' => $content, 'stitle' => $this->data['stitle']));
		}
	}
}
?>