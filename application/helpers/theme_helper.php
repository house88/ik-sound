<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function get_theme_view($view = ''){
	$CI = &get_instance();
	return $CI->config->item('client_theme_path') . '/' . $view;
}
?>
