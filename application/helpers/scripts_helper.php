<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function br2nl($str) {
	$str = preg_replace("/(\r\n|\n|\r)/", "", $str);
	return preg_replace("/\<br(\s*)?\/?\>/i", "\n", $str);
}

function get_embed_video_link($link){
    $components = explode('=', $link);
    $video_id = $components[1];
    return 'https://youtube.com/embed/'.$video_id;
}

function getImage($img_path, $template_img = 'theme/images/no_image.png'){
	$path_parts = pathinfo($img_path);
	if(!empty($path_parts['extension']) && file_exists($img_path)){
		return $img_path;
	}else{
		return $template_img;
	}
}

function formatDate($date, $format = "d/m/Y H:i:s", $lang = 'en'){
	$str_to_time = strtotime($date);
	if($str_to_time && $date != '0000-00-00 00:00:00'){
		return date($format, $str_to_time);
	} else{
		return '&mdash;';
	} 
}

function validateDate($date, $format = "Y-m-d H:i:s"){
    $d = date_create_from_format($format, $date);
    return $d && date_format($d, $format) == $date;
}

function getDateFormat($date, $format = "Y-m-d H:i:s", $return_format = 'j M, Y H:i'){
    $d = date_create_from_format($format, $date);
	if($d && date_format($d, $format) == $date) {
    	return $d->format($return_format);
    }
}

function format_date_i18n($date, $format = "d/m/Y H:i:s", $lang = 'en'){
	$str_to_time = strtotime($date);
	if($str_to_time && $date != '0000-00-00 00:00:00'){
		if($lang == 'ru'){
			$rus_months = array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
			$newDatetime = new Datetime($date);
			$month = $newDatetime->format('n');
			$album_data = $newDatetime->format('j '.mb_ucfirst($rus_months[$month-1]).'');
			$album_data .= $newDatetime->format(' Y');
			return $album_data;	
		} else{
			return date($format, $str_to_time);
		}
	} else{
		return '&mdash;';
	} 
}

function arrayByKey($arrays, $key, $multiple = false){
	$rez = array();
	if(empty($arrays)){
		return $rez;
	}
	
    foreach($arrays as $one){
		if(isset($one[$key])){
			if($multiple){
				$rez[$one[$key]][] = $one;
			} else{
				$rez[$one[$key]] = $one;
			}
		}
    }
    return $rez;
}

function generate_video_html($id, $type, $w, $h, $autoplay = 0){
	if(empty($id)) return false;

	switch($type) {
		case 'vimeo':
			return '<iframe class="player bd-none" src="//player.vimeo.com/video/'.$id.'?autoplay='.$autoplay.'&title=0&amp;byline=0&amp;portrait=0&amp;color=13bdab" width="'.$w.'" height="'.$h.'" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
		break;
		case 'youtube':
			return  '<iframe class="player bd-none" width="'.$w.'" height="'.$h.'" src="//www.youtube.com/embed/'.$id.'?autoplay='.$autoplay.'" allowfullscreen></iframe>';
		break;
	}
}

function messageInModal($message, $type = 'errors'){
    $CI = &get_instance();
	$data['message'] = $message;
	$data['type'] = $type;
	echo $CI->load->view('messages_view', $data, true);
	exit();
}

function jsonResponse($message = '',$type = 'error', $additional_params = array()){
    $resp = $additional_params;
    $resp['mess_type'] = $type;
    $resp['mess_class'] = $type;
    $resp['message'] = $message;

    echo json_encode($resp);
    exit();
}

function jsonDTResponse($message = '',$additional_params = array(), $type = 'error'){
    $output = array(
		"iTotalRecords" => 0,
		"iTotalDisplayRecords" => 0,
		"aaData" => array()
    );

    $output = array_merge($output, $additional_params);
    $output['mess_type'] = $type;
    $output['message'] = $message;

    echo json_encode($output);
    exit();
}

function id_from_link($str, $to_int = true){
    $segments  = explode('-', $str);
	
	if($to_int){
    	return (int)end($segments);
	} else{
    	return end($segments);
	}
}

function clean_output($str = ''){
	return htmlspecialchars($str);
}

function lang_title($str='no_title'){
    $CI = get_instance();
	echo $CI->lang->line($str);
}

function cut_str($str = "", $maxlength = 50){
	$trimmed_str = trim($str);
	return substr($trimmed_str, 0, $maxlength);
}

function text_elipsis($str = "", $maxlength = 50){
	$trimmed_str = trim($str);
	if(strlen($trimmed_str) > $maxlength){
		return substr($trimmed_str, 0, $maxlength).'...';
	}

	return $trimmed_str;
}

function remove_dir($dir,$action = 'delete') {
	if(empty($dir))
		return false;
	
	if(!is_dir($dir))
		return false;

	foreach(glob($dir . '/*') as $file) {
		@unlink($file);
	}
	
	if($action == 'delete')
		rmdir($dir);
}

function create_dir($dir, $mode = 0755){
    if(!is_dir($dir))
        mkdir ($dir, $mode, true);
}

function file_modification_time($link){
	if(!file_exists($link)){
		$version = uniqid();
	} else{
		$version = md5(filemtime( $link ));
	}
    return base_url($link).'?v='.$version;
}

if (!function_exists('mb_ucfirst') && function_exists('mb_substr')) {
    function mb_ucfirst($string) {
        $string = mb_strtoupper(mb_substr($string, 0, 1)) . mb_substr($string, 1);
        return $string;
    }
}

function return_404($data = array()) {
	$ci =& get_instance();
	// echo $ci->load->view('errors/custom/404_view', $data, true);
	// exit();

	$data['error_page']['header_ru'] = 'Ошибка 404. Эта страница была удалена.';
	$data['error_page']['header_en'] = 'Error 404. This page has been deleted.';
	$data['error_page']['text_ru'] = 'Перейдите на <a href="/ru">главную страницу</a>.';
	$data['error_page']['text_en'] = 'Please go to the <a href="/en"> home page </a>.';

	$_404_content = 'include/404_view';
	if (!$ci->input->is_ajax_request()) {
		$ci->output->set_status_header('404');
		$data['main_content'] = $_404_content;
		echo $ci->load->view(get_theme_view('page_template'), $data, true);
		exit();
	} else{
		$content = $ci->load->view(get_theme_view($_404_content), $data, true);
		jsonResponse('', 'success', array('content' => $content));
	}
}